<?php
namespace common\components;

use Yii;
use yii\base\Component;
use yii\base\ErrorException;
use yii\httpclient\Client;
use yii\httpclient\CurlTransport;
use yii\httpclient\Request;

/**
 * Class Api
 *
 * @property string $host
 * @property string $auth
 * @property string $token
 *
 * @package common\components
 */
class Api extends Component
{

    public $host;
    public $auth;
    public $token;

    /**
     * @inheritDoc
     * @throws ErrorException
     */
    public function init(): void
    {
        // Если нет активного платформы
        if(!$this->host || !$this->auth || !$this->token) {
            throw new ErrorException('Необходимо установить все параметры для API');
        }

        parent::init();
    }

    /**
     * Отправка запроса и ответ в расширенном формате
     *
     * @param string $apiUrl
     * @param string $method
     * @param array  $data
     * @param array  $headers
     * @param array  $fileData : ['name' => 'name file request', 'path' => 'path to file local']
     *
     * @return array ['success' => true|false, 'data' => []]
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\httpclient\Exception
     */
    public function requestWithInfo(string $apiUrl, $method = 'GET', $data = [], $headers = [], $fileData = []): array
    {
        $request = $this->initClient($apiUrl, $headers, $method)->setData($data);
        if (!empty($fileData)) {
            $request->addFile($fileData['name'], $fileData['path']);
        }
        $response = $request->send();

        if (!$response->isOk) {
            return ['success' => false, 'data' => $response->data];
        }

        return ['success' => true, 'data' => $response->data];
    }

    /**
     * Возвращает адреса
     * @param string $address
     * @return array
     */
    public function getAddress(string $address)
    {
        $result = [];
        $address = trim($address);
        $search = Yii::$app->api->requestWithInfo('address/search/similar', 'GET', ['query' => $address]);
        if (!empty($search['data']['items'])) {
            foreach ($search['data']['items'] as $id => $value) {
                $result[] = [
                    'id' => $id,
                    'value' => trim($value),
                    'label' => trim($value),
                ];
            }
        }

        return $result;
    }

    /**
     * Инициализация клиента
     *
     * @param string $apiUrl
     * @param array  $headers
     * @param string $method
     *
     * @return Request
     * @throws \yii\base\InvalidConfigException
     */
    private function initClient(string $apiUrl, array $headers, $method = 'GET'): Request
    {
        $url = $this->host . '/' . $apiUrl;
        $headers['Authorization'] = $this->auth;
        $headers['Api-Token'] = $this->token;

        $client = new Client(['transport' => CurlTransport::class]);
        return $client
            ->createRequest()
            ->setOptions([
                CURLOPT_TIMEOUT => Yii::$app->params['apiTimeoutRequest'],
            ])
            ->setHeaders($headers)
            ->setMethod($method)
            ->setUrl($url);
    }
}