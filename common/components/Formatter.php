<?php
namespace common\components;

/**
 * Class Api
 *
 * @property string $host
 * @property string $auth
 * @property string $token
 *
 * @package common\components
 */
class Formatter extends \yii\i18n\Formatter
{
    /**
     * Формирует вывод цены
     *
     * @param float $price
     * @return float
     */
    public function asPrice(float $price)
    {
        return $this->asCurrency($price, 'RUB');
    }

    /**
     * Вовзращает количество лет прошедших с даты
     *
     * @param string $date
     * @return int
     */
    public function getAgeByDate(string $date)
    {
        $date = strtotime($date);
        $age = date('Y') - date('Y', $date);
        if (date('md', $date) > date('md')) {
            $age--;
        }
        return (int)$age;
    }

}