<?php
namespace common\components\mailer;

use Yii;
use yii\base\Component;

/**
 * Class SendMail
 */
class SendMail extends Component
{
    /**
     * @var string От кого (email)
     */
    private $from = 'test@mail.ru';

    /**
     * @var string Кому (email)
     */
    private $to;

    /**
     * @var string Тема письма
     */
    private $subject = '';

    /**
     * @var string Текст письма (с HTML тегами)
     */
    private $body = '';

    /**
     * @var array Массив ссылок на файлы прикрепляемые к письму
     */
    private $files = [];

    /**
     * @var string|null Имя шаблона
     */
    private $templateView = null;

    /**
     * @var array Параметры для шаблона
     */
    private $templateParams = [];

    /**
     * Инициирует отправку
     * @param array $params
     * @return SendMail
     */
    public function compose($params = [])
    {
        $model = new self();
        // Если пришли параметры для класса сразу в $params - выставляю
        if (!empty($params)) {
            foreach ($params as $key => $value) {
                $model->{$key} = $value;
            }
        }

        return $model;
    }

    /**
     * Отправляет письмо
     * Пример использования:
     *  Отправка письма через body
        \Yii::$app->mail->compose()
        ->setTo('user@mail.ru')
        ->setSubject("Тема письма")
        ->setBody("Тело письма")
        ->attach($filePatch)
        ->send();
     * Отправка письма используя шаблон (шаблон хранить в \common\components\mailer\views)
        \Yii::$app->mail->compose()
        ->setTo('user@mail.ru')
        ->setSubject("Тема письма")
        ->setTemplateView("template_name")
        ->setTemplateParams([
            'userName' => 'Имя Пользователя',
            'companyName' => 'Название Компании',
        ])
        ->attach($policyPatch)
        ->send();
     *
     * @return bool|void
     */
    public function send()
    {
        $mailer = Yii::$app->mailer;
        $mailer->viewPath = Yii::getAlias('@mailViews');

        $mail = Yii::$app->mailer->compose($this->templateView, $this->templateParams)
            ->setFrom($this->from)
            ->setTo($this->to)
            ->setSubject($this->subject);

        if (!empty($this->files)) {
            foreach ($this->files as $file) {
                $mail->attach($file);
            }
        }

        if (!empty($this->body)) {
            $mail->setTextBody('')
                ->setHtmlBody($this->body);
        }

        return $mail->send();
    }

    /**
     * Задает email отправителя
     * @param string $from
     */
    public function setFrom(string $from) {
        $this->from = trim($from);
        return $this;
    }

    /**
     * Задает email получателя
     * @param string $to
     */
    public function setTo(string $to) {
        $this->to = trim($to);
        return $this;
    }

    /**
     * Задает тему письма
     * @param string $subject
     */
    public function setSubject(string $subject)
    {
        $this->subject = trim($subject);
        return $this;
    }

    /**
     * Задает текст письма
     * @param string $body
     */
    public function setBody(string $body)
    {
        $this->body = trim($body);
        return $this;
    }

    /**
     * Задает шаблон
     * @param string $template
     */
    public function setTemplateView(string $template)
    {
        $this->templateView = trim($template);
        return $this;
    }

    /**
     * Задает параметры для шаблона
     * @param array $params
     */
    public function setTemplateParams(array $params)
    {
        $this->templateParams = $params;
        return $this;
    }

    /**
     * Прикрепляет файл к письму
     * @param string $filePatch
     */
    public function attach(string $filePatch)
    {
        if (file_exists($filePatch)) {
            $this->files[] = $filePatch;
        }
        return $this;
    }
}