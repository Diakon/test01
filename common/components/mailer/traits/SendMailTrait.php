<?php

namespace common\components\mailer\traits;

use common\modules\insurance\models\InsuranceFactory;
use Yii;
use common\modules\orders\dictionaries\OrderDictionary;
use common\modules\orders\models\Order;

trait SendMailTrait
{
    /**
     * Отправка письма об успешной оплате полиса ОСАГО
     * @param Order $order
     */
    public function osagoSuccsessPaymentMail(Order $order)
    {
        $model = (new InsuranceFactory())->documentsOSAGO();
        $model->setDocuments($order);
        $documents = $model->documents;
        if ($documents) {
            $filePolicyName = "$order->token.pdf";
            $patch = Yii::getAlias('@documents');
            $patch = $patch . '/' . OrderDictionary::POLICY_DIRECTORY_NAME;
            $policyPatch = $patch . '/' . $filePolicyName;
            // Если файла полиса нет - загружаю
            if (!is_dir($patch)) {
                @mkdir($patch, 0777, true);
            }
            if (!file_exists($policyPatch)) {
                $policyPdf = file_get_contents($documents->policyOSAGOLink);
                @file_put_contents($policyPatch, $policyPdf);
            }

            // Письма отправляются страховщику и владельцу (если разные лица)
            $ownerEmail = trim($order->propertyValue('ownerEmail'));
            $insurerEmail = trim($order->propertyValue('insurerEmail'));

            // Данные передаваемые в шаблон
            $carNumber = trim($order->propertyValue('vehicleLicensePlate'));
            $carVin = trim($order->propertyValue('vehicleVin'));
            $carChassis = trim($order->propertyValue('vehicleChassisNum'));
            $carBody = trim($order->propertyValue('vehicleBodyNum'));
            $companyName = trim($order->propertyValue('companyName'));
            $dateOSAGOStart = $order->propertyValue('ownerDateOSAGOStart');
            $dateOSAGOEnd = $order->propertyValue('ownerDateOSAGOEnd');

            $dateOSAGOStart = !empty($dateOSAGOStart) ? Yii::$app->formatter->asDate($dateOSAGOStart, 'php:d.m.Y') : "";
            $dateOSAGOEnd = !empty($dateOSAGOEnd) ? Yii::$app->formatter->asDate($dateOSAGOEnd, 'php:d.m.Y') : "";

            // Отправляю письма
            foreach (['isOwner' => $ownerEmail, 'isInsurer' => $insurerEmail] as $typeUser => $email) {
                if (empty($email)) {
                    continue;
                }

                $userName = $typeUser == 'isOwner'
                    ? (trim($order->propertyValue('ownerFirstName') . ' ' . $order->propertyValue('ownerMiddleName')))
                    : (trim($order->propertyValue('insurerFirstName') . ' ' . $order->propertyValue('insurerMiddleName')));

                $params = [
                    'userName' => $userName,
                    'carVin' => $carVin,
                    'carNumber' => $carNumber,
                    'carChassis' => $carChassis,
                    'carBody' => $carBody,
                    'companyName' => $companyName,
                    'dateOSAGOStart' => $dateOSAGOStart,
                    'dateOSAGOEnd' => $dateOSAGOEnd,
                ];

                \Yii::$app->mail->compose()
                    ->setTo($email)
                    ->setSubject("Полис ОСАГО успешно оплачен. Копия документа электронного полиса.")
                    ->setTemplateView("payment_osago")
                    ->setTemplateParams($params)
                    ->attach($policyPatch)
                    ->send();
            }
        }
    }
}