<?php


/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */
/* @var $content string main view render result */
?>
<?php $this->beginPage() ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta content="telephone=no" name="format-detection"/>
    <title></title>
    <style type="text/css">
        body {
            Margin: 0;
            padding: 0;
            min-width: 100%;
            background-color: #ffffff;
        }

        table {
            border-spacing: 0;
        }

        td {
            padding: 0;
        }

        img {
            border: 0;
        }

        .wrapper {
            width: 100%;
            table-layout: fixed;
            -webkit-text-size-adjust: 100%;
            -ms-text-size-adjust: 100%;
        }

        .webkit {
            max-width: 600px;
        }

        @-ms-viewport {
            width: device-width;
        }

        @media screen and (max-width: 600px) {
            .mobile {
                Margin-left: 15px !important;
                Margin-right: 15px !important;
            }

            .mobile__margin_zero {
                Margin-left: 0 !important;
                Margin-right: 0 !important;
            }
        }
    </style>
    <!--[if (gte mso 9)|(IE)]>
    <style type="text/css">
        table {
            border-collapse: collapse;
        }
    </style>
    <![endif]-->
    <!--[if mso]>
    <style type=”text/css”>
        body,
        table,
        td
        .body-text {
            font-family: Helvetica, Arial, Sans-Serif !important;
        }
    </style>
    <![endif]-->
</head>
<body>
<?php $this->beginBody() ?>

<?= $content ?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
