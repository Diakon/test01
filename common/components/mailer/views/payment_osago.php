<?php
/**
 * @var string $userName                ФИО
 * @var string $carVin                  ВИН ТС
 * @var string $carNumber               Гос. номер ТС
 * @var string $carBody                 Номер кузова
 * @var string $carChassis              Номер шасси
 * @var string $companyName             Название страховой компании
 * @var string $dateOSAGOStart          Дата начала действия полиса
 * @var string $dateOSAGOEnd            Дата окончания действия полиса
 */
$logo = $message->embed(Yii::getAlias('@root') . '/frontend/web/images/mail-logo.jpg');
?>

<div style="background-color: #ffffff;">
    <center class="wrapper">
        <div class="webkit">
            <table class="body-text" style="Margin: 0; padding: 0; border: 0; border-collapse: collapse; border-spacing: 0; width: 100%; background-color: #ffffff;" cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td class="pad_null">
                        <table style="Margin: 0; padding: 0; border: 0; border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <tr>
                                <td class="pad_null" style="padding: 0; vertical-align: top;"></td>
                                <td class="pad_null" style="padding: 0 10px 1px 10px; vertical-align: top; width: 600px;">
                                    <table style="
									border: 0; border-collapse: collapse; border-spacing: 0; width: 600px; Margin: 45px auto 30px auto; padding: 0;
									">
                                        <tr>
                                            <td class="pad_null">
                                                <div class="mobile" style="Margin: 0 0 0 62px">
                                                    <img src="<?= $logo ?>" alt="" style="display: block; Margin: 0; border: 0;">
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                    <table border="0" cellpadding="0" cellspacing="0" width="600px" style="padding: 0; background: #ffffff; Margin: 0 auto; overflow: auto; color: #000000; border-spacing: 0;">
                                        <tr>
                                            <td class="pad_null body-text" style="font-family: Helvetica, Arial, Sans-Serif !important;">
                                                <p class="mobile body-text" style="Margin: 0 62px 20px 62px; line-height: 1.6; color: #3f1b4e; font-size: 24px; font-family: Times, Sans; font-weight: 800;">Уважаемый, <?= $userName ?>!</p>
                                                <p class="mobile body-text" style="Margin: 0 62px 20px 62px; line-height: 1.6; color: #2c2c2c; font-size: 20px;">
                                                    Была успешно произведена оплата полиса ОСАГО в страховой компании «<?= $companyName ?>» для автомобиля
                                                    <?php
                                                        if (!empty($carNumber)) {
                                                            echo " c гос. номером $carNumber";
                                                        } else {
                                                            if (!empty($carVin)) {
                                                                echo " c VIN $carVin";
                                                            }
                                                            if (!empty($carBody)) {
                                                                echo " c номером кузова $carBody";
                                                            }
                                                            if (!empty($carChassis)) {
                                                                echo " c номером шасси $carChassis";
                                                            }
                                                        }
                                                    ?>!<br>
                                                    Срок действия полиса с <?= $dateOSAGOStart ?> по <?=  $dateOSAGOEnd ?>. Мы напомним Вам о необходимости продлить полис заранее, через год.
                                                    Скачать полис вы можете в прикрепленном к этому письму PDF файле.<br>
                                                    Спасибо, что решили остановить свой выбор на нас!
                                                </p>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td class="pad_null" style="padding: 0; vertical-align: top;"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </center>
</div>
