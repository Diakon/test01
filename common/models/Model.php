<?php
namespace common\models;

use yii\helpers\Inflector;
use yii\base\Model as BaseModel;

/**
 * Class Model
 * @package common\models
 */
class Model extends BaseModel
{
    /**
     * Возвращает сокращенное имя класса (без пути)
     *
     * @param false $format
     * @return mixed|string
     */
    public static function classNameShort($format = false)
    {
        $path = explode('\\', static::className());
        $name = end($path);

        switch ($format) {
            case 'id':
                $name = Inflector::camel2id($name, '_');
                break;
        }

        return $name;
    }
}