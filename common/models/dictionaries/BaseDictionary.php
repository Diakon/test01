<?php

namespace common\models\dictionaries;

/**
 *
 */
class BaseDictionary
{
    /**
     * Типы сервисов оформления ОСАГО
     */
    public const TYPE_INGURU = 1;
    public const TYPE_SRAVNIRU = 2;
    public const TYPE_ELMARKET = 3;
    const TYPES = [
        self::TYPE_INGURU => 'Ингуру',
        self::TYPE_SRAVNIRU => 'Сравни.ру',
        self::TYPE_ELMARKET => 'El-market',
    ];

}