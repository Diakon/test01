<?php

namespace common\modules\insurance\dictionaries;

use common\models\dictionaries\BaseDictionary;

/**
 * Class InsuranceFormDictionaries
 * @package common\modules\insurance\dictionaries
 */
class InsuranceFormDictionaries extends BaseDictionary
{
    /**
     * Категории ТС
     */
    public const CAR_CATEGORY_A = 0;
    public const CAR_CATEGORY_B = 1;
    public const CAR_CATEGORY_C = 2;
    const CAR_CATEGORY_LIST = [
        self::CAR_CATEGORY_A => "A",
        self::CAR_CATEGORY_B => "B",
        self::CAR_CATEGORY_C => "C",
    ];

    /**
     * Идентификация ТС по VIN, или кузову, или шасси
     */
    public const CAR_HAVE_VIN = 0;
    public const CAR_HAVE_BODY = 1;
    public const CAR_HAVE_CHASSIS = 2;
    const CAR_HAVE_LIST = [
        self::CAR_HAVE_VIN => "VIN номер ТС",
        self::CAR_HAVE_BODY => "Номер кузова",
        self::CAR_HAVE_CHASSIS => "Номер шасси",
    ];

    /**
     * Период использования (кол-во месяцев) ТС
     */
    public const MONTH_3 = 3;
    public const MONTH_4 = 4;
    public const MONTH_5 = 5;
    public const MONTH_6 = 6;
    public const MONTH_7 = 7;
    public const MONTH_8 = 8;
    public const MONTH_9 = 9;
    public const MONTH_10 = 10;
    public const MONTH_12 = 12;
    const MONTHS = [
        self::MONTH_12 => '12 месяцев',
        self::MONTH_9 => '9 месяцев',
        self::MONTH_8 => '8 месяцев',
        self::MONTH_7 => '7 месяцев',
        self::MONTH_6 => '6 месяцев',
        self::MONTH_5 => '5 месяцев',
        self::MONTH_4 => '4 месяца',
        self::MONTH_3 => '3 месяца',
    ];

    /**
     * Ключ названия сессии для записи предложений от страховой компании
     */
    public const SESSION_INSURE_DATA_NAME = 'insure-offer';

    /**
     * Количество максимально возможных водителей
     */
    public const MAX_DRIVERS_COUNT = 5;

    /**
     * Максимальное количество дней на которые можно получить полис (текущая дата + это значение)
     */
    public const MAX_DAY_POLICY = 60;

    /**
     * Указать предыдущее ВУ
     */
    public const ADD_PREV_DRIVER_INFO_YES = 1;
    public const ADD_PREV_DRIVER_INFO_NO = 0;

    /**
     * Тип расчета
     */
    public const PRE_CALCULATION_YES = 1; // Предварительный расчет
    public const PRE_CALCULATION_NO = 0; // Полный расчет
    const TYPE_CALC = [
        self::PRE_CALCULATION_YES => "Предварительный расчет (минимум данных)",
        self::PRE_CALCULATION_NO => "Полный расчет (с возможностью оформления полиса)",
    ];

    /**
     * Мультидрайв (ввод водителей)
     */
    public const IS_MULTI_DRIVE_YES = 1;
    public const IS_MULTI_DRIVE_NO = 0;

    /**
     * Есть предыдущий полис
     */
    public const PREV_POLICY_ISSET_YES = 1; // Есть предыдущий полис
    public const PREV_POLICY_ISSET_NO = 0; // Нет предыдущего полиса

    /**
     * Сколько секунд будет доступна ссылка на оплату
     */
    public const TIMESTAMP_AVAILABLE_PAYMENT_URL = 86400;

    /**
     * Типы документов автомобиля
     */
    public const CAR_DOC_TYPE_PTS = 1;
    public const CAR_DOC_TYPE_STS = 2;
    public const CAR_DOC_TYPE_EPTS = 4;
    const CAR_DOCS = [
        self::CAR_DOC_TYPE_PTS => 'ПТС',
        self::CAR_DOC_TYPE_STS => 'СТС',
        self::CAR_DOC_TYPE_EPTS => 'ЭПТС',
    ];

    /**
     * Страхователь и владелец автомобиля одно лицо
     */
    public const ONE_FACE_YES = 1;
    public const ONE_FACE_NO = 0;
    const ONE_FACES = [
        self::ONE_FACE_YES => 'Да',
        self::ONE_FACE_NO => 'Нет',
    ];
}