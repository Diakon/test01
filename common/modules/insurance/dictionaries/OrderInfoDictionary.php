<?php

namespace common\modules\insurance\dictionaries;

use common\models\dictionaries\BaseDictionary;

/**
 * Class OrderInfoDictionary
 * @package common\modules\insurance\dictionaries
 */
class OrderInfoDictionary extends BaseDictionary
{
    public const STATUS_PAID_TRUE = 1;
    public const STATUS_PAID_FALSE = 0;
    const STATUS_PAID_LIST = [
        self::STATUS_PAID_TRUE => "Заказ полиса ОСАГО оплачен",
        self::STATUS_PAID_FALSE => "Заказ полиса ОСАГО не оплачен",
    ];

}