<?php

namespace common\modules\insurance\dictionaries\inguru;

/**
 * Class ModelBrandDictionary
 *
 * @package common\modules\insurance\dictionaries\inguru
 */
class BrandModelDictionary extends InguruDictionary
{
    /**
     * Ссылка на список брендов и моделей
     */
    public const API_BRAD_MODEL_URL = 'https://osago.finuslugi.ru/data/EOsago.json';

    /**
     * Константа для передачи в Ингуру, если ищем марку (по модели)
     */
    public const SEARCH_BRAND = 'brand';

    /**
     * Константа для передачи в Ингуру, если ищем модель (по марке)
     */
    public const SEARCH_MODEL = 'model';

    /**
     * Константа для передачи в Ингуру, если ищем марку (по модели) в конкретной страховой компании
     */
    public const SEARCH_BRAND_IN_COMPANY = 'list_brands';

    /**
     * Константа для передачи в Ингуру, если ищем модель (по марке) в конкретной страховой компании
     */
    public const SEARCH_MODEL_IN_COMPANY = 'list_models';

    /**
     * Тип поиска Бренда/Модели
     */
    public const SEARCH_TYPE_1 = 1;
    public const SEARCH_TYPE_2 = 2;
    const SEARCH_TYPE_LIST = [
        self::SEARCH_TYPE_1 => 'Поиск марки',
        self::SEARCH_TYPE_2 => 'Поиск модели'
    ];
}