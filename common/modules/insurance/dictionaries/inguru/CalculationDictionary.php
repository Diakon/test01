<?php

namespace common\modules\insurance\dictionaries\inguru;

/**
 * Class CalculationDictionary
 *
 * @package common\modules\insurance\dictionaries\inguru
 */
class CalculationDictionary extends InguruDictionary
{
    /**
     * Тип водительского удостоверения
     */
    public const DRIVER_LICENSE_TYPE_RU = 0;
    public const DRIVER_LICENSE_TYPE_FOREIGN = 1;
    const DRIVER_LICENSE_TYPES = [
        self::DRIVER_LICENSE_TYPE_RU => "РФ",
        self::DRIVER_LICENSE_TYPE_FOREIGN => "Иностранное"
    ];

    /**
     * Страховые компании
     */
    public const INSURANCE_COMPANY_ALPHA = 1;
    //public const INSURANCE_COMPANY_INGOS = 3;
    public const INSURANCE_COMPANY_ROSGOS = 5;
    public const INSURANCE_COMPANY_SOGLASIE = 7;
    public const INSURANCE_COMPANY_ZETTA = 27;
    public const INSURANCE_COMPANY_RESO = 32;
    public const INSURANCE_COMPANY_VSK = 33;
    public const INSURANCE_COMPANY_RENESSANS = 36;
    public const INSURANCE_COMPANY_TINKOFF = 107;
    public const INSURANCE_COMPANY_MAFIN = 142;
    public const INSURANCE_COMPANY_MAX = 10;
    public const INSURANCE_COMPANY_ABSOLUT = 75;
    public const INSURANCE_COMPANY_EVRO = 127;
    public const INSURANCE_COMPANY_GAIDE = 47;
    public const INSURANCE_COMPANY_ASTRO = 98;
    public const INSURANCE_COMPANY_VERNA = 125;
    public const INSURANCE_COMPANY_SOGAZ = 46;
    public const INSURANCE_COMPANY_UGORIA = 25;
    public const INSURANCE_COMPANY_OSK = 61;
    public const INSURANCE_COMPANY_INTACH = 39;
    const INSURANCE_COMPANIES = [
        self::INSURANCE_COMPANY_ALPHA => 'АльфаСтрахование',
        // self::INSURANCE_COMPANY_INGOS => 'Ингосстрах',
        self::INSURANCE_COMPANY_ROSGOS => 'Росгосстрах',
        self::INSURANCE_COMPANY_SOGLASIE => 'Согласие',
        self::INSURANCE_COMPANY_ZETTA => 'Зетта Страхование',
        self::INSURANCE_COMPANY_RESO => 'РЕСО',
        self::INSURANCE_COMPANY_VSK => 'ВСК',
        self::INSURANCE_COMPANY_RENESSANS => 'Ренессанс Страхование',
        self::INSURANCE_COMPANY_TINKOFF => 'Тинькофф Страхование',
        self::INSURANCE_COMPANY_MAFIN => 'Mafin',
        self::INSURANCE_COMPANY_MAX => 'МАКС',
        self::INSURANCE_COMPANY_ABSOLUT => 'Абсолют Страхование',
        self::INSURANCE_COMPANY_EVRO => 'ЕВРОИНС',
        self::INSURANCE_COMPANY_OSK => 'ОСК',
        self::INSURANCE_COMPANY_GAIDE => 'Гайде',
        self::INSURANCE_COMPANY_ASTRO => 'Астро-Волга',
        self::INSURANCE_COMPANY_VERNA => 'ВЕРНА',
        self::INSURANCE_COMPANY_SOGAZ => 'СОГАЗ',
        self::INSURANCE_COMPANY_UGORIA => 'Югория',
        self::INSURANCE_COMPANY_INTACH => 'Интач',
    ];

    /**
     * Способ использования автомобиля
     */
    public const CAR_PURPOSE_PERSONAL = 'personal';
    public const CAR_PURPOSE_TAXI = 'taxi';
    public const CAR_PURPOSE_TRAINING = 'training';
    public const CAR_PURPOSE_DANGER = 'dangerous';
    public const CAR_PURPOSE_RENTAL = 'rental';
    public const CAR_PURPOSE_PASSENGER = 'passenger';
    public const CAR_PURPOSE_SPECIAL = 'special';
    public const CAR_PURPOSE_SERVICES = 'services';
    public const CAR_PURPOSE_OTHER = 'other';
    const CAR_PURPOSES = [
        self::CAR_PURPOSE_PERSONAL => 'Личная',
        self::CAR_PURPOSE_TAXI => 'Такси',
        self::CAR_PURPOSE_TRAINING => 'Учебная езда',
        self::CAR_PURPOSE_DANGER => 'Перевозка опасных и легковоспламеняющихся грузов',
        self::CAR_PURPOSE_RENTAL => 'Прокат/краткосрочная аренда',
        self::CAR_PURPOSE_PASSENGER => 'Регулярные пассажирские перевозки/перевозки пассажиров по заказам',
        self::CAR_PURPOSE_SPECIAL => 'Дорожные и специальные транспортные средства',
        self::CAR_PURPOSE_SERVICES => 'Экстренные и коммунальные службы',
        self::CAR_PURPOSE_OTHER => 'Прочее',
    ];

    /**
     * Признак управления транспортным средством с прицепом
     */
    public const USE_TRAILER_YES = 1;
    public const USE_TRAILER_NO = 0;
    const USE_TRAILER = [
        self::USE_TRAILER_NO => 'Нет',
        self::USE_TRAILER_YES => 'Да',
    ];

    /**
     * Тип паспорта
     */
    public const PASSPORT_TYPE_RU = 0;
    public const PASSPORT_TYPE_FOREIGN = 1;
    const PASSPORT_TYPES = [
        self::PASSPORT_TYPE_RU => 'Паспорт РФ',
        self::PASSPORT_TYPE_FOREIGN => 'Иностранное гражданство',
    ];

    /**
     * Признак точного расчета с запросом в РСА
     */
    public const EXTRACT_CALC_YES = 1;
    public const EXTRACT_CALC_NO = 0;

    /**
     * Типы ТС
     */
    public const VEHICLE_TYPE_CAR = 1;
    public const VEHICLE_TYPE_TAXI = 2;
    public const VEHICLE_TYPE_BUS = 3;
    public const VEHICLE_TYPE_BUS_MORE_16 = 4;
    public const VEHICLE_TYPE_TRUCK = 5;
    public const VEHICLE_TYPE_TRUCK_MORE_16 = 6;
    public const VEHICLE_TYPE_MOTO = 7;
    public const VEHICLE_TYPE_TROLLEYBUS = 8;
    public const VEHICLE_TYPE_TRAMS = 9;
    public const VEHICLE_TYPE_SPEC = 10;
    public const VEHICLE_TYPE_TAXI_BUS = 11;
    const VEHICLE_TYPES = [
        self::VEHICLE_TYPE_CAR => 'Легковой автомобиль',
        self::VEHICLE_TYPE_TAXI => 'Такси',
        self::VEHICLE_TYPE_BUS => 'Автобусы (16 и менее мест)',
        self::VEHICLE_TYPE_BUS_MORE_16 => 'Автобусы (более 16 мест)',
        self::VEHICLE_TYPE_TRUCK => 'Грузовики (16 и менее тонн)',
        self::VEHICLE_TYPE_TRUCK_MORE_16 => 'Грузовики (более 16 тонн)',
        self::VEHICLE_TYPE_MOTO => 'Мотоциклы',
        self::VEHICLE_TYPE_TROLLEYBUS => 'Троллейбусы',
        self::VEHICLE_TYPE_TRAMS => 'Трамваи',
        self::VEHICLE_TYPE_SPEC => 'Спец. техника',
        self::VEHICLE_TYPE_TAXI_BUS => 'Маршрутные автобусы',
    ];
}