<?php

namespace common\modules\insurance\dictionaries\inguru;

/**
 * Class PaymentDictionary
 *
 * @package common\modules\insurance\dictionaries\inguru
 */
class PaymentDictionary extends InguruDictionary
{
    /**
     * Константа для передачи в Ингуру
     */
    public const INGURU_PAYMENT_CONS = 'payment';

    /**
     * Статус оплаты возвращаемый ИНГУРУ
     */
    public const INGURU_STATUS_PAYMENT_PROCESSED = 0;
    public const INGURU_STATUS_PAYMENT_SUCCESS = 1;
    public const INGURU_STATUS_PAYMENT_FAIL = 2;
    public const INGURU_STATUS_PAYMENT_OTHER_SUCCESS = 3;
    const INGURU_STATUSES_PAYMENT = [
        self::INGURU_STATUS_PAYMENT_PROCESSED => 'Идет опрос компании',
        self::INGURU_STATUS_PAYMENT_SUCCESS => 'Оплачен',
        self::INGURU_STATUS_PAYMENT_FAIL => 'Не оплачен',
        self::INGURU_STATUS_PAYMENT_OTHER_SUCCESS => 'Оплата зафиксирована в рамках другого оформления'
    ];
}