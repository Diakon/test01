<?php

namespace common\modules\insurance\dictionaries\sravniru;

/**
 * Class BrandModelDictionary
 * @package common\modules\insurance\dictionaries\sravniru\SravniruDictionary
 */
class BrandModelDictionary extends SravniruDictionary
{
    /**
     * Типы поиска ТС
     */
    public const TYPE_SEARCH_BRAND = 0; // Поиск бренда
    public const TYPE_SEARCH_MODEL = 1; // Поиск модели
    public const TYPE_SEARCH_POWER = 3; // Поиск мощности
}