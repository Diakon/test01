<?php

namespace common\modules\insurance\dictionaries\sravniru;

use common\models\dictionaries\BaseDictionary;

/**
 * Class SravniruDictionary
 * @package common\modules\insurance\dictionaries\sravniru
 */
class SravniruDictionary extends BaseDictionary
{
    public const TEST_HOST = 'http://apigateway.svc.master.qa.sravni-team.ru/';
    public const PROD_HOST = 'https://api.sravni.ru/';
}