<?php

namespace common\modules\insurance\forms;

use common\models\Model;

/**
 * Форма для ответа списка моделей и брендов авто
 */
class BrandsModelsInsuranceForm extends Model
{
    /**
     * @var string|integer
     */
    public $id;

    /**
     * @var string|integer
     */
    public $name;

    /**
     * @var integer
     */
    public int $insuranceType;

    const SCENARIO_BRANDS = 'brands';
    const SCENARIO_MODELS = 'models';

    /**
     * @inheritdoc
     */
    public function scenarios(): array
    {
        $scenarios = parent::scenarios();

        $scenarios[self::SCENARIO_BRANDS] = $scenarios[self::SCENARIO_MODELS] = ['id', 'name', 'insuranceType'];

        return $scenarios;
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            [['id', 'name', 'insuranceType'], 'required'],
            [['id', 'name'], 'trim'],
        ];
    }
}