<?php

namespace common\modules\insurance\forms;

use common\models\dictionaries\BaseDictionary;
use common\models\Model;

/**
 * Форма для ответа предложения от страховой компании
 */
class CalculationInsuranceForm extends Model
{
    /**
     * Генерируемый уникальный ключ для предложения
     *
     * @var string
     */
    public string $key;

    /**
     * ID расчета (в АПИ агрегатора)
     * @var string|integer
     */
    public $calculateId;

    /**
     * ID предложения (в АПИ агрегатора)
     * @var string|integer
     */
    public $offerId;

    /**
     * ID компании
     * @var string|integer
     */
    public $companyId;

    /**
     * Название компании
     * @var string|null
     */
    public ?string $companyName;

    /**
     *  Ссылка на оплату
     * @var string|null
     */
    public ?string $paymentUrl;

    /**
     * ID сервиса агрегатора
     * @var int
     */
    public int $insuranceType;

    /**
     * Стоимость полиса
     * @var float|null
     */
    public ?float $price;

    /**
     * Размер комиссии
     * @var float|null
     */
    public ?float $commission;

    /**
     * Токен (хеш) код операции в агрегаторе
     * @var string|null
     */
    public ?string $paymentToken;

    /**
     * Коэффициенты влияющие на расчет (информация)
     * @var array
     */
    public array $coefficients = [];


    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            [['offerId', 'companyId', 'companyName', 'paymentUrl', 'insuranceType', 'price', 'key'], 'required'],
            [['offerId', 'companyName', 'calculateId', 'paymentToken'], 'trim'],
            [['coefficients', 'calculateId', 'companyName', 'paymentToken'], 'safe'],
            [['price', 'commission'], 'number'],
            [['insuranceType'], 'integer'],
            [['insuranceType'], 'in', 'range' => array_keys(BaseDictionary::TYPES)]
        ];
    }

    /**
     * @return bool
     */
    public function beforeValidate()
    {
        // Генерирую уникальный ключ для предложения (для идентификации предложения на сайте)
        $this->key = md5($this->companyId . '-' . $this->offerId . '-' . $this->insuranceType . '-' . time());

        return parent::beforeValidate();
    }
}