<?php

namespace common\modules\insurance\forms;

use common\models\dictionaries\BaseDictionary;
use common\models\Model;

/**
 * Форма информации о документах ОСАГО от агрегатора
 */
class DocumentsOSAGOForm extends Model
{
    /**
     * @var string
     */
    public string $policyOSAGOLink;

    /**
     * @var string
     */
    public string $otherLink;

    /**
     * @var integer
     */
    public int $insuranceType;

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            [['policyOSAGOLink', 'insuranceType'], 'required'],
            [['policyOSAGOLink', 'otherLink'], 'trim'],
            [['insuranceType'], 'integer'],
            [['policyOSAGOLink', 'otherLink'], 'string'],
            [['insuranceType'], 'in', 'range' => array_keys(BaseDictionary::TYPES)]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'policyOSAGOLink' => 'Полис ОСАГО',
            'otherLink' => 'Другое',
        ];
    }
}