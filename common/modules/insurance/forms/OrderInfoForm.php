<?php

namespace common\modules\insurance\forms;

use common\models\dictionaries\BaseDictionary;
use common\models\Model;
use common\modules\insurance\dictionaries\OrderInfoDictionary;

/**
 * Форма информации о заказе в агрегаторах
 */
class OrderInfoForm extends Model
{
    /**
     * @var integer
     */
    public int $statusPaid;

    /**
     * @var string
     */
    public string $companyName;

    /**
     * @var string|integer
     */
    public $idOrderApi;

    /**
     * @var null|float
     */
    public ?float $price;

    /**
     * @var integer
     */
    public int $insuranceType;

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            [['idOrderApi', 'statusPaid', 'insuranceType'], 'required'],
            [['idOrderApi', 'companyName'], 'trim'],
            [['statusPaid', 'insuranceType'], 'integer'],
            [['price'], 'number'],
            [['statusPaid'], 'in', 'range' => array_keys(OrderInfoDictionary::STATUS_PAID_LIST)],
            [['insuranceType'], 'in', 'range' => array_keys(BaseDictionary::TYPES)]
        ];
    }
}