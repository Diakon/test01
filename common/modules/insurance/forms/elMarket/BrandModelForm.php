<?php
namespace common\modules\insurance\forms\elMarket;

use common\modules\insurance\dictionaries\inguru\BrandModelDictionary;
use common\models\Model;

/**
 * Информация об марке и модели автомобиля
 * Для получения марки авто передать в search первые буквы названия марки, а searchType = 1 (например, search=peug, а в searchType=1)
 * Для получения модели авто, в search передать первые буквы названия модели, в searchType = 2, а в brandName - название марки (например, brandName=peugeot; searchType=2; search=30)
 *
 * @property string $search
 * @property string $brandName
 * @property integer $searchType
 * @property integer $companyId
 *
 * Class ModelBrandInfo
 * @package common\modules\insurance\forms\inguru
 */
class BrandModelForm extends Model
{
    /**
     * Строка с данными для поиска
     *
     * @var string
     */
    public $search;

    /**
     * Марка транспортного средства в случае поиска модели. Обязательный параметр при поиске модели (q=model).
     *
     * @var string
     */
    public $brandName;

    /**
     * Тип поиска 1 - поиск марки, 2 - поиск модели
     *
     * @var integer
     */
    public $searchType;

    /**
     * ID страховой компании, если искать надо в словарях конкретной страховой
     *
     * @var integer
     */
    public $companyId;

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            [['searchType', 'search'], 'required'],
            [['search', 'brandName'], 'trim'],
            [['companyId'], 'integer'],
            [['brandName'], 'required', 'when' => function ($model) {
                return $model->searchType == BrandModelDictionary::SEARCH_TYPE_2;
            }],
            [['searchType'], 'in', 'range' => array_keys(BrandModelDictionary::SEARCH_TYPE_LIST)]
        ];
    }
}