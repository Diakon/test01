<?php
namespace common\modules\insurance\forms\inguru;

use common\models\Model;

/**
 * Информация об автомобиле по гос. номеру
 *
 * Class CarInfoForm
 *
 * @package common\modules\insurance\forms\inguru
 */
class CarInfoForm extends Model
{
    /**
     * Номерной знак автомобиля в формате а123яя10
     *
     * @var string
     */
    public $vehicleLicensePlate;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['vehicleLicensePlate'], 'required'],
            [['vehicleLicensePlate'], 'trim'],
            ['vehicleLicensePlate', 'validateCarNumber']
        ];
    }

    /**
     * Валидация ввода гос. номера
     *
     * @param $attribute
     * @param $params
     * @param $items
     */
    public function validateCarNumber($attribute, $params, $items)
    {
        if (!preg_match('/^[АВЕКМНОРСТУХ]\d{3}(?<!000)[АВЕКМНОРСТУХ]{2}\d{2,3}$/ui', $this->{$attribute})) {
            $this->addError($attribute, 'Ошибка ввода гос. номера. Введите номер в формате а123яя45');
        }
    }
}