<?php
namespace common\modules\insurance\forms\inguru;

use common\models\Model;

/**
 * Информация об оплате заказа в ИНГУРУ
 *
 * Class PaymentForm
 *
 * @package common\modules\insurance\forms\inguru;
 */
class PaymentForm extends Model
{
    /**
     * ID заказа в системе ИНГУРУ
     *
     * @var integer
     */
    public $eId;

    /**
     * URL для перехода после успешной оплаты
     *
     * @var string
     */
    public string $successUrl;

    /**
     * URL для перехода после не успешной оплаты
     *
     * @var string
     */
    public string $failUrl;

    /**
     * СМС код
     *
     * @var string
     */
    public string $smsCode;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['eId'], 'required'],
            [['eId', 'successUrl', 'failUrl', 'smsCode'], 'trim'],
            [['eId', 'successUrl', 'failUrl', 'smsCode'], 'safe']
        ];
    }
}