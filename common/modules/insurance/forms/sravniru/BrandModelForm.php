<?php
namespace common\modules\insurance\forms\sravniru;

use common\models\Model;
use common\modules\insurance\dictionaries\sravniru\BrandModelDictionary;

/**
 * Информация об марке/модели автомобиля
 *
 * Class BrandModelForm
 * @package common\modules\insurance\forms\sravniru
 *
 * @param int $searchType
 * @param int $brandId
 * @param int $modelId
 * @param int $yearFrom
 */
class BrandModelForm extends Model
{
    /**
     * Тип поиска - бренд или модель
     *
     * @var int
     */
    public $searchType;

    /**
     * Год с которгого возвращать бренды ТС
     *
     * @var int
     */
    public $yearFrom;

    /**
     * ID бренда
     *
     * @var int
     */
    public $brandId;

    /**
     * ID модели
     *
     * @var int
     */
    public $modelId;

    /**
     * @return array
     */
    public function rules() : array
    {
        return [
            [['typeSearch'], 'required'],
            [['brandId', 'modelId'], 'integer'],
            [['yearFrom'], 'integer', 'min' => 1950],
            [['yearFrom'], 'required', 'when' => function ($model) {
                return $model->searchType == BrandModelDictionary::TYPE_SEARCH_BRAND;
            }],
            [['brandId'], 'required', 'when' => function ($model) {
                return $model->searchType == BrandModelDictionary::TYPE_SEARCH_MODEL;
            }],
            [['brandId', 'modelId', 'yearFrom'], 'required', 'when' => function ($model) {
                return $model->searchType == BrandModelDictionary::TYPE_SEARCH_POWER;
            }],
        ];
    }
}