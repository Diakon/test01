<?php
namespace common\modules\insurance\forms\sravniru;

use common\models\Model;

/**
 * Форма для формирования запроса на получение информации о заказе в сравни.ру
 *
 * Class OrderInfoForm
 *
 * @package common\modules\insurance\forms\sravniru
 */
class OrderInfoForm extends Model
{
    /**
     * Дата заказов с
     * @var string
     */
    public string $dateFrom;

    /**
     * Дата заказов по
     * @var string
     */
    public string $dateTo;

    /**
     * Возвращать только оплаченные заказы
     * @var bool
     */
    public bool $isAccepted = true;

    /**
     * Токен заказа
     * @var string
     */
    public string $orderToken;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['orderToken', 'dateTo', 'dateFrom'], 'required'],
            [['orderToken'], 'trim'],
            ['orderToken', 'string'],
            [['dateFrom', 'dateTo'], 'date', 'format' => 'php:Y-m-d'],
            ['isAccepted', 'boolean'],
        ];
    }
}