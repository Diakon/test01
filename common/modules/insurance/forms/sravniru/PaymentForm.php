<?php
namespace common\modules\insurance\forms\sravniru;

use common\models\Model;

/**
 * Информация об оплате заказа в сравни.ру
 *
 * Class PaymentForm
 *
 * @package common\modules\insurance\forms\sravniru
 */
class PaymentForm extends Model
{
    /**
     * Дата заказов с
     * @var string
     */
    public string $dateFrom;

    /**
     * Дата заказов по
     * @var string
     */
    public string $dateTo;

    /**
     * Возвращать только оплаченные заказы
     * @var bool
     */
    public bool $isAccepted = true;

    /**
     * Токен заказа
     * @var string
     */
    public string $orderToken; // affSub3

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            [['orderToken', 'dateTo', 'dateFrom'], 'required'],
            [['orderToken'], 'trim'],
            ['orderToken', 'string'],
            [['dateFrom', 'dateTo'], 'date', 'format' => 'php:Y-m-d'],
            ['isAccepted', 'boolean'],
        ];
    }
}