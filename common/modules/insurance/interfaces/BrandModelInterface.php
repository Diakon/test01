<?php

namespace common\modules\insurance\interfaces;

use common\modules\cars\models\CarBrand;

/**
 * Интерфейс для работы с моделями / брендами авто.
 */
interface BrandModelInterface extends InsuranseBaseInterface
{
    /**
     * @return mixed
     */
    public function getBrandsList();

    /**
     * @param CarBrand $brand
     *
     * @return mixed
     */
    public function getModelsList(CarBrand $brand);
}