<?php

namespace common\modules\insurance\interfaces;

use common\modules\insurance\forms\InsuranceForm;
use common\modules\orders\models\Order;

/**
 * Интерфейс для работы с расчетами и получения предложений
 */
interface CalculationInterface extends InsuranseBaseInterface
{
    /**
     * Возвращает предложения от страховых компаний
     *
     * @param InsuranceForm $form
     * @param Order $order
     *
     * @return mixed
     */
    public function insuranceOffers(InsuranceForm $form, Order $order);
}