<?php

namespace common\modules\insurance\interfaces;

use common\modules\cars\models\CarBrand;

/**
 * Интерфейс для работы с информацией об автомобиле
 */
interface CarInfoInterface extends InsuranseBaseInterface
{
    /**
     * @return mixed
     */
    public function infoByNumber(string $carNumber);
}