<?php

namespace common\modules\insurance\interfaces;

use common\modules\orders\models\Order;

/**
 * Интерфейс для получения документов ОСАГО
 */
interface DocumentsOSAGOInterface extends InsuranseBaseInterface
{
    /**
     * @param Order $order
     * @return mixed
     */
    public function getDocuments(Order $order);
}