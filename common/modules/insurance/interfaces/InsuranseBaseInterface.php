<?php

namespace common\modules\insurance\interfaces;

/**
 * Интерфейс для работы с моделями / брендами авто.
 */
interface InsuranseBaseInterface
{
    /**
     * @return integer
     */
    public function getInsuranceServiceType();
}