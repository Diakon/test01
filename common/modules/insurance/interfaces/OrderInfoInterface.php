<?php

namespace common\modules\insurance\interfaces;

use common\modules\orders\models\Order;

/**
 * Интерфейс для получения информации о заказе в агрегаторе
 */
interface OrderInfoInterface extends InsuranseBaseInterface
{
    /**
     * @param Order $order
     * @return mixed
     */
    public function getOrderInfo(Order $order);
}