<?php

namespace common\modules\insurance\interfaces;

use common\modules\orders\models\Order;

/**
 * Интерфейс для работы с оплатами
 */
interface PaymentInterface
{
    /**
     * @param Order $order
     *
     * @return mixed
     */
    public function getOrderPayment(Order $order);
}