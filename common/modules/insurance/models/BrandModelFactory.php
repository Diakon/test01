<?php
namespace common\modules\insurance\models;

use common\modules\cars\dictionaries\CarBrandDictionary;
use common\modules\cars\dictionaries\ModuleDictionary;
use common\modules\cars\models\CarBrand;
use common\modules\cars\services\BrandService;
use common\modules\cars\services\ModelService;
use common\modules\insurance\forms\BrandsModelsInsuranceForm;
use yii\helpers\ArrayHelper;

/**
 * Фабрика для работы с брендами и моделями авто
 */
class BrandModelFactory extends InsuranceFactory
{
    /**
     * Бренды - получаю из АПИ сервисов
     *
     * @var BrandsModelsInsuranceForm[]
     */
    public array $brandsInsurance = [];

    /**
     * Модели - получаю из АПИ сервисов
     *
     * @var BrandsModelsInsuranceForm[]
     */
    public array $modelsInsurance = [];

    /**
     * Возвращает список брендов из сервисов страховых компаний
     *
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\httpclient\Exception
     */
    public function setBrandsFromInsurance(): void
    {
        $returnData = [];
        foreach ($this->getServices() as $service) {
            $insuranceType = $service->getInsuranceServiceType();
            if (!in_array($insuranceType, $this->activeInsureIds)) continue;   // Если этот сервис сейчас не используется - пропускаю
            foreach ($service->getBrandsList() as $brand) {
                if (empty($brand)) continue;
                $brandName = trim($brand['name']);
                $brandName = CarBrandDictionary::BRANDS[$brandName] ?? $brandName; // Иногда в разных агрегаторах один и тот же бренд называется по разному - привожу к общему имени
                $key = mb_strtolower($brandName);

                $form = new BrandsModelsInsuranceForm();
                $form->setScenario(BrandsModelsInsuranceForm::SCENARIO_BRANDS);
                $form->name = $brandName;
                $form->id = $brand['id'];
                $form->insuranceType = $insuranceType;
                if ($form->validate()) {
                    $returnData[$key] = $form;
                }
            }
        }
        $this->brandsInsurance = array_values($returnData);
    }

    /**
     * Возвращает список моделей от агрегаторов страховых компаний для бренда.
     * Бренд, для которого надо получить модели, передается в массиве $insureBrandIds - ключ, это ID типа страховой - значение, это ID бренда в этой страховой
     *
     * @param CarBrand $brand
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\httpclient\Exception
     */
    public function setModelsFromInsurance(CarBrand $brand): void
    {
        $returnData = [];

        foreach ($this->getServices() as $service) {
            $insuranceType = $service->getInsuranceServiceType();
            if (!in_array($insuranceType, $this->activeInsureIds)) continue;   // Если этот сервис сейчас не используется - пропускаю
            $models = $service->getModelsList($brand);
            if (!empty($models)) {
                foreach ($models as $model) {
                    $name = trim($model['name']);
                    $key = mb_strtolower($name);

                    $form = new BrandsModelsInsuranceForm();
                    $form->setScenario(BrandsModelsInsuranceForm::SCENARIO_MODELS);
                    $form->name = $name;
                    $form->id = $model['id'];
                    $form->insuranceType = $service->getInsuranceServiceType();
                    if ($form->validate()) {
                        $returnData[$key] = $form;
                    }
                }
            }
        }

        $this->modelsInsurance = array_values($returnData);
    }

    /**
     * Возвращает список брендов
     *
     * @return array|\yii\db\ActiveRecord|\yii\db\ActiveRecord[]|null
     */
    public function getBrands()
    {
        return BrandService::findByParams(['status' => ModuleDictionary::STATUS_ON], false, ['name' => SORT_ASC]);
    }

    /**
     * Возвращает список моделей для бренда
     *
     * @param int $brandId
     *
     * @return array|\yii\db\ActiveRecord|\yii\db\ActiveRecord[]|null
     */
    public function getModels(int $brandId)
    {
        return ModelService::findByParams(['brand_id' => $brandId, 'status' => ModuleDictionary::STATUS_ON], false, ['name' => SORT_ASC]);
    }
}