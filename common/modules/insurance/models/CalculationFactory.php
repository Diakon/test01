<?php
namespace common\modules\insurance\models;

use Yii;
use common\modules\insurance\forms\CalculationInsuranceForm;
use common\modules\insurance\forms\InsuranceForm;
use common\modules\orders\models\Order;

/**
 * Фабрика для расчета и получения предложений по введенным ранее параметрам оформления полиса ОСАГО
 */
class CalculationFactory extends InsuranceFactory
{
    /**
     * Предложения полученные от агрегаторов страховых компаний
     *
     * @var CalculationInsuranceForm[]
     */
    public array $offersInsurance = [];

    /**
     * @param InsuranceForm $form
     * @param Order $order
     */
    public function setOffers(InsuranceForm $form, Order $order): void
    {
        foreach ($this->getServices() as $service) {
            $insuranceType = $service->getInsuranceServiceType();
            if (!in_array($insuranceType, $this->activeInsureIds)) continue;   // Если этот сервис сейчас не используется - пропускаю

            try {
                $offers = $service->insuranceOffers($form, $order);
            } catch(\Exception $e) {
                Yii::error("Ошибка при получении предложений по заказу ID=$order->id для агрегатора с ID=$insuranceType. Ошибка: " . $e->getMessage());
                $offers = null;
            }

            if (empty($offers)) continue;
            foreach ($offers as $offer) {
                $this->offersInsurance[$offer->key] = $offer;
            }
        }
    }
}