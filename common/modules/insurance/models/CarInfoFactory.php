<?php
namespace common\modules\insurance\models;

use common\models\dictionaries\BaseDictionary;
use common\models\Model;
use common\modules\cars\models\Car;
use common\modules\cars\services\CarService;
use common\modules\insurance\traits\InsuranceTrait;

/**
 * Фабрика для работы с сервисами агрегаторов получения данных об авто мо гос. номеру
 */
class CarInfoFactory extends InsuranceFactory
{
    use InsuranceTrait;

    /**
     * @var Car
     */
    public ?Car $car = null;

    /**
     * Выставляет в $car модель Car. Если в таблице Car нет записи с таким гос. номером - ищет в сервисах АПИ
     *
     * @param string $number
     */
    public function setCarInfoByNumber(string $number): void
    {
        // Смотрю, есть ли данные об этом авто в таблице Car - если есть, отдаю их
        $model = CarService::getByNumber($number);
        if (!empty($model)) {
            $this->car = $model;
        } else {
            // Данных об авто с таким номером нет - тянем из сервисов
            foreach ($this->getServices() as $service) {
                $carInfo = $service->infoByNumber($number);
                if (!empty($carInfo)) {
                    // Обновляю, если такая машина есть, новыми данными
                    $model = CarService::getByNumber($number);
                    if (empty($model)) {
                        $model = new Car();
                    }
                    $model->setAttributes($carInfo);
                    $model->number = $number;
                    $service = new CarService($model);
                    if ($service->save()) {
                        $this->car = $service->car;
                    }
                }
            }
        }
    }
}