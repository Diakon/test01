<?php
namespace common\modules\insurance\models;

use common\modules\insurance\forms\DocumentsOSAGOForm;
use common\modules\insurance\traits\InsuranceTrait;
use common\modules\orders\models\Order;

/**
 * Фабрика для работы с документами ОСАГО
 */
class DocumentsOSAGOFactory extends InsuranceFactory
{
    use InsuranceTrait;

    /**
     * Форма с документами ОСАГО
     *
     * @var DocumentsOSAGOForm
     */
    public ?DocumentsOSAGOForm $documents;

    /**
     * Возврат документов ОСАГО
     *
     * @param Order $order
     */
    public function setDocuments(Order $order): void
    {
        foreach ($this->getServices() as $service) {
            $insuranceType = $service->getInsuranceServiceType();
            if (!in_array($insuranceType, $this->activeInsureIds)) continue;   // Если этот сервис сейчас не используется - пропускаю

            $documents = $service->getDocuments($order);
            if (empty($documents)) continue;

            $this->documents = $documents;
        }
    }
}