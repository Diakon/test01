<?php
namespace common\modules\insurance\models;

use common\models\dictionaries\BaseDictionary;
use common\models\Model;
use common\modules\insurance\traits\InsuranceTrait;

/**
 * Фабрика для работы с сервисами агрегаторов страховых компаний
 */
class InsuranceFactory extends Model
{
    use InsuranceTrait;

    /**
     * Используемые в системе сервисы агрегаторов страховых компаний
     *
     * @var array
     */
    protected array $activeInsureIds;

    /**
     * Сервисы API с которыми будет производиться работа
     *
     * @var array
     */
    public array $servicesApi = [];

    /**
     *
     */
    public function __construct()
    {
        // Список используемых сервисов
        $this->activeInsureIds = [
            BaseDictionary::TYPE_SRAVNIRU,
            BaseDictionary::TYPE_INGURU,
            BaseDictionary::TYPE_ELMARKET,
        ];

        parent::__construct();
    }

    /**
     * Бренды и модели авто из сервисов страховых компаний
     *
     * @return BrandModelFactory
     */
    public function brandModel(): BrandModelFactory
    {
        return new BrandModelFactory();
    }

    /**
     * Информация об автомобиле
     *
     * @return CarInfoFactory
     */
    public function carInfo(): CarInfoFactory
    {
        return new CarInfoFactory();
    }

    /**
     * Фабрика для работы с сервисами агрегаторов получения данных об авто мо гос. номеру
     *
     * @return CalculationFactory
     */
    public function calculation(): CalculationFactory
    {
        return new CalculationFactory();
    }

    /**
     * Фабрика для работы с оплатами полиса
     *
     * @return CalculationFactory
     */
    public function payment(): PaymentFactory
    {
        return new PaymentFactory();
    }

    /**
     * Фабрика возвращающая данные о заказе в агрегаторах
     *
     * @return OrderInfoFactory
     */
    public function orderInfo(): OrderInfoFactory
    {
        return new OrderInfoFactory();
    }

    /**
     * Фабрика для работы с документами полиса ОСАГО
     *
     * @return DocumentsOSAGOFactory
     */
    public function documentsOSAGO(): DocumentsOSAGOFactory
    {
        return new DocumentsOSAGOFactory();
    }
}