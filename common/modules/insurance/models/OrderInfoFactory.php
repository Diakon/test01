<?php
namespace common\modules\insurance\models;

use common\modules\insurance\dictionaries\OrderInfoDictionary;
use common\modules\insurance\forms\OrderInfoForm;
use common\modules\insurance\traits\InsuranceTrait;
use common\modules\orders\models\Order;

/**
 * Фабрика для получения информации о заказе в агрегаторах
 */
class OrderInfoFactory extends InsuranceFactory
{
    use InsuranceTrait;

    /**
     * Массив документов ОСАГО
     *
     * @var OrderInfoForm
     */
    public OrderInfoForm $orderInfo;

    /**
     * Возврат данных по статусе заказа в агрегаторе
     *
     * @param Order $order
     * @param bool $onlyPaid
     */
    public function setOrderInfo(Order $order, bool $onlyPaid  = false): void
    {
        foreach ($this->getServices() as $service) {
            $insuranceType = $service->getInsuranceServiceType();
            if (!in_array($insuranceType, $this->activeInsureIds)) continue;   // Если этот сервис сейчас не используется - пропускаю
            $orderInfo = $service->getOrderInfo($order);
            if (empty($orderInfo) || ($onlyPaid && $orderInfo->statusPaid != OrderInfoDictionary::STATUS_PAID_TRUE)) continue;

            $this->orderInfo = $orderInfo;
        }
    }
}