<?php
namespace common\modules\insurance\models;

use common\modules\insurance\traits\InsuranceTrait;
use common\modules\orders\models\Order;

/**
 * Фабрика для работы с оплатами
 */
class PaymentFactory extends InsuranceFactory
{
    use InsuranceTrait;

    /**
     * Массив данных по оплате заказа из Страховой компании
     *
     * @var array
     */
    public array $orderPayment;

    /**
     * Возврат данных по оплате заказа
     *
     * @param Order $order
     */
    public function setOrderPayment(Order $order): void
    {
        $returnData = [];
        foreach ($this->getServices() as $service) {
            $insuranceType = $service->getInsuranceServiceType();
            if (!in_array($insuranceType, $this->activeInsureIds)) continue;   // Если этот сервис сейчас не используется - пропускаю

            $orderPayment = $service->getOrderPayment($order);
            if (empty($orderPayment->paymentSuccess)) continue;

            $returnData[$insuranceType] = $orderPayment;
        }

        $this->orderPayment = array_values($returnData);
    }
}