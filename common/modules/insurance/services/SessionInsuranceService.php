<?php
namespace common\modules\insurance\services;

use common\models\Service;
use common\modules\insurance\forms\CalculationInsuranceForm;
use Yii;

class SessionInsuranceService extends Service
{
    const SESSION_NAME = 'insuranceOffersList';

    /**
     * Сохраняет в сессию предложения от страховых компаний, что бы после выбора предложения взять от туда цену, комиссию и тд
     *
     * @param array $offers
     */
    public static function setOffersData(array $offers): void
    {
        // Удаляю старые записи предложений из сессии
        if (!empty(Yii::$app->session->get(self::SESSION_NAME))) {
            Yii::$app->session->remove(self::SESSION_NAME);
        }
        // Пишу новые данные в сессию
        if (!empty($offers)) {
            Yii::$app->session->set(self::SESSION_NAME, serialize($offers));
        }
    }

    /**
     * Возвращает предложение из сессии
     *
     * @param string $offerKey
     *
     * @return CalculationInsuranceForm|null
     */
    public static function getOfferData(string $offerKey): ?CalculationInsuranceForm
    {

        $offers = Yii::$app->session->get(self::SESSION_NAME);
        $offers = !empty($offers) ? unserialize($offers) : [];

        return $offers[$offerKey] ?? null;
    }
}