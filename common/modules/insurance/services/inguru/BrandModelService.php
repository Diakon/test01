<?php
namespace common\modules\insurance\services\inguru;

use common\modules\cars\models\CarBrand;
use common\modules\insurance\dictionaries\inguru\BrandModelDictionary;
use common\modules\insurance\forms\inguru\BrandModelForm;
use common\modules\insurance\interfaces\BrandModelInterface;
use common\modules\insurance\traits\inguru\InguruTrait;
use yii\httpclient\Client;
use Yii;

/**
 * Сервис для получения данных брендов/моделей авто в ингуру
 *
 * Class BrandModelService
 * @package common\modules\insurance\services\inguru
 */
class BrandModelService extends InguruBaseService implements BrandModelInterface
{
    use InguruTrait;

    /**
     * @var BrandModelForm $form
     */
    private $form;

    /**
     * Метод подготавливает данные для отправки в АПИ ИНГУРУ
     *
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    protected function getApiData(): array
    {
        $typeSearch = $this->form->searchType == BrandModelDictionary::SEARCH_TYPE_1
            ? BrandModelDictionary::SEARCH_BRAND
            : BrandModelDictionary::SEARCH_MODEL;
        if (!empty($this->form->companyId)) {
            $typeSearch = $this->form->searchType == BrandModelDictionary::SEARCH_TYPE_1
                ? BrandModelDictionary::SEARCH_BRAND_IN_COMPANY
                : BrandModelDictionary::SEARCH_MODEL_IN_COMPANY;
        }

        $requestData = [];
        $requestData['q'] = $typeSearch;
        $requestData['search'] = $this->form->search;
        if ($this->form->searchType == BrandModelDictionary::SEARCH_TYPE_2) {
            $requestData['brand'] = $this->form->brandName;
        }
        if (!empty($this->form->companyId)) {
            $requestData['sk'] = $this->form->companyId;
        }

        return $requestData;
    }

    /**
     * Возвращает список марок авто
     *
     * @return array|null
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\httpclient\Exception
     */
    public function getBrandsList(): ?array
    {
        $result = [];
        $this->form = new BrandModelForm();
        $this->form->searchType = BrandModelDictionary::SEARCH_TYPE_1;
        $response = $this->cache();

        if (empty($response)) {
            $response = $this->request(
                BrandModelDictionary::API_BRAD_MODEL_URL,
                $this->getApiData(),
                Client::FORMAT_CURL,
                'get'
            );
            if ($response->isOk) {
                $response = $response->data;
                if (!empty($response['vehicles'])) {
                    // Пишу в кеш, что бы не делать постоянно запросы
                    $this->cache($response);
                }
            }
        }

        if (!empty($response['vehicles'])) {
            foreach ($response['vehicles'] as $brand) {
                if (empty($brand['brand'])) continue;
                $brandId = $brandName = trim($brand['brand']);
                $result[] = [
                    'id' => $brandId,
                    'name' => $brandName,
                ];
            }
        }

        return !empty($result) ? $result : null;
    }

    /**
     * Возвращает список моделей
     *
     * @param CarBrand $brand
     *
     * @return array|null
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\httpclient\Exception
     */
    public function getModelsList(CarBrand $brand): ?array
    {
        // Получаю ID бренда из модели CarBrand для этого агрегатора страховых компаний
        $brandApiIds = $brand->getApiIds();
        $brandId = $brandApiIds[$this->getInsuranceServiceType()] ?? null;

        if (empty($brandId)) return null;

        $this->form = new BrandModelForm();
        $this->form->searchType = BrandModelDictionary::SEARCH_TYPE_2;

        // Если токен есть в кеше - беру от туда json файл ИНГУРУ с брендами/моделями
        $response = $this->cache();
        if (empty($response)) {
            $response = $this->request(
                BrandModelDictionary::API_BRAD_MODEL_URL,
                $this->getApiData(),
                Client::FORMAT_CURL,
                'get'
            );
            if ($response->isOk) {
                $response = $response->data;
                if (!empty($response['vehicles'])) {
                    // Пишу в кеш, что бы не делать постоянно запросы
                    $this->cache($response);
                }
            }
        }

        if (!empty($response['vehicles'])) {
            $result = [];
            foreach ($response['vehicles'] as $brand) {
                if (empty($brand['brand']) || empty($brand['models'])) continue;
                $models = $brand['models'];
                $searchBrandId = trim($brand['brand']);
                if (!empty($searchBrandId) && $brandId != $searchBrandId) continue;
                foreach ($models as $model) {
                    $modelId = $modelName = trim($model['model']);
                    $result[] = [
                        'id' => $modelId,
                        'brandId' => $brandId,
                        'name' => $modelName,
                    ];
                }
            }
        }

        return !empty($result) ? $result : null;
    }

    /**
     * @param array|null $response
     * @return array|null
     */
    protected function cache(array $response = null)
    {
        $cacheKey = 'api-inguru-cars-brands-models-list-json-file';

        // Если $response != null - значит нужно обновить кеш. Если null - получить данные из кеша
        if (is_null($response)) return Yii::$app->cache->get($cacheKey);

        // $response != null - пишу в кеш
        if (!empty($response['vehicles'])) {
            // Пишу в кеш, что бы не делать постоянно запросы
            Yii::$app->cache->set($cacheKey, $response, 15 * 86400);  // Время жизни кеша 15 дней
        }

        return null;
    }
}