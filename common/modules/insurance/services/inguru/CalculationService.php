<?php
namespace common\modules\insurance\services\inguru;

use common\modules\cars\models\CarBrand;
use common\modules\cars\models\CarModel;
use common\modules\cars\services\BrandService;
use common\modules\cars\services\ModelService;
use common\modules\insurance\dictionaries\inguru\CalculationDictionary;
use common\modules\insurance\dictionaries\InsuranceFormDictionaries;
use common\modules\insurance\forms\CalculationInsuranceForm;
use common\modules\insurance\forms\InsuranceForm;
use common\modules\orders\models\Order;
use Yii;
use common\modules\insurance\traits\inguru\InguruTrait;
use yii\httpclient\Client;
use common\modules\insurance\interfaces\CalculationInterface;


/**
 * Сервис для расчета и получения предложений от ингуру
 *
 * Class CalculationService
 * @package common\modules\insurance\services\inguru
 */
class CalculationService extends InguruBaseService implements CalculationInterface
{
    use InguruTrait;

    /**
     * @var InsuranceForm $form
     */
    protected InsuranceForm $form;

    /**
     * @var Order $order
     */
    protected Order $order;

    /**
     * Возвращает URL ИНГУРУ для отправки запроса
     *
     * @return string
     */
    protected function getApiUrl(): string
    {
        return trim(Yii::$app->params['apiUrlInguru']);
    }

    /**
     * @return string
     */
    protected function getApiCurlFormat(): string
    {
        return Client::FORMAT_JSON;
    }

    /**
     * @return string
     */
    protected function getApiCurlMethod(): string
    {
        return 'post';
    }

    /**
     * Возвращает ID бренда в системе ИНГУРУ
     *
     * @return string|null
     */
    protected function getBrand(): ?string
    {
        /**
         * @var CarBrand $brand
         */
        $brand = BrandService::findByParams(['id' => $this->form->vehicleBrand]);
        if (empty($brand)) {
            return null;
        }
        $apiId = $brand->getApiIds();

        return (string)($apiId[$this->getInsuranceServiceType()] ?? '');
    }

    /**
     * Возвращает ID модели в системе ИНГУРУ
     *
     * @return string|null
     */
    protected function getModel(): ?string
    {
        /**
         * @var CarModel $model
         */
        $model = ModelService::findByParams(['id' => $this->form->vehicleModel]);
        if (empty($model)) {
            return null;
        }
        $apiId = $model->getApiIds();

        return (string)($apiId[$this->getInsuranceServiceType()] ?? '');
    }

    /**
     * Возвращает url оплаты
     *
     * @param string|int|null $eId
     * @return string|null
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\httpclient\Exception
     */
    protected function getPaymentLink($eId = null): ?string
    {
        $requestData = [];
        $requestData['q'] = 'payurl';
        $requestData['eId'] = $eId;

        $response = $this->request(
            $this->getApiUrl(),
            $requestData,
            Client::FORMAT_CURL,
            'get'
        );
        $responseResult = $response->isOk ? $response->data : null;

        return $responseResult['results'] ?? null;
    }

    /**
     * Метод собирающий данные с формы и подготавливающий их к отправке в ИНГУРУ
     *
     * @return array
     *
     * @throws \yii\base\InvalidConfigException
     */
    protected function getRequestData(): array
    {
        $requestData = [];

        $requestData['useTrailer'] = CalculationDictionary::USE_TRAILER_NO;  // ToDo обсудить, что указывать в Использует прицеп Да/Нет, если на форме его не указывают
        $requestData['vehicle'] = [
            'type' => CalculationDictionary::VEHICLE_TYPE_CAR, // ToDo обсудить, как будем получать тип ТС, если на форме его не указывают
            'power' => (int)$this->form->vehiclePower,
            'year' => (int)$this->form->vehicleYear,
        ];
        $requestData['multidrive'] = (int)$this->form->multiDrive;
        $requestData['licensePlate'] = $this->form->vehicleLicensePlate;

        $requestData['drivers'] = [];
        if (!empty($this->form->driverLastName)) {
            foreach ($this->form->driverLastName as $key => $value) {

                $requestData['drivers'][$key]['lastname'] = $value;
                $requestData['drivers'][$key]['middlename'] = $this->form->driverMiddleName[$key] ?? "";
                $requestData['drivers'][$key]['firstname'] = $this->form->driverFirstName[$key] ?? "";
                $requestData['drivers'][$key]['birthdate'] = !empty($this->form->driverBirthDate[$key])
                    ? \Yii::$app->formatter->asDate($this->form->driverBirthDate[$key], 'php:Y-m-d') : "";
                $requestData['drivers'][$key]['licenseSerial'] =  $this->form->driverLicenseSerial[$key] ?? "";
                $requestData['drivers'][$key]['licenseNumber'] = $this->form->driverLicenseNumber[$key] ?? "";
                $requestData['drivers'][$key]['licenseForeign'] = CalculationDictionary::DRIVER_LICENSE_TYPE_RU;
                $requestData['drivers'][$key]['lastname'] = $value;

                if (!empty($this->form->driverPrevLicenseSerial[$key])) {
                    foreach (
                        [
                            'prevLicenseSerial' => 'driverPrevLicenseSerial',
                            'prevLicenseNumber' => 'driverPrevLicenseNumber',
                            'prevLicenseDate' => 'driverPrevLicenseDate',
                            'prevLicenseLastname' => 'driverPrevLastName',
                            'prevLicenseFirstname' => 'driverPrevFirstName',
                            'prevLicenseMiddlename' => 'driverPrevMiddleName'
                        ] as $requestField => $driverPrevFieldName)
                    {
                        if (empty($this->form->{$driverPrevFieldName}[$key]))  {
                            $this->form->{$driverPrevFieldName}[$key] = null;
                        }
                        $value = $this->form->{$driverPrevFieldName}[$key];
                        switch ($driverPrevFieldName) {
                            case "driverPrevLicenseDate":
                                $value = \Yii::$app->formatter->asDate($value, 'php:Y-m-d');
                                break;
                        }
                        $requestData['drivers'][$key][$requestField] = $value;
                    }
                }
            }
        }

        foreach ($this->form->driverBirthDate as $key => $value) {
            $requestData['drivers'][$key]['age'] = (int)Yii::$app->formatter->getAgeByDate($value);
            $requestData['drivers'][$key]['exp'] = !empty($this->form->driversExp[$key]) ? (int)$this->form->driversExp[$key] : null;
        }

        if (!empty($this->form->prevPolicySerial) && !empty($this->form->prevPolicyNumber)) {
            $requestData['prevPolicySerial'] = (string)$this->form->prevPolicySerial;
            $requestData['prevPolicyNumber'] = (string)$this->form->prevPolicyNumber;
        }

        $requestData['owner'] = [];
        $requestData['owner']['city'] = (string)$this->form->ownerCity;

        // Поля для полного расчета
        $requestData['sk'] = array_keys(CalculationDictionary::INSURANCE_COMPANIES);  // ID компаний в которые рассылать запросы (если указаны - идет как полный расчет)

        $requestData['usePeriod'] = (int)$this->form->usePeriod;
        $requestData['date'] = $this->form->ownerDateOSAGOStart;

        // Выставляю выбранное значение для идентификации ТС (по VIN, кузову или шасси)
        foreach (['vehicleVin' => 'vin', 'vehicleBodyNum' => 'bodyNum', 'vehicleChassisNum' => 'chassisNum'] as $identityCarParam => $requestDataKeyName) {
            if (empty($this->form->{$identityCarParam})) continue;
            $requestData['vehicle'][$requestDataKeyName] = $this->form->{$identityCarParam};
        }

        $requestData['vehicle']['docType'] = (int)$this->form->vehicleDocType;
        $requestData['vehicle']['docSerial'] = (string)$this->form->vehicleDocSerial;
        $requestData['vehicle']['docNumber'] = (string)$this->form->vehicleDocNumber;
        $requestData['vehicle']['docDate'] = Yii::$app->formatter->asDate($this->form->vehicleDocDate, 'php:Y-m-d');
        $requestData['vehicle']['brand'] = $this->getBrand();
        $requestData['vehicle']['model'] = $this->getModel();
        $requestData['owner']['passportSerial'] = (string)$this->form->ownerPassportSerial;
        $requestData['owner']['passportNumber'] = (string)$this->form->ownerPassportNumber;
        $requestData['owner']['passportDate'] = Yii::$app->formatter->asDate($this->form->ownerPassportDate, 'php:Y-m-d');
        $requestData['owner']['lastname'] = (string)$this->form->ownerLastName;
        $requestData['owner']['firstname'] = (string)$this->form->ownerFirstName;
        $requestData['owner']['middlename'] = (string)$this->form->ownerMiddleName;
        $requestData['owner']['birthdate'] = Yii::$app->formatter->asDate($this->form->ownerBirthDate, 'php:Y-m-d');
        if ($this->form->ownerDadataAddress) {
            $search = Yii::$app->api->requestWithInfo('address/search/detail', 'GET', ['query' => trim($this->form->ownerDadataAddress)]);
            if (!empty($search['data']['items'])) {
                $requestData['owner']['dadata'] = current($search['data']['items']);
            }
        }
        $requestData['email'] = $this->form->ownerEmail;
        $requestData['purpose'] = CalculationDictionary::CAR_PURPOSE_PERSONAL; // ToDo обсудить, какой вид использования ТС указать, если на форме его не указывают
        $requestData['name'] =  $this->form->ownerFirstName;

        $requestData['insurerIsOwner'] = (int)$this->form->insurerIsOwner;
        if ((int)$this->form->insurerIsOwner == InsuranceFormDictionaries::ONE_FACE_NO) {
            $requestData['insurer']['lastname'] = $this->form->insurerLastName;
            $requestData['insurer']['firstname'] = $this->form->insurerFirstName;
            $requestData['insurer']['middlename'] = $this->form->insurerMiddleName;
            $requestData['insurer']['birthdate'] = Yii::$app->formatter->asDate($this->form->insurerBirthDate, 'php:Y-m-d');
            $requestData['insurer']['passportSerial'] = (string)$this->form->insurerPassportSerial;
            $requestData['insurer']['passportNumber'] = (string)$this->form->insurerPassportNumber;
            $requestData['insurer']['passportDate'] = Yii::$app->formatter->asDate($this->form->insurerPassportDate, 'php:Y-m-d');
            if ($this->form->insurerDadataAddress) {
                $search = Yii::$app->api->requestWithInfo('address/search/detail', 'GET', ['query' => trim($this->form->insurerDadataAddress)]);
                if (!empty($search['data']['items'])) {
                    $requestData['insurer']['dadata'] = current($search['data']['items']);
                }
            }
        }

        // Формирую номер телефона по шаблону +7 (999) 111-11-11
        $phone = preg_replace("/[^0-9]/", '', $this->form->ownerPhone);
        $requestData['phone'] = "+" . substr($phone, 0, 1)
            . " (" . substr($phone, 1, 3) . ") "
            . substr($phone, 4, 3) . "-"
            . substr($phone, 7, 2) . "-"
            . substr($phone, 8, 2);

        $requestData['date'] = \Yii::$app->formatter->asDate($this->form->ownerDateOSAGOStart, 'php:Y-m-d');

        foreach ($this->form->driverExpDate as $key => $driverExpDate) {
            $requestData['drivers'][$key]['expdate'] = Yii::$app->formatter->asDate($driverExpDate, 'php:Y-m-d');
        }

        return $requestData;
    }

    /**
     * Метод собирающий данные с формы и отправляющий в АПИ ИНГУРУ
     *
     * @param InsuranceForm $form
     * @param Order $order
     *
     * @return array
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\httpclient\Exception
     */
    public function insuranceOffers(InsuranceForm $form, Order $order): array
    {
        $returnData = [];
        $this->form = $form;
        $this->order = $order;

        $response = $this->request(
            $this->getApiUrl(),
            $this->getRequestData(),
            $this->getApiCurlFormat(),
            $this->getApiCurlMethod()
        );

        if ($response->isOk) {
            $response = $response->data;
            if (!empty($response['results'])) {
                foreach ($response['results'] as $result) {
                    $model = new CalculationInsuranceForm();
                    $model->calculateId = $result['eId'] ?? null;
                    $model->offerId = $result['eId'] ?? null;
                    $model->companyId = $result['sk'] ?? null;
                    $model->companyName = $result['skName'] ?? null;
                    $model->insuranceType = $this->getInsuranceServiceType();
                    $model->price = $result['total'] ?? null;
                    $model->commission = !empty($result['commission']) ? (float)$result['commission'] : null;
                    $model->coefficients = [];
                    $model->paymentUrl = $this->getPaymentLink($model->offerId);
                    $model->paymentToken = null;

                    if (!$model->validate()) {
                        continue;
                    }
                    $returnData[] = $model;
                }
            }
        }

        return $returnData;
    }
}