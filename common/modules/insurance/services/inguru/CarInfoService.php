<?php
namespace common\modules\insurance\services\inguru;

use common\modules\cars\models\CarBrand;
use common\modules\cars\models\CarModel;
use common\modules\cars\services\BrandService;
use common\modules\cars\services\ModelService;
use common\modules\insurance\forms\inguru\CarInfoForm;
use Yii;
use common\modules\insurance\interfaces\CarInfoInterface;
use common\modules\insurance\traits\inguru\InguruTrait;
use yii\httpclient\Client;

/**
 * Сервис для получения данных об авто из ИНГУРУ
 *
 * Class BrandModelService
 * @package common\modules\insurance\services\inguru
 */
class CarInfoService extends InguruBaseService implements CarInfoInterface
{
    use InguruTrait;

    protected CarInfoForm $form;

    /**
     * Возвращает URL ИНГУРУ для отправки запроса
     *
     * @return string
     */
    protected function getApiUrl(): string
    {
        return trim(Yii::$app->params['apiUrlInguru']);
    }

    /**
     * @return mixed|string
     */
    public function getApiCurlFormat(): string
    {
        return Client::FORMAT_CURL;
    }

    /**
     * @return string
     */
    protected function getApiCurlMethod(): string
    {
        return 'get';
    }

    /**
     * Метод возвращающий данные для отправки в АПИ ИНГУРУ
     *
     * @return array
     *
     * @throws \yii\base\InvalidConfigException
     */
    protected function getApiData(): array
    {
        $vehicleLicensePlate = preg_replace('/[^а-яё\d]/ui', '',  $this->form->vehicleLicensePlate);
        $vehicleLicensePlate = str_replace(' ', '', $vehicleLicensePlate);
        $vehicleLicensePlate = trim($vehicleLicensePlate);

        $requestData = [];
        $requestData['q'] = 'vehicle';
        $requestData['licensePlate'] = $vehicleLicensePlate;

        return $requestData;
    }

    /**
     * Возвращает массив данных об авто полученный из сравни.ру
     *
     * @param array $response
     *
     * @return array
     */
    protected function prepareDate(array $response): array
    {
        $response = $response['results'] ?? [];

        if (empty($response)) {
            return $response;
        }

        $docs = !empty($response['vehicle']['docs'][0]) ? $response['vehicle']['docs'][0] : [];

        $brandApiId = $response['vehicle']['brand'] ?? null;
        $modelApiId = $response['vehicle']['model'] ?? null;

        $brand = BrandService::findByTypeApiId($this->getInsuranceServiceType(), $brandApiId);
        // Нет такого бренда в БД - добавляю
        if (empty($brand) && !empty($brandApiId)) {
            $model = BrandService::findByParams(['like', 'name', $brandApiId, false]);
            if (empty($model)) {
                $model = new CarBrand();
            }
            $model->name = $brandApiId;
            $service = new BrandService($model);
            if (!$service->save([$this->getInsuranceServiceType() => $brandApiId])) {
                return [];
            }
            $brand = $service->model;
        }

        $model = ModelService::findByTypeApiId($brand->id, $this->getInsuranceServiceType(), $modelApiId);
        // Нет такой модели в БД - добавляю
        if (empty($model) && (!empty($modelApiId) && !empty($response['vehicle']['model']))) {
            $model = ModelService::findByParams(['name' => $response['vehicle']['model'], 'brand_id' => $brand->id]);
            if (empty($model)) {
                $model = new CarModel();
            }
            $model->name = $response['vehicle']['model'];
            $service = new ModelService($model);
            if ($service->save($brand->id, [$this->getInsuranceServiceType() => $modelApiId])) {
                $model = $service->model;
            }
        }

        return [
            'vin' => $response['vehicle']['vin'] ?? null,
            'body_num' => $response['vehicle']['bodyNum'] ?? null,
            'chassis_num' => $response['vehicle']['chassisNum'] ?? null,
            'year' => $response['vehicle']['year'] ?? null,
            'type' => $response['vehicle']['type'] ?? null,
            'doc_type' => $docs['docType'] ?? null,
            'doc_serial' => $docs['docSerial'] ?? null,
            'doc_number' => $docs['docNumber'] ?? null,
            'doc_date' => !empty($docs['docDate']) ? Yii::$app->formatter->asDate($docs['docDate'], 'php:Y-m-d') : null,
            'brand_id' => $brand->id,
            'model_id' => $model->id,
            'model_brand_doc' => $response['vehicle']['brand_model'],
            'diag_card_number' => $response['vehicle']['dc'],
            'diag_card_date' => !empty($response['vehicle']['dcDate']) ? Yii::$app->formatter->asDate($response['vehicle']['dcDate'], 'php:Y-m-d') : null,
            'diag_card_issue_date' => $response['vehicle']['dcIssueDate'] ?? null
        ];
    }

    /**
     * Возвращает данные об авто по гос. номеру из сервиса ИНГУРУ
     *
     * @param string $carNumber
     *
     * @return array
     */
    public function infoByNumber(string $carNumber): array
    {
        $form = new CarInfoForm();
        $form->vehicleLicensePlate = $carNumber;
        $this->form = $form;

        if ($this->form->validate()) {
            $response = $this->request(
                $this->getApiUrl(),
                $this->getApiData(),
                $this->getApiCurlFormat(),
                $this->getApiCurlMethod()
            );
            if ($response->isOk) {
                return $this->prepareDate($response->data);
            }
        }

        return [];
    }
}