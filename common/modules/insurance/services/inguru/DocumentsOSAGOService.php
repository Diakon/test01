<?php
namespace common\modules\insurance\services\inguru;

use common\modules\insurance\interfaces\DocumentsOSAGOInterface;
use common\modules\insurance\traits\inguru\InguruTrait;
use common\modules\orders\models\Order;
use Yii;
use yii\httpclient\Client;

/**
 * Сервис для получения документов ОСАГО в ИНГУРУ
 *
 * Class PaymentServices
 * @package common\modules\insurance\services\inguru
 */
class DocumentsOSAGOService extends InguruBaseService implements DocumentsOSAGOInterface
{
    use InguruTrait;

    /**
     * @var Order
     */
    protected Order $order;

    /**
     * Возвращает URL ИНГУРУ для отправки запроса
     *
     * @return string
     */
    protected function getApiUrl(): string
    {
        return trim(Yii::$app->params['apiUrlInguru']);
    }

    /**
     * @return string
     */
    protected function getApiCurlFormat(): string
    {
        return Client::FORMAT_CURL;
    }

    /**
     * @return string
     */
    protected function getApiCurlMethod(): string
    {
        return 'get';
    }

    /**
     * Метод возвращающий данные для отправки в АПИ ИНГУРУ
     *
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    protected function getApiData(): array
    {
        $requestData = [];
        $requestData['q'] = 'document';
        $requestData['eId'] = trim($this->order->propertyValue('calculateId'));

        return $requestData;
    }

    /**
     * @param Order $order
     *
     * @return array|null
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\httpclient\Exception
     */
    public function getDocuments(Order $order): ?array
    {
        $this->order = $order;
        $response = $this->request(
            $this->getApiUrl(),
            $this->getApiData(),
            $this->getApiCurlFormat(),
            $this->getApiCurlMethod()
        );

        if ($response->isOk) {
            $response = $response->data;
            if (empty($response['errors'])) {
                return $response;
            }
        }

        return null;
    }
}