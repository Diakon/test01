<?php
namespace common\modules\insurance\services\inguru;

use common\models\Service;
use common\modules\insurance\dictionaries\inguru\InguruDictionary;
use common\modules\insurance\interfaces\InsuranseBaseInterface;

/**
 * Class InguruBaseService
 *
 * @package common\modules\insurance\services\sravniru
 */
class InguruBaseService extends Service implements InsuranseBaseInterface
{
    /**
     * Возвращает ID сервиса-агрегатора страховок
     *
     * @return integer
     */
    public function getInsuranceServiceType(): int
    {
        return InguruDictionary::TYPE_INGURU;
    }
}