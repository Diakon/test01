<?php
namespace common\modules\insurance\services\inguru;

use Yii;
use common\modules\insurance\traits\inguru\InguruTrait;
use yii\httpclient\Client;


/**
 * Сервис для работы с КЛАДР
 *
 * Class KladrService
 * @package common\modules\insurance\services\inguru
 */
class KladrService extends InguruBaseService
{
    use InguruTrait;

    /**
     * Возвращает URL ИНГУРУ для отправки запроса
     *
     * @return string
     */
    protected function getApiUrl(): string
    {
        return trim(Yii::$app->params['apiUrlInguru']);
    }

    /**
     * Возвращает список городов найденый по названию в КЛАДР
     *
     * @param string $cityName
     *
     * @return array|mixed
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\httpclient\Exception
     */
    public static function getCityByName(string $cityName): array
    {
        $requestData = [];
        $requestData['q'] = 'kladr';
        $requestData['name'] = trim($cityName);

        $service = new self();
        $response = $service->request(
            $service->getApiUrl(),
            $requestData,
            Client::FORMAT_CURL,
            'get'
        );
        if ($response->isOk) {
            $response = $response->data;
        }

        return $response['results']['found'] ?? [];
    }
}