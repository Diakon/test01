<?php
namespace common\modules\insurance\services\inguru;

use Yii;
use common\modules\insurance\dictionaries\inguru\PaymentDictionary;
use common\modules\insurance\forms\OrderInfoForm;
use common\modules\insurance\interfaces\OrderInfoInterface;
use common\modules\insurance\traits\inguru\InguruTrait;
use common\modules\orders\models\Order;
use yii\httpclient\Client;
use common\modules\insurance\dictionaries\OrderInfoDictionary;

/**
 * Сервис для получения документов ОСАГО в ИНГУРУ
 *
 * Class OrderInfoService
 * @package common\modules\insurance\services\inguru
 */
class OrderInfoService extends InguruBaseService implements OrderInfoInterface
{
    use InguruTrait;

    /**
     * Возвращает URL ИНГУРУ для отправки запроса
     *
     * @return string
     */
    protected function getApiUrl(): string
    {
        return trim(Yii::$app->params['apiUrlInguru']);
    }

    /**
     * @return mixed|string
     */
    public function getApiCurlFormat()
    {
        return Client::FORMAT_CURL;
    }

    /**
     * @return string
     */
    public function getApiCurlMethod()
    {
        return 'get';
    }

    /**
     * Метод возвращающий данные для отправки в АПИ ИНГУРУ
     *
     * @param $eID
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    public function getApiData($eID): array
    {
        $requestData = [];
        $requestData['q'] = PaymentDictionary::INGURU_PAYMENT_CONS;
        $requestData['eId'] = $eID;

        return $requestData;
    }

    /**
     * Возвращает форму со статусом оплаты заказа
     *
     * @param Order $order
     *
     * @return OrderInfoForm|null
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\httpclient\Exception
     */
    public function getOrderInfo(Order $order): ?OrderInfoForm
    {
        $offerId = $order->propertyValue('offerId');
        if (empty($offerId)) return null;

        $response = $this->request(
            $this->getApiUrl(),
            $this->getApiData($offerId),
            $this->getApiCurlFormat(),
            $this->getApiCurlMethod()
        );
        if ($response->isOk) {
            $response = $response->data;
            $form = new OrderInfoForm();
            $form->statusPaid = !empty($response['results']) && in_array((int)$response['results']['state'], [PaymentDictionary::INGURU_STATUS_PAYMENT_SUCCESS])
                ? OrderInfoDictionary::STATUS_PAID_TRUE : OrderInfoDictionary::STATUS_PAID_FALSE;
            $form->companyName = "";
            $form->price = null;
            $form->insuranceType = $this->getInsuranceServiceType();
            $form->idOrderApi = md5('inguru-order-id-' . $order->id);

            if ($form->validate()) return $form;
        }

        return null;
    }
}