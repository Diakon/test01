<?php
namespace common\modules\insurance\services\inguru;

use common\modules\insurance\dictionaries\inguru\PaymentDictionary;
use common\modules\insurance\interfaces\PaymentInterface;
use common\modules\insurance\traits\inguru\InguruTrait;
use common\modules\orders\models\Order;
use common\modules\insurance\forms\inguru\PaymentForm;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\httpclient\Client;

/**
 * Сервис для работы с оплатами в ИНГУРУ
 *
 * Class PaymentServices
 * @package common\modules\insurance\services\inguru
 */
class PaymentServices extends InguruBaseService implements PaymentInterface
{
    use InguruTrait;

    /**
     * @var Order
     */
    protected Order $order;

    /**
     * Возвращает URL ИНГУРУ для отправки запроса
     *
     * @return string
     */
    protected function getApiUrl(): string
    {
        return trim(Yii::$app->params['apiUrlInguru']);
    }

    /**
     * @return string
     */
    protected function getApiCurlFormat(): string
    {
        return Client::FORMAT_CURL;
    }

    /**
     * @return string
     */
    protected function getApiCurlMethod(): string
    {
        return 'get';
    }

    /**
     * Метод возвращающий данные для отправки в АПИ ИНГУРУ
     *
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    protected function getApiData(): array
    {
        $requestData = [];

        $eId = $this->order->propertyValue('eId');

        $model = new PaymentForm();
        $model->eId = $eId;

        if (!$model->validate()) {
            return $requestData;
        }

        $requestData['q'] = PaymentDictionary::INGURU_PAYMENT_CONS;
        $requestData['eId'] = $model->eId;

        return $requestData;
    }

    /**
     * @param Order $order
     * @return array|false[]
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\httpclient\Exception
     */
    public function getOrderPayment(Order $order): array
    {
        $returnPaymentInfo = ['paymentSuccess' => false];

        $this->order = $order;
        $eId = $order->propertyValue('eId');

        if (empty($eId)) {
            Yii::error('Для заказа ' . $order->id . ' не возможно получить статус оплаты в ИНГУРУ - не указан eId', 'statusPaymentError');

            return $returnPaymentInfo;
        }

        $response = $this->request(
            $this->getApiUrl(),
            $this->getApiData(),
            $this->getApiCurlFormat(),
            $this->getApiCurlMethod()
        );

        if ($response->isOk) {
            $response = $response->data;
            if (!empty($response['errors'])) {
                $error = Json::encode($response['errors']);
                Yii::error('Для заказа ' . $order->id . ', при попытке получить статус оплаты, ИНГУРУ вернул ошибку:' . $error, 'statusPaymentError');

                return $returnPaymentInfo;
            }

            // Если заказ был оплачен - сохраняю статус заказа как  OrderDictionary::STATUS_COMPLETE и в bonuses
            if (!empty($response['results']) && in_array((int)$response['results']['state'], [PaymentDictionary::INGURU_STATUS_PAYMENT_SUCCESS])) {
                $returnPaymentInfo = ['paymentSuccess' => true, 'data' => $response['results']];
            }
        }

        return $returnPaymentInfo;
    }
}