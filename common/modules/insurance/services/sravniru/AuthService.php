<?php
namespace common\modules\insurance\services\sravniru;

use Yii;
use yii\base\ErrorException;
use yii\httpclient\Client;

/**
 * Сервис для авторизации с сравни.ру
 *
 * Class AuthService
 * @package common\modules\insurance\services\sravniru
 */
class AuthService extends SravniruBaseService
{
    /**
     * Возвращает URL для отправки запроса
     * @return string
     */
    public function getApiUrl()
    {
        $url = Yii::$app->params['authParamsSravniRu'][$this->isTestServer() ? 'test' : 'prod']['url'];

        return $url . '/connect/token';
    }

    /**
     * @return bool
     */
    protected function isTestServer(): bool
    {
        return !empty(Yii::$app->params['isTestApiSravniRu']);
    }

    /**
     * @return mixed|string
     */
    public function getApiCurlFormat()
    {
        return Client::FORMAT_RAW_URLENCODED;
    }

    /**
     * @return string
     */
    public function getApiCurlMethod()
    {
        return 'post';
    }

    /**
     * Метод возвращающий данные для отправки в АПИ sravni.ru
     *
     * @return array
     */
    public function getApiData()
    {
        list($grantType, $scope, $clientId, $clientSecret) = Yii::$app->params['authParamsSravniRu'][$this->isTestServer() ? 'test' : 'prod'];

        $requestData = [];
        $requestData['grant_type'] = $grantType;
        $requestData['scope'] = $scope;
        $requestData['client_id'] = $clientId;
        $requestData['client_secret'] = $clientSecret;

        return $requestData;
    }

    /**
     * Возвращает токен для авторизации
     *
     * @return mixed|string
     *
     * @throws ErrorException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\httpclient\Exception
     */
    public static function getToken()
    {
        $cacheKey = 'api-sraviru-auth-token';
        // Если токен есть в кеше - беру от туда
        $token = Yii::$app->cache->get($cacheKey);
        if (empty($token)) {
            $service = new self();
            $client = new Client();
            $response = $client->createRequest()
                ->setFormat($service->getApiCurlFormat())
                ->setMethod($service->getApiCurlMethod())
                ->setUrl($service->getApiUrl())
                ->setData($service->getApiData())
                ->send();
            if ($response->isOk) {
                $response = $response->data;
                $accessToken = $response['access_token'] ?? '';
                $tokenType = $response['token_type'] ?? '';
                $expiresIn = (int)$response['expires_in'] ?? 0;
                $expiresIn = $expiresIn - 300; // Время жизни кеша уменьшаю на 5 мин от времени действия токена
                $expiresIn = $expiresIn < 1 ? 1 : $expiresIn;
                $token = trim($tokenType . ' ' . $accessToken);
                // Пишу в кеш что бы не делать постоянно запросы
                Yii::$app->cache->set($cacheKey, $token, $expiresIn);
            }
        }

        if (empty($token)) {
            throw new ErrorException('Не удалось получить токен API для сравни.ру');
        }

        return $token;
    }
}
