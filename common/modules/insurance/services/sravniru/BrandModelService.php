<?php
namespace common\modules\insurance\services\sravniru;

use common\modules\cars\models\CarBrand;
use common\modules\insurance\dictionaries\sravniru\BrandModelDictionary;
use common\modules\insurance\forms\sravniru\BrandModelForm;
use common\modules\insurance\interfaces\BrandModelInterface;
use Yii;
use yii\httpclient\Client;

/**
 * Сервис для получения данных об авто из сравни.ру
 *
 * Class BrandModelService
 * @package common\modules\insurance\services\inguru
 */
class BrandModelService extends SravniruBaseService implements BrandModelInterface
{
    /**
     * @var $form BrandModelForm
     */
    private $form;

    /**
     * Возвращает URL для отправки запроса
     *
     * @return string
     */
    protected function getApiUrl(): string
    {
        switch ($this->form->searchType) {
            case BrandModelDictionary::TYPE_SEARCH_MODEL:
                return $this->host . 'auto/v1/brand/' . $this->form->brandId . '/models';
            case BrandModelDictionary::TYPE_SEARCH_POWER:
                return $this->host . 'auto/v1/brand/' . $this->form->brandId . '/years/' . $this->form->yearFrom . '/models/' .  $this->form->modelId . '/engine-powers';
            default:
                return $this->host . 'auto/v1/brands';
        }
    }

    /**
     * Метод возвращающий данные для отправки в АПИ sravni.ru
     *
     * @return array
     */
    protected function getApiData(): array
    {
        $requestData = [];
        if (!empty($this->form->yearFrom)) {
            $requestData[$this->form->searchType == BrandModelDictionary::TYPE_SEARCH_MODEL ? 'year' : 'yearFrom'] = (int)$this->form->yearFrom;
        }
        if (!empty($this->form->brandId)) {
            $requestData['brandId'] = $this->form->brandId;
        }
        if (!empty($this->formmodelId)) {
            $requestData['modelId'] = $this->form->modelId;
        }

        return $requestData;
    }

    /**
     * Делает запрос в АПИ сравни.ру
     *
     * @return \yii\httpclient\Response
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\httpclient\Exception
     */
    protected function request()
    {
        $client = new Client();
        return $client->createRequest()
            ->setFormat(Client::FORMAT_RAW_URLENCODED)
            ->setMethod('get')
            ->setUrl($this->getApiUrl())
            ->setData($this->getApiData())
            ->send();
    }

    /**
     * Возвращает бренды авто
     *
     * @return array|null
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\httpclient\Exception
     */
    public function getBrandsList(): ?array
    {
        $cacheKey = 'api-sraviru-car-brands-list';
        // Если токен есть в кеше - беру от туда
        $brandsList = Yii::$app->cache->get($cacheKey);
        if (empty($brandsList)) {
            $this->form = new BrandModelForm();
            $this->form->searchType = BrandModelDictionary::TYPE_SEARCH_BRAND;
            $this->form->yearFrom = 1950;
            $response = $this->request();
            if ($response->isOk) {
                $brandsList = $response->data;
                // Пишу в кеш, что бы не делать постоянно запросы
                Yii::$app->cache->set($cacheKey, $brandsList, 15 * 86400);  // Время жизни кеша 15 дней
            }
        }

        return !empty($brandsList) ? $brandsList : null;
    }

    /**
     * Возвращает модели авто
     *
     * @param CarBrand $brand
     *
     * @return array|null
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\httpclient\Exception
     */
    public function getModelsList(CarBrand $brand): ?array
    {
        // Получаю ID бренда из модели CarBrand для этого агрегатора страховых компаний
        $brandApiIds = $brand->getApiIds();
        $brandId = $brandApiIds[$this->getInsuranceServiceType()] ?? null;
        if (empty($brandId)) return null;

        $cacheKey =  "api-sraviru-car-id-$brandId-models-list";
        // Если токен есть в кеше - беру от туда
        $modelsList = Yii::$app->cache->get($cacheKey);
        if (empty($modelsList)) {
            $this->form = new BrandModelForm();
            $this->form->brandId = $brandId;
            $this->form->searchType = BrandModelDictionary::TYPE_SEARCH_MODEL;
            $response = $this->request();
            if ($response->isOk) {
                $modelsList = $response->data;
                // Пишу в кеш, что бы не делать постоянно запросы
                Yii::$app->cache->set($cacheKey, $modelsList, 15 * 86400);  // Время жизни кеша 15 дней
            }
        }

        return !empty($modelsList) ? $modelsList : null;
    }
}