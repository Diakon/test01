<?php
namespace common\modules\insurance\services\sravniru;

use common\modules\cars\models\CarBrand;
use common\modules\cars\models\CarModel;
use common\modules\cars\services\BrandService;
use common\modules\cars\services\ModelService;
use common\modules\insurance\forms\CalculationInsuranceForm;
use common\modules\insurance\traits\sravniru\RequestApiTrait;
use common\modules\orders\models\Order;
use common\modules\insurance\dictionaries\InsuranceFormDictionaries;
use common\modules\insurance\forms\InsuranceForm;
use common\modules\insurance\interfaces\CalculationInterface;
use yii\httpclient\Client;

/**
 * Сервис для расчета и получения предложений сравни.ру
 *
 * Class CalculationService
 * @package common\modules\insurance\services\sravniru
 */
class CalculationService extends SravniruBaseService implements CalculationInterface
{
    use RequestApiTrait;

    /**
     * @var InsuranceForm $form
     */
    protected InsuranceForm $form;

    /**
     * @var Order $order
     */
    protected Order $order;

    /**
     * Возвращает URL получения предложений от страховых компаний
     *
     * @return string
     */
    protected function getFullCalculateApiUrl(): string
    {
        return $this->host . 'osago/v1/calcthroughorders';
    }

    /**
     * Возвращает URL sravni.ru для отправки запроса (предварительные данные)
     *
     * @return string
     */
    protected function getPreCalculateApiUrl(): string
    {
        return $this->host . 'osago/v1/calculate';
    }

    /**
     * @return mixed|string
     */
    protected function getApiCurlFormat(): string
    {
        return Client::FORMAT_RAW_URLENCODED;
    }

    /**
     * @return string
     */
    protected function getApiCurlMethod(): string
    {
        return 'post';
    }

    /**
     * Возвращает ID бренда в системе сравни.ру
     *
     * @return int|null
     */
    protected function getBrand(): ?int
    {
        /**
         * @var CarBrand $brand
         */
        $brand = BrandService::findByParams(['id' => $this->form->vehicleBrand]);
        if (empty($brand)) {
            return null;
        }
        $apiIds = $brand->getApiIds();

        return $apiIds[$this->getInsuranceServiceType()] ?? 0;
    }

    /**
     * Возвращает ID модели в системе сравни.ру
     *
     * @return int|null
     */
    protected function getModel(): ?int
    {
        /**
         * @var CarModel $model
         */
        $model = ModelService::findByParams(['id' => $this->form->vehicleModel]);
        if (empty($model)) {
            return null;
        }
        $apiId = $model->getApiIds();

        return $apiId[$this->getInsuranceServiceType()] ?? 0;
    }

    /**
     * Метод возвращающий данные для отправки в sravni.ru. В $tokenOrder передать токен заказа
     *
     * @param false $returnAsJson
     *
     * @return array|false|mixed|string
     * @throws \yii\base\InvalidConfigException
     */
    protected function getApiData(bool $returnAsJson = false)
    {
        $requestData = [];

        $requestData['brandId'] = (int)$this->getBrand();
        $requestData['modelId'] = (int)$this->getModel();
        $requestData['year'] = (int)$this->form->vehicleYear;
        $requestData['enginePower'] = (int)$this->form->vehiclePower;
        $requestData['getting'] = $requestData['registration'] = $this->form->ownerCityName;
        $requestData['policyStartDate'] = \Yii::$app->formatter->asDate($this->form->ownerDateOSAGOStart, 'php:Y-m-d');
        $requestData['carNumber'] = (string)$this->form->vehicleLicensePlate;
        $isMultiDrive = !empty($this->form->multiDrive) ? InsuranceFormDictionaries::IS_MULTI_DRIVE_YES : InsuranceFormDictionaries::IS_MULTI_DRIVE_NO;
        $requestData['driversInfo'] = [];
        $requestData['driversInfo']['driverAmount'] = $isMultiDrive;
        if ($isMultiDrive == InsuranceFormDictionaries::IS_MULTI_DRIVE_NO) {
            $drivers = [];
            foreach ($this->form->driverLastName as $key => $value) {
                $drivers[] = [
                    'lastName' => $value,
                    'middleName' => $this->form->driverMiddleName[$key] ?? "",
                    'firstName' => $this->form->driverFirstName[$key] ?? "",
                    'birthDate' => \Yii::$app->formatter->asDate($this->form->driverBirthDate[$key], 'php:Y-m-d'),
                    'license' => [
                        'series' => $this->form->driverLicenseSerial[$key] ?? null,
                        'number' => $this->form->driverLicenseNumber[$key] ?? null,
                        'date' => !empty($this->form->driverExpDate[$key]) ? \Yii::$app->formatter->asDate($this->form->driverExpDate[$key], 'php:Y-m-d') : null,
                    ],
                    // Предыдущее водительское удостоверение
                    'previousInfo' => !empty($this->form->driverPrevAddInfo[$key]) ? [
                        'lastName' => $this->form->driverPrevLastName[$key] ?? "",
                        'middleName' => $this->form->driverPrevMiddleName[$key] ?? "",
                        'firstName' => $this->form->driverPrevFirstName[$key] ?? "",
                        'date' => !empty($this->form->driverPrevLicenseDate[$key]) ? \Yii::$app->formatter->asDate($this->form->driverPrevLicenseDate[$key], 'php:Y-m-d') : null,
                        'number' => $this->form->driverPrevLicenseNumber[$key] ?? null,
                        'series' => $this->form->driverPrevLicenseSerial[$key] ?? null,
                    ] : null,
                ];
            }
            $requestData['driversInfo']['drivers'] = array_values($drivers);
        }
        if (!empty($this->form->vehicleVin)) {$requestData['vin'] = $this->form->vehicleVin;}
        if (!empty($this->form->vehicleBodyNum)) {$requestData['bodyNumber'] = $this->form->vehicleBodyNum;}
        if (!empty($this->form->vehicleChassisNum)) {$requestData['chassisNumber'] = $this->form->vehicleChassisNum;}

        // Для полного расчета
        if (empty($this->form->preCalculation)) {
            // Данные о владельце ТС
            $requestData['owner'] = [
                'lastName' => $this->form->ownerLastName ?? "",
                'firstName' => $this->form->ownerFirstName ?? "",
                'middleName' => $this->form->ownerMiddleName ?? "",
                'birthDate' => !empty($this->form->ownerBirthDate) ? \Yii::$app->formatter->asDate($this->form->ownerBirthDate, 'php:Y-m-d') : null,
                'phone' => !empty($this->form->ownerPhone) ? preg_replace("/[^0-9]/", '', $this->form->ownerPhone) : null,
                'email' => $this->form->ownerEmail ?? "",
                'registrationAddress' => $this->form->ownerDadataAddress ?? "",
                // Паспорт
                'passport' => [
                    'series' => !empty($this->form->ownerPassportSerial) ? preg_replace("/[^0-9]/", '', $this->form->ownerPassportSerial) : "",
                    'number' => !empty($this->form->ownerPassportNumber) ? preg_replace("/[^0-9]/", '', $this->form->ownerPassportNumber) : "",
                    'issueDate' => !empty($this->form->ownerPassportDate) ? \Yii::$app->formatter->asDate($this->form->ownerPassportDate, 'php:Y-m-d') : null,
                    'issuedBy' => $this->form->ownerPassportIssuedBy ?? "",
                ],
            ];

            // Данные о страхователе ТС
            $insurerLastName = !empty($this->form->insurerIsOwner) ? $this->form->ownerLastName : $this->form->insurerLastName;
            $insurerFirstName = !empty($this->form->insurerIsOwner) ? $this->form->ownerFirstName : $this->form->insurerFirstName;
            $insurerMiddleName = !empty($this->form->insurerIsOwner) ? $this->form->ownerMiddleName : $this->form->insurerMiddleName;
            $insurerBirthDate = !empty($this->form->insurerIsOwner)
                ? (!empty($this->form->ownerBirthDate) ? \Yii::$app->formatter->asDate($this->form->ownerBirthDate, 'php:Y-m-d') : null)
                : (!empty($this->form->insurerBirthDate) ? \Yii::$app->formatter->asDate($this->form->insurerBirthDate, 'php:Y-m-d') : null);
            $insurerRegistrationAddress = !empty($this->form->insurerIsOwner) ? $this->form->ownerDadataAddress : $this->form->insurerDadataAddress;
            $insurerPassportSeries = !empty($this->form->insurerIsOwner)
                ? (!empty($this->form->ownerPassportSerial) ? preg_replace("/[^0-9]/", '', $this->form->ownerPassportSerial) : "")
                : (!empty($this->form->insurerPassportSerial) ? preg_replace("/[^0-9]/", '', $this->form->insurerPassportSerial) : "");
            $insurerPassportNumber = !empty($this->form->insurerIsOwner)
                ? (!empty($this->form->ownerPassportNumber) ? preg_replace("/[^0-9]/", '', $this->form->ownerPassportNumber) : "")
                : (!empty($this->form->insurerPassportNumber) ? preg_replace("/[^0-9]/", '', $this->form->insurerPassportNumber) : "");
            $insurerPassportIssueDate = !empty($this->form->insurerIsOwner)
                ? (!empty($this->form->ownerPassportDate) ? \Yii::$app->formatter->asDate($this->form->ownerPassportDate, 'php:Y-m-d') : null)
                : (!empty($this->form->insurerPassportDate) ? \Yii::$app->formatter->asDate($this->form->insurerPassportDate, 'php:Y-m-d') : null);
            $insurerPassportIssuedBy = !empty($this->form->insurerIsOwner) ? $this->form->ownerPassportIssuedBy : $this->form->insurerPassportIssuedBy;
            $phone = !empty($this->form->insurerIsOwner)
                ? (!empty($this->form->ownerPhone) ? preg_replace("/[^0-9]/", '', $this->form->ownerPhone) : null)
                : (!empty($this->form->insurerPhone) ? preg_replace("/[^0-9]/", '', $this->form->insurerPhone) : null);
            $email = !empty($this->form->insurerIsOwner)
                ? (!empty($this->form->ownerEmail) ? trim($this->form->ownerEmail) : "")
                : (!empty($this->form->insurerEmail) ? trim($this->form->insurerEmail) : "");

            $requestData['insurer'] = [
                'lastName' => $insurerLastName ?? "",
                'firstName' => $insurerFirstName ?? "",
                'middleName' => $insurerMiddleName ?? "",
                'birthDate' => $insurerBirthDate,
                'phone' => $phone,
                'email' => $email,
                'registrationAddress' => $insurerRegistrationAddress ?? "",
                // Паспорт
                'passport' => [
                    'series' => $insurerPassportSeries,
                    'number' => $insurerPassportNumber,
                    'issueDate' => $insurerPassportIssueDate,
                    'issuedBy' => $insurerPassportIssuedBy,
                ],
            ];

            // Документы на авто
            $requestData['carDocument'] = [];
            $requestData['carDocument']['series'] = (string)$this->form->vehicleDocSerial;
            $requestData['carDocument']['number'] = (string)$this->form->vehicleDocNumber;
            $requestData['carDocument']['date'] = !empty($this->form->vehicleDocDate) ? \Yii::$app->formatter->asDate($this->form->vehicleDocDate, 'php:Y-m-d') : null;
            $requestData['carDocument']['documentType'] = (int)$this->form->vehicleDocType;
        }

        $requestData['partner'] = [
            'affSub3' => $this->order->token,
            'affSub4' => null,
            'affSub5' => null,
            'medium' => 'api',
            'source' => 'offagent.5652663',
            'campaign' => 'api_osago',
            'agentRegion' => '7700000000000',
        ];
        $requestData['platform'] = 'api';
        $requestData['usageMonthsPerYear'] = !empty($this->form->usePeriod) ? (int)$this->form->usePeriod : InsuranceFormDictionaries::MONTH_10;

        return $returnAsJson ? json_encode($requestData, JSON_UNESCAPED_UNICODE) : $requestData;
    }

    /**
     * Запрашивает расчет по ID расчета (ID в sravni.ru)
     *
     * @param string $id
     * @param bool $isPreCalculation
     *
     * @return array
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\httpclient\Exception
     */
    protected function getDetailCalculationById(string $id, bool $isPreCalculation = false): array
    {
        $url = ($isPreCalculation ? $this->getPreCalculateApiUrl() : $this->getFullCalculateApiUrl()) . '/' . $id;
        $client = new Client();
        $response = $client->createRequest()
            ->setFormat($this->getApiCurlFormat())
            ->setMethod('get')
            ->setUrl($url)
            ->send();

        return ['success' => $response->isOk, 'response' => $response->data];
    }

    /**
     * Выполняет ID предварительного расчета сравни.ру
     *
     * @return array
     *
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\httpclient\Exception
     */
    protected function preCalculationId(): array
    {
        // Формирую запрос вы сравни.ру
        $response = $this->request(
            $this->getPreCalculateApiUrl(),
            $this->getApiData(),
            $this->getApiCurlFormat(),
            $this->getApiCurlMethod()
        );

        $status = $response->isOk;
        $response = $response->data;

        return ['success' => $status, 'id' => $response];
    }

    /**
     * Возвращает ID полного расчета сравни.ру
     *
     * @return array
     * @throws \yii\base\ErrorException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\httpclient\Exception
     */
    protected function fullCalculationId(): array
    {
        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_URL => $this->getFullCalculateApiUrl(),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => $this->getApiData(true),
            CURLOPT_HTTPHEADER => [
                'Authorization: ' . $this->getAuthToken(),
                'Content-Type: application/json'
            ],
        ]);
        $response = curl_exec($curl);
        curl_close($curl);
        if (empty($response) || (is_string($response) && strripos($response, 'error') !== false) || !empty($response['error'])) {
            return ['success' => false, 'id' => $response];
        }

        return ['success' => true, 'id' => $response];
    }

    /**
     *  Возвращает результат предварительного расчета
     *
     * @param InsuranceForm $form
     *
     * @param Order $order
     * @return array
     */
    public function insuranceOffers(InsuranceForm $form, Order $order): array
    {
        $returnData = [];
        $this->form = $form;
        $this->order = $order;
        $isPreCalculation = !empty($this->form->preCalculation);
        $request = $isPreCalculation ? $this->preCalculationId() : $this->fullCalculationId();
        $requestId = !empty($request['success']) ? $request['id'] : false;
        if ($requestId) {
            // Получаю по ID расчета данные пока isCompleted == false каждые 5 сек (пробую максимум 1 минуту)
            for ($i = 0; $i < 15; ++$i) {
                $result = $this->getDetailCalculationById($requestId, $isPreCalculation);
                if (!empty($result['response']['isCompleted'])) {
                    break;
                }
                sleep(5);
            }
        }

        if (empty($result) || empty($result['success']) || empty($result['response']['osagoCalculateThroughOrderResults'])) return $returnData;

        foreach ($result['response']['osagoCalculateThroughOrderResults'] as $offer) {
            $model = new CalculationInsuranceForm();
            $model->calculateId = $result['response']['searchId'] ?? null;
            $model->offerId = $offer['id'] ?? null;
            $model->companyId = $offer['companyId'] ?? null;
            $model->companyName = $offer['provider']['fullName'] ?? null;
            $model->paymentUrl = $offer['paymentUrl'] ?? null;
            $model->insuranceType = $this->getInsuranceServiceType();
            $model->price = !empty($offer['price']) ? (float)$offer['price'] : null;
            $model->commission = !empty($offer['partnerProfit']['profit']) ? (float)$offer['partnerProfit']['profit'] : null;
            $model->paymentToken = $offer['hash'] ?? null;
            $model->coefficients = $offer['coefficients'] ?? [];
            if (!$model->validate()) {
                continue;
            }
            $returnData[] = $model;
        }


        return $returnData;
    }
}