<?php
namespace common\modules\insurance\services\sravniru;

use common\modules\cars\dictionaries\CarDictionary;
use common\modules\cars\models\CarBrand;
use common\modules\cars\models\CarModel;
use common\modules\cars\services\BrandService;
use common\modules\cars\services\ModelService;
use common\modules\insurance\forms\sravniru\CarInfoForm;
use common\modules\insurance\interfaces\CarInfoInterface;
use common\modules\insurance\traits\sravniru\RequestApiTrait;
use yii\httpclient\Client;

/**
 * Сервис для получения данных об авто из сравни.ру
 *
 * Class BrandModelService
 * @package common\modules\insurance\services\sravniru
 */
class CarInfoService extends SravniruBaseService implements CarInfoInterface
{
    use RequestApiTrait;

    /**
     * @var $form CarInfoForm
     */
    private CarInfoForm $form;

    /**
     * Возвращает URL для отправки запроса
     *
     * @return string
     */
    protected function getApiUrl(): string
    {
        return $this->host . 'prt/v1/autoinfo/number';
    }

    /**
     * @return string
     */
    protected function getApiCurlFormat(): string
    {
        return Client::FORMAT_URLENCODED;
    }

    /**
     * @return string
     */
    protected function getApiCurlMethod(): string
    {
        return 'get';
    }

    /**
     * Метод возвращающий данные для отправки в АПИ sravni.ru
     *
     * @return array
     */
    protected function getApiData(): array
    {
        $requestData = [];
        $requestData['number'] = trim($this->form->vehicleLicensePlate);

        return $requestData;
    }

    /**
     * Возвращает массив данных об авто полученный из сравни.ру
     *
     * @param array $response
     *
     * @return array
     */
    protected function prepareDate(array $response): array
    {
        $response = $response['result'] ?? [];

        if (empty($response)) {
            return $response;
        }
        $category = $response['category'] ?? null;
        $category = array_search($category, CarDictionary::CATEGORY_CARS);
        $docs = $response['carDocument'] ?? [];
        $brandApiId = $response['brand']['id'] ?? null;
        $modelApiId = $response['model']['id'] ?? null;
        $brand = BrandService::findByTypeApiId($this->getInsuranceServiceType(), $brandApiId);
        // Нет такого бренда в БД - добавляю
        if (empty($brand) && (!empty($brandApiId) && !empty($response['brand']['name']))) {
            $model = new CarBrand();
            $model->name = $response['brand']['name'];
            $service = new BrandService($model);
            if (!$service->save([$this->getInsuranceServiceType() => $brandApiId])) {
                return [];
            }
            $brand = $service->model;
        }

        $model = ModelService::findByTypeApiId($brand->id, $this->getInsuranceServiceType(), $modelApiId);

        // Нет такой модели в БД - добавляю
        if (empty($model) && (!empty($modelApiId) && !empty($response['model']['name']))) {
            $model = new CarModel();
            $model->name = $response['model']['name'];
            $service = new ModelService($model);
            if ($service->save($brand->id, [$this->getInsuranceServiceType() => $modelApiId])) {
                $model = $service->model;
            }
        }

        return [
            'vin' => $response['vin'] ?? null,
            'body_umber' => $response['bodyNumber'] ?? null,
            'chassis_num' => $response['chassisNumber'] ?? null,
            'year' => $response['year'] ?? null,
            'power' => $response['power'] ?? null,
            'category' => $category,
            'doc_type' => CarDictionary::TYPES_DOCUMENTS_BY_ALIAS[$docs['documentType'] ?? null] ?? null,
            'doc_serial' => $docs['series'] ?? null,
            'doc_number' => $docs['number'] ?? null,
            'brand_id' => $brand->id,
            'model_id' => $model->id,
        ];
    }

    /**
     * Возвращает данные об авто по гос. номеру из сервиса сравни.ру
     *
     * @param string $carNumber
     *
     * @return array
     */
    public function infoByNumber(string $carNumber): array
    {
        $form = new CarInfoForm();
        $form->vehicleLicensePlate = $carNumber;
        $this->form = $form;

        if ($this->form->validate()) {
            $response = $this->request(
                $this->getApiUrl() . "/" . urlencode($this->form->vehicleLicensePlate),
                $this->getApiData(),
                $this->getApiCurlFormat(),
                $this->getApiCurlMethod()
            );
            if ($response->isOk) {
                return $this->prepareDate($response->data);
            }
        }

        return [];
    }
}