<?php
namespace common\modules\insurance\services\sravniru;

use common\modules\insurance\dictionaries\OrderInfoDictionary;
use common\modules\insurance\forms\DocumentsOSAGOForm;
use common\modules\insurance\interfaces\DocumentsOSAGOInterface;
use common\modules\insurance\traits\sravniru\RequestApiTrait;
use common\modules\orders\models\Order;
use yii\httpclient\Client;

/**
 * Сервис для получения документов ОСАГО в сравни.ру
 *
 * Class PaymentServices
 * @package common\modules\insurance\services\sravniru
 */
class DocumentsOSAGOService extends SravniruBaseService implements DocumentsOSAGOInterface
{
    use RequestApiTrait;

    /**
     * Возвращает URL для отправки запроса
     *
     * @return string
     */
    protected function getApiUrl(): string
    {
        return $this->host . 'osago/v1.0/ordertehinfo/';
    }

    /**
     * @return string
     */
    protected function getApiCurlFormat(): string
    {
        return Client::FORMAT_URLENCODED;
    }

    /**
     * @return string
     */
    protected function getApiCurlMethod(): string
    {
        return 'get';
    }

    /**
     * @return string
     */
    protected function documentDownloadHostUrl(): string
    {
        return "https://www.sravni.ru/";
    }

    /**
     * @param Order $order
     *
     * @return DocumentsOSAGOForm|null
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\httpclient\Exception
     */
    public function getDocuments(Order $order): ?DocumentsOSAGOForm
    {
        $orderInfoService = new OrderInfoService();
        $orderInfo = $orderInfoService->getOrderInfo($order);
        if (empty($orderInfo) || $orderInfo->statusPaid != OrderInfoDictionary::STATUS_PAID_TRUE || empty($orderInfo->idOrderApi)) return null;

        $response = $this->request(
            $this->getApiUrl() . $orderInfo->idOrderApi,
            [],
            $this->getApiCurlFormat(),
            $this->getApiCurlMethod()
        );
        $response = $response->data;
        if (!empty($response['isAccepted'])) {

            $form = new DocumentsOSAGOForm();
            $form->policyOSAGOLink = !empty($response['policyBlankLink']) ? ($this->documentDownloadHostUrl() . $response['policyBlankLink']) : null;
            $form->otherLink = !empty($response['policyLink']) ? ($this->documentDownloadHostUrl() . $response['policyLink']) : null;
            $form->insuranceType = $this->getInsuranceServiceType();

            if ($form->validate()) return $form;
        }

        return null;
    }
}