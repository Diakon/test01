<?php
namespace common\modules\insurance\services\sravniru;

use common\modules\insurance\dictionaries\OrderInfoDictionary;
use common\modules\insurance\forms\OrderInfoForm;
use common\modules\insurance\forms\sravniru\OrderInfoForm as OrderInfoFormSravniRu;
use common\modules\insurance\interfaces\OrderInfoInterface;
use common\modules\insurance\traits\sravniru\RequestApiTrait;
use common\modules\orders\models\Order;
use Yii;
use yii\httpclient\Client;

/**
 * Сервис для получения документов ОСАГО в сравни.ру
 *
 * Class OrderInfoService
 * @package common\modules\insurance\services\sravniru
 */
class OrderInfoService extends SravniruBaseService implements OrderInfoInterface
{
    use RequestApiTrait;

    /**
     * @var OrderInfoFormSravniRu
     */
    protected OrderInfoFormSravniRu $form;

    /**
     * Возвращает URL для отправки запроса
     *
     * @return string
     */
    protected function getApiUrl(): string
    {
        return $this->host . 'osago/v1.0/reports/orders';
    }

    /**
     * @return mixed|string
     */
    protected function getApiCurlFormat(): string
    {
        return Client::FORMAT_URLENCODED;
    }

    /**
     * @return string
     */
    protected function getApiCurlMethod(): string
    {
        return 'get';
    }

    /**
     * Метод возвращающий данные для отправки в АПИ sravni.ru
     * @return array
     */
    public function getApiData(): array
    {
        $params = \Yii::$app->params;
        $requestData = [];
        $requestData['affSub3'] = $this->form->orderToken;
        $requestData['isAccepted'] = $this->form->isAccepted ? "true" : "false";
        if (!empty($this->form->dateFrom)) {
            $requestData['dateFrom'] = $this->form->dateFrom;
        }
        if (!empty($this->form->dateTo)) {
            $requestData['dateTo'] = $this->form->dateTo;
        }
        $requestData['encryptId'] = $params['encryptIdSravniRu'];
        $requestData['partnerId'] = $params['partnerIdSravniRu'];

        return $requestData;
    }

    /**
     * @param Order $order
     *
     * @return OrderInfoForm|null
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\httpclient\Exception
     */
    public function getOrderInfo(Order $order): ?OrderInfoForm
    {
        $dateFrom = Yii::$app->formatter->asDate($order->created_at, 'php:Y-m-d');
        $dateFrom = Yii::$app->formatter->asDate(strtotime($dateFrom . '-2 days'), 'php:Y-m-d');
        $dateTo = Yii::$app->formatter->asDate(strtotime('+1 day'), 'php:Y-m-d');

        $form = new OrderInfoFormSravniRu();
        $form->orderToken = $order->token;
        $form->isAccepted = true;
        $form->dateFrom = $dateFrom;
        $form->dateTo = $dateTo;
        if (!$form->validate()) {
            return null;
        }
        $this->form = $form;

        $params = $this->getApiData();
        $url = $this->getApiUrl() . '?';
        foreach ($params as $key => $value) {
            $url .= "&$key=$value";
        }
        $url = str_replace("?&", "?", $url);

        $response = $this->request(
            $url,
            $params,
            $this->getApiCurlFormat(),
            $this->getApiCurlMethod()
        );

        if ($response->isOk && !empty($response = $response->data)) {
            $response = $response[0];
            $form = new OrderInfoForm();
            $form->statusPaid = $response['status'] == 'paid' ? OrderInfoDictionary::STATUS_PAID_TRUE : OrderInfoDictionary::STATUS_PAID_FALSE;
            $form->companyName = $response['companyName'] ?? "";
            $form->price = (float)$response['price'] ?? 0;
            $form->insuranceType = $this->getInsuranceServiceType();
            $form->idOrderApi = $response['hash'];

            if ($form->validate()) return $form;
        }

        return null;
    }
}