<?php
namespace common\modules\insurance\services\sravniru;

use common\modules\insurance\forms\sravniru\PaymentForm;
use common\modules\insurance\interfaces\PaymentInterface;
use common\modules\insurance\traits\sravniru\RequestApiTrait;
use common\modules\orders\models\Order;
use Yii;
use yii\httpclient\Client;

/**
 * Сервис для работы с оплатами в сравни.ру
 *
 * Class PaymentServices
 * @package common\modules\insurance\services\sravniru
 */
class PaymentServices extends SravniruBaseService implements PaymentInterface
{
    use RequestApiTrait;

    /**
     * @var PaymentForm
     */
    protected PaymentForm $form;

    /**
     * Возвращает URL для отправки запроса
     *
     * @return string
     */
    protected function getApiUrl(): string
    {
        return $this->host . 'osago/v1.0/reports/orders';
    }

    /**
     * @return mixed|string
     */
    protected function getApiCurlFormat(): string
    {
        return Client::FORMAT_URLENCODED;
    }

    /**
     * @return string
     */
    protected function getApiCurlMethod(): string
    {
        return 'get';
    }

    /**
     * Метод возвращающий данные для отправки в АПИ sravni.ru
     *
     * @return array
     */
    public function getApiData(): array
    {
        $params = Yii::$app->params;
        $requestData = [];
        $requestData['affSub3'] = $this->form->orderToken;
        $requestData['isAccepted'] = $this->form->isAccepted ? "true" : "false";
        if (!empty($this->form->dateFrom)) {
            $requestData['dateFrom'] = $this->form->dateFrom;
        }
        if (!empty($this->form->dateTo)) {
            $requestData['dateTo'] = $this->form->dateTo;
        }
        $requestData['encryptId'] = $params['encryptIdSravniRu'];
        $requestData['partnerId'] = $params['partnerIdSravniRu'];

        return $requestData;
    }

    /**
     * Возвращает массив данных о заказе по $hash заказа из сравни.ру
     * @param string $hash
     * @return mixed
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\httpclient\Exception
     */
    protected function getOrderInfo(string $hash)
    {
        $response = $this->request(
            $this->host . 'osago/v1.0/ordertehinfo/' . $hash,
            [],
            $this->getApiCurlFormat(),
            $this->getApiCurlMethod()
        );

        return $response->data;
    }

    /**
     * @param Order $order
     * @return array|false[]
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\httpclient\Exception
     */
    public function getOrderPayment(Order $order): array
    {
        $returnPaymentInfo = ['paymentSuccess' => false];

        $dateFrom = Yii::$app->formatter->asDate($order->created_at, 'php:Y-m-d');
        $dateFrom = Yii::$app->formatter->asDate(strtotime($dateFrom . '-2 days'), 'php:Y-m-d');
        $dateTo = !empty($dateTo) ? $dateTo : Yii::$app->formatter->asDate(strtotime('+1 day'), 'php:Y-m-d');

        $model = new PaymentForm();
        $model->orderToken = $order->token;
        $model->isAccepted = true;
        $model->dateFrom = $dateFrom;
        $model->dateTo = $dateTo;
        if (!$model->validate()) {
            return $returnPaymentInfo;
        }

        $params =  $this->getApiData();
        $url = $this->getApiUrl() . '?';
        foreach ($params as $key => $value) {
            $url .= "&$key=$value";
        }
        $url = str_replace("?&", "?", $url);

        $response = $this->request(
            $url,
            $params,
            $this->getApiCurlFormat(),
            $this->getApiCurlMethod()
        );

        $status = $response->isOk;
        $response = $response->data ?? [];
        if (empty($response)) {
            $status = false;
        }

        return ['paymentSuccess' => $status, 'data' => $response ?? []];
    }
}