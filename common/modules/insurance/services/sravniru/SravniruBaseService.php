<?php
namespace common\modules\insurance\services\sravniru;

use common\modules\insurance\dictionaries\inguru\InguruDictionary;
use common\modules\insurance\dictionaries\sravniru\SravniruDictionary;
use common\modules\insurance\interfaces\InsuranseBaseInterface;
use Yii;
use common\models\Service;

/**
 * Class SravniruBaseService
 *
 * @package common\modules\insurance\services\sravniru
 * @param string $host
 */
class SravniruBaseService extends Service implements InsuranseBaseInterface
{
    /**
     * Возвращает ID сервиса-агрегатора страховок
     *
     * @return integer
     */
    public function getInsuranceServiceType(): int
    {
        return InguruDictionary::TYPE_SRAVNIRU;
    }

    /**
     * Возвращает арес сервера куда следует слать запросы в зависимости от настройки
     *
     * @return string
     */
    public function getHost(): string
    {
        return !empty(Yii::$app->params['isTestApiSravniRu']) ? SravniruDictionary::TEST_HOST : SravniruDictionary::PROD_HOST;
    }

}