<?php
namespace common\modules\insurance\traits;

use Yii;

/**
 * Трейт модуля insurance
 */
trait InsuranceTrait
{
    /**
     * Возвращает сервисы АПИ агрегаторов страховок для фабрики
     *
     * @return array
     */
    public function getServices(): array
    {
        $di = Yii::$container->get(get_class($this));

        return $di->servicesApi;
    }

}