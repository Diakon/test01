<?php

namespace common\modules\insurance\traits\elMarket;

use Yii;
use yii\httpclient\Client;
use yii\httpclient\Response;

/**
 * Трейт для работы с АПИ ElMarket
 */
trait ElMarketTrait
{
    /**
     * Отправляет curl запрос
     *
     * @param string $url
     * @param string $token
     * @param array $data
     * @param string $format
     * @param string $method
     *
     * @return \yii\httpclient\Response
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\httpclient\Exception
     */
    public function request(string $url, array $data, string $format, string $method): Response
    {
        $client = new Client();
        return $client->createRequest()
            ->setFormat($format)
            ->setMethod($method)
            ->setUrl($url)
            ->addHeaders(['Authorization' => $this->getToken(), 'Content-Type' => 'application/x-www-form-urlencoded'])
            ->setData($data)
            ->send();
    }

    /**
     * Возвращает токен для авторизации в inguru по логину и паролю
     *
     * @return string
     */
    protected function getToken(): string
    {
        $time = time();
        $age = 60 * 60 * 24;
        $hash = md5(Yii::$app->params['loginInguru'] . ':' . $time . ':' . $age . ':' . hash('sha256', md5(Yii::$app->params['passwordInguru'])));

        return "INGURU " . (base64_encode(implode(':', [hash('sha256', Yii::$app->params['loginInguru']), $time, $age, $hash])));
    }


}