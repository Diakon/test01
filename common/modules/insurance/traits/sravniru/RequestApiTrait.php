<?php

namespace common\modules\insurance\traits\sravniru;

use common\modules\insurance\services\sravniru\AuthService;
use yii\httpclient\Client;
use yii\httpclient\Response;

trait RequestApiTrait
{
    /**
     * Отправляет curl запрос
     *
     * @param string $url
     * @param array $data
     * @param string $format
     * @param string $method
     *
     * @return \yii\httpclient\Response
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\httpclient\Exception
     */
    public function request(string $url, array $data, string $format, string $method): Response
    {
        $client = new Client();
        return $client->createRequest()
            ->setFormat($format)
            ->setMethod($method)
            ->setUrl($url)
            ->addHeaders($this->getHeader())
            ->setData($data)
            ->send();
    }

    /**
     * Возвращает заголовок для отправки в АПИ сравни.ру
     *
     * @param string $contentType
     *
     * @return array
     * @throws \yii\base\ErrorException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\httpclient\Exception
     */
    public function getHeader(string $contentType = 'application/x-www-form-urlencoded'): array
    {
        return ['Authorization' => $this->getAuthToken(), 'Content-Type' => $this->getContentType($contentType)];
    }

    /**
     * @return mixed|string
     * @throws \yii\base\ErrorException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\httpclient\Exception
     */
    public function getAuthToken(): string
    {
        return AuthService::getToken();
    }

    /**
     * @param string $contentType
     *
     * @return string
     */
    public function getContentType(string $contentType): string
    {
        return $contentType;
    }
}