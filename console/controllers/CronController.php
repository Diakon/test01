<?php

namespace console\controllers;

use common\modules\bonuses\dictionaries\BonusDictionary;
use common\modules\bonuses\models\Bonus;
use common\modules\bonuses\services\BonusService;
use common\modules\cars\models\CarBrand;
use common\modules\cars\models\CarModel;
use common\modules\cars\services\BrandService;
use common\modules\cars\services\ModelService;
use common\modules\insurance\dictionaries\OrderInfoDictionary;
use common\modules\insurance\models\InsuranceFactory;
use common\modules\orders\dictionaries\OrderDictionary;
use common\modules\orders\models\Order;
use common\modules\orders\services\OrderPropertyService;
use Yii;
use yii\bootstrap\BaseHtml;
use yii\console\Controller;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use common\components\mailer\traits\SendMailTrait;

/**
 * Class CronController
 * @package console\controllers
 */
class CronController extends Controller
{
    use SendMailTrait;

    /**
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\httpclient\Exception
     */
    public function actionCarModelBrand()
    {
        // Обновляю список брендов
        $model = (new InsuranceFactory())->brandModel();
        $model->setBrandsFromInsurance();
        foreach ($model->brandsInsurance as $carBrand) {
            $model = BrandService::findByParams(['name' => $carBrand->name]);
            if (!$model) {
                $model = new CarBrand();
            }
            $model->name = $carBrand->name;
            $service = new BrandService($model);

            if (!$service->save([$carBrand->insuranceType => $carBrand->id])) {
                Yii::error('Ошибка при сохранении бренда' . $carBrand->name . ':' . Json::encode($service->model->errors));
            }
        }
        // Обновляю список моделей
        /**
         * @var CarBrand $brand
         */
        foreach (BrandService::findByParams([], false, ['id' => SORT_ASC]) as $brand) {
            $model = (new InsuranceFactory())->brandModel();
            $model->setModelsFromInsurance($brand);
            $modelsList = $model->modelsInsurance;
            if (empty($modelsList)) {
                continue;
            }
            foreach ($modelsList as $carModel) {
                $model = ModelService::findByParams(['brand_id' => $brand->id, 'name' => $carModel->name]);
                if (!$model) {
                    $model = new CarModel();
                }
                $model->name = $carModel->name;
                $service = new ModelService($model);
                if (!$service->save($brand->id, [$carModel->insuranceType => $carModel->id])) {
                    Yii::error('Ошибка при сохранении модели для бренда' . $brand->id . ':' . Json::encode($service->model->errors));
                }
            }
        }
    }

    /**
     * Делает запрос в страховую компанию и получает статус оплаты заказа. Если оплачен - начисляет бонусы
     *
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\httpclient\Exception
     */
    public function actionCheckBonuses()
    {
        // Получаю id заказов, которые успешно обработаны и которым были начислены бонусы
        $ordersIds = ArrayHelper::map(
            Bonus::find()
                ->select('order_id')
                ->where(['status_send_store' => BonusDictionary::STATUS_SEND_STORE_SUCCESS])
                ->asArray()
                ->all(), 'order_id', 'order_id');

        // Проходим по всем заказам которые в статусе "оплачен" или "ожидает оплаты" и по ним еще не было начислений бонусов (нет в $ordersIds)
        $orders = Order::find()->where(['in', 'status', [OrderDictionary::STATUS_COMPLETE, OrderDictionary::STATUS_AWAITING_PAYMENT]]);
        if (!empty($ordersIds)) {
            $orders->andWhere(['not in', 'id', $ordersIds]);
        }
        $orders->orderBy(['id' => SORT_DESC]);

        /**
         * @var $order Order
         */
        foreach ($orders->each(100) as $order) {
            if (empty($order->commission) || empty($order->price)) {
                Yii::error('Для заказа ' . $order->id . ' не указан размер комиссии и/или стоимость полиса', 'statusPaymentError');
                continue;
            }
            $model = (new InsuranceFactory())->orderInfo();
            $model->setOrderInfo($order, true); // Получаем только оплаченные заказы

            if (empty($model->orderInfo)) {
                continue;
            }

            // Если все верно - начисля бонусы
            $transaction = Yii::$app->db->beginTransaction();
            $needCommit = false;
            try {
                $order->status = OrderDictionary::STATUS_COMPLETE;
                if ($order->save()) {
                    $bonusService = new BonusService();
                    if ($bonusService->save($order)) {
                        BonusService::sendStoreBonus($bonusService->bonus);
                        $transaction->commit();
                        $needCommit = true;

                    }
                } else {
                    throw new \yii\web\HttpException(500, BaseHtml::errorSummary($order));
                }
            } catch (\Throwable $e) {
                $transaction->rollBack();
                Yii::error('При работе крона cron/check-bonuses возникла ошибка начислении бонусов за заказ id=' . $order->id . '. Сообщение валидатора: ' . $e->getMessage());
            }

            if (!$needCommit) {
                Yii::error('При работе крона cron/check-bonuses возникла неизвестная ошибка начислении бонусов за заказ id=' . $order->id);
                $transaction->rollBack();
            } else {
                // Отправлю я письма с PDF полиса страхователю и владельцу
                $this->osagoSuccsessPaymentMail($order);
            }
        }
    }
}