<?php

use yii\db\Migration;

/**
 * Class m210401_173416_add_field_token_to_orders
 */
class m210401_173416_add_field_token_to_orders extends Migration
{
    const TABLE_NAME = 'orders';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(self::TABLE_NAME, 'token', $this->string(250)->null());
        $this->createIndex('IND_' . self::TABLE_NAME . '_token', self::TABLE_NAME, 'token', true);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn(self::TABLE_NAME, 'token');
    }
}
