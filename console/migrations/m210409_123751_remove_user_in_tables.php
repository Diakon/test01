<?php

use yii\db\Migration;

/**
 * Class m210409_123751_remove_user_in_tables
 */
class m210409_123751_remove_user_in_tables extends Migration
{
    const TABLE_NAME = 'orders';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropForeignKey('FK_' . self::TABLE_NAME . '_user_id', self::TABLE_NAME);
        $this->createIndex('IND_'. self::TABLE_NAME . '_user_id', self::TABLE_NAME, 'user_id');
        $this->dropTable('user');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),
            'username' => $this->string()->notNull()->unique(),
            'auth_key' => $this->string(32)->notNull(),
            'password_hash' => $this->string()->notNull(),
            'password_reset_token' => $this->string()->unique(),
            'email' => $this->string()->notNull()->unique(),

            'status' => $this->smallInteger()->notNull()->defaultValue(10),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->addForeignKey('FK_' . self::TABLE_NAME . '_user_id', self::TABLE_NAME, 'user_id', 'user', 'id', 'CASCADE');
    }
}
