<?php

use yii\db\Migration;

/**
 * Class m210713_153254_help_page
 */
class m210713_153254_help_page extends Migration
{
    const TABLE_NAME = 'description_page';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(self::TABLE_NAME, [
            'id' => $this->primaryKey(),
            'page_type_id' => $this->tinyInteger(3)->notNull(),
            'description' => 'LONGTEXT',
            'status' => $this->tinyInteger(13)->notNull(),
            'updated_at' => $this->integer(),
            'created_at' => $this->integer()
        ]);
        $this->createIndex('IND_' . self::TABLE_NAME . '_page_type_id', self::TABLE_NAME, 'page_type_id', true);
        $this->createIndex('IND_' . self::TABLE_NAME . '_status', self::TABLE_NAME, 'status');

        Yii::$app->db->getSchema()->refresh();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable(self::TABLE_NAME);
        Yii::$app->db->getSchema()->refresh();
    }
}
