<?php

use yii\db\Migration;

/**
 * Class m210714_170420_create_table_faq
 */
class m210714_170420_create_table_faq extends Migration
{
    const TABLE_NAME = 'faq';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(self::TABLE_NAME, [
            'id' => $this->primaryKey(),
            'name' => $this->string(256)->notNull(),
            'description' => 'LONGTEXT',
            'status' => $this->tinyInteger(1)->notNull(),
            'updated_at' => $this->integer(),
            'created_at' => $this->integer()
        ]);
        $this->createIndex('IND_' . self::TABLE_NAME . '_status', self::TABLE_NAME, 'status');

        Yii::$app->db->getSchema()->refresh();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable(self::TABLE_NAME);
        Yii::$app->db->getSchema()->refresh();
    }
}
