<?php

use common\modules\users\services\UserService;
use yii\db\Migration;
use common\modules\orders\models\Order;
use common\modules\users\models\UserInfo;
use common\modules\users\services\UserInfoService;

/**
 * Class m210715_082222_create_table_users
 */
class m210715_082222_create_table_users extends Migration
{
    const TABLE_NAME = 'users_info';
    const TABLE_ORDER = 'orders';
    const TABLE_BONUS = 'bonuses';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(self::TABLE_NAME, [
            'id' => $this->integer()->notNull(),
            'login' => $this->string(256)->notNull(),
            'email' => $this->string(256),
            'phone' => $this->string(15),
            'updated_at' => $this->integer(),
            'created_at' => $this->integer()
        ]);
        $this->createIndex('IND_' . self::TABLE_NAME . '_id', self::TABLE_NAME, 'id', true);
        $this->createIndex('IND_' . self::TABLE_NAME . '_login', self::TABLE_NAME, 'login');
        $this->createIndex('IND_' . self::TABLE_NAME . '_email', self::TABLE_NAME, 'email');
        $this->createIndex('IND_' . self::TABLE_NAME . '_phone', self::TABLE_NAME, 'phone');

        Yii::$app->db->getSchema()->refresh();

        // Заполняю данными таблицу UserInfo по заказам в Orders и Bonuses
        $service = new UserService();
        $userIds = \yii\helpers\ArrayHelper::map(
            Order::find()
                ->select('user_id')
                ->groupBy('user_id')
                ->asArray()->all(), 'user_id', 'user_id');
        $userIds = \yii\helpers\ArrayHelper::merge(
            $userIds,
            \yii\helpers\ArrayHelper::map(\common\modules\bonuses\models\Bonus::find()
                ->select('user_id')
                ->groupBy('user_id')
                ->asArray()->all(), 'user_id', 'user_id')
        );

        foreach ($userIds as $userId) {
            $profile = $service->getUserProfileById($userId);
            if (empty($profile)) continue;

            // Обновляю данные в UserInfo
            UserInfoService::saveByParam($userId,
                [
                    'login' => $profile['login'],
                    'email' => $profile['profile']['email'] ?? null,
                    'phone' => $profile['personalData']['phone'] ?? null,
                ]);

            sleep(5);
        }

        Yii::$app->db->getSchema()->refresh();
        $this->addForeignKey("FK_" . self::TABLE_ORDER . '_user_id', self::TABLE_ORDER, 'user_id', self::TABLE_NAME, 'id', 'CASCADE');
        $this->addForeignKey("FK_" . self::TABLE_BONUS . '_user_id', self::TABLE_BONUS, 'user_id', self::TABLE_NAME, 'id', 'CASCADE');


        Yii::$app->db->getSchema()->refresh();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey("FK_" . self::TABLE_ORDER . '_user_id', self::TABLE_ORDER);
        $this->dropForeignKey("FK_" . self::TABLE_BONUS . '_user_id', self::TABLE_BONUS);
        Yii::$app->db->getSchema()->refresh();

        $this->dropTable(self::TABLE_NAME);
        Yii::$app->db->getSchema()->refresh();
    }
}
