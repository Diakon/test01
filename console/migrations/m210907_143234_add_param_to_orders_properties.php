<?php

use common\modules\orders\models\OrderProperty;
use common\modules\orders\dictionaries\OrderDictionary;
use common\modules\orders\models\Order;
use common\modules\orders\models\PaymentProperty;
use common\modules\orders\services\OrderPropertyService;
use common\modules\sravniru\dictionaries\OsagoSravniRuFormDictionary;
use yii\db\Migration;

/**
 * Class m210907_143234_add_param_to_orders_properties
 */
class m210907_143234_add_param_to_orders_properties extends Migration
{
    const TABLE_NAME = 'orders_properties';
    const KEY = 'paymentUrlAvailableDateTime';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $serviceProperty = new OrderPropertyService();
        $orders = Order::find()->where(['status' => OrderDictionary::STATUS_AWAITING_PAYMENT]);
        foreach ($orders->each(100) as $order) {
            $timePaymentOrder = $serviceProperty->getOrderPropertyValue($order->properties, 'paymentUrlAvailableDateTime');
            if (!empty($timePaymentOrder)) {
                continue;
            }
            $model = new PaymentProperty();
            $model->order_id = $order->id;
            $model->key = self::KEY;
            $model->value = time() + OsagoSravniRuFormDictionary::TIMESTAMP_AVAILABLE_PAYMENT_URL;
            $serviceProperty->save($model);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        OrderProperty::deleteAll(['key' => self::KEY]);
    }
}
