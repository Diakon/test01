<?php

use yii\db\Migration;
use common\modules\users\dictionaries\UserDictionary;

/**
 * Class m210910_152821_add_users_info_agree
 */
class m210910_152821_add_users_info_agree extends Migration
{
    const TABLE_NAME = 'users_info';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(self::TABLE_NAME, 'rule_agree', $this->smallInteger(1)->defaultValue(UserDictionary::RULE_AGREE_NO));
        Yii::$app->db->getSchema()->refresh();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn(self::TABLE_NAME, 'rule_agree');
        Yii::$app->db->getSchema()->refresh();
    }
}
