<?php

use yii\db\Migration;
use common\modules\orders\models\OrderProperty;
use common\modules\orders\dictionaries\OrderPropertyDictionary;

/**
 * Class m210924_134242_clear_order_property_step
 */
class m210924_134242_clear_order_property_step extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        // Удаляю шаг 5 из orders_properties - не надо там ничего писать
        OrderProperty::deleteAll(['type' => OrderPropertyDictionary::TYPE_CALCULATE_PROPERTY_ID]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
