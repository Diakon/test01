<?php

use yii\db\Migration;

/**
 * Class m211004_100441_cars_info
 */
class m211004_100441_cars_info extends Migration
{
    const TABLE_BRAND = 'cars_brands';
    const TABLE_MODEL = 'cars_models';
    const TABLE_BRAND_API = 'cars_brands_api';
    const TABLE_MODEL_API = 'cars_models_api';
    const TABLE_CAR = 'cars';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        // Создаю таблицу для моделей/брендов
        $this->createTable(self::TABLE_BRAND, [
            'id' => $this->primaryKey(),
            'name' => $this->string(190)->notNull(),
            'status' => $this->tinyInteger(1)->notNull()->defaultValue(1),
            'updated_at' => $this->integer(),
            'created_at' => $this->integer()
        ]);
        $this->createTable(self::TABLE_MODEL, [
            'id' => $this->primaryKey(),
            'brand_id' => $this->integer()->notNull(),
            'name' => $this->string(190)->notNull(),
            'status' => $this->tinyInteger(1)->notNull()->defaultValue(1),
            'updated_at' => $this->integer(),
            'created_at' => $this->integer()
        ]);
        $this->createTable(self::TABLE_BRAND_API, [
            'id' => $this->primaryKey(),
            'parent_id' => $this->integer()->notNull(),
            'type_id' => $this->tinyInteger(2)->notNull(),
            'api' => $this->string(190)->notNull(),
        ]);
        $this->createTable(self::TABLE_MODEL_API, [
            'id' => $this->primaryKey(),
            'parent_id' => $this->integer()->notNull(),
            'type_id' => $this->tinyInteger(2)->notNull(),
            'api' => $this->string(190)->notNull(),
        ]);

        $this->truncateTable(self::TABLE_CAR);

        $this->addForeignKey('FK_' . self::TABLE_MODEL . '_brand_id', self::TABLE_MODEL, 'brand_id', self::TABLE_BRAND, 'id', 'CASCADE');
        $this->createIndex('IND_' . self::TABLE_BRAND . '_name', self::TABLE_BRAND, 'name', true);
        $this->createIndex('IND_' . self::TABLE_MODEL . '_brand_id_name', self::TABLE_MODEL, 'brand_id,name', true);
        $this->createIndex('IND_' . self::TABLE_BRAND . '_status', self::TABLE_BRAND, 'status');
        $this->createIndex('IND_' . self::TABLE_MODEL . '_status', self::TABLE_MODEL, 'status');
        $this->createIndex('IND_' . self::TABLE_BRAND_API . '_parent_id_type_id_api', self::TABLE_BRAND_API, 'parent_id,type_id,api', true);
        $this->createIndex('IND_' . self::TABLE_MODEL_API . '_parent_id_type_id_api', self::TABLE_MODEL_API, 'parent_id,type_id,api', true);

        // Правим таблицу данных об авто
        $this->dropColumn(self::TABLE_CAR, 'model');
        $this->dropColumn(self::TABLE_CAR, 'brand');
        $this->dropColumn(self::TABLE_CAR, 'model_id');
        $this->dropColumn(self::TABLE_CAR, 'brand_id');
        $this->dropColumn(self::TABLE_CAR, 'source_id');
        $this->addColumn(self::TABLE_CAR, 'brand_id', $this->integer());
        $this->addColumn(self::TABLE_CAR, 'model_id', $this->integer());

        $this->addForeignKey('FK_' . self::TABLE_CAR . '_brand_id', self::TABLE_CAR, 'brand_id', self::TABLE_BRAND, 'id', 'CASCADE');
        $this->addForeignKey('FK_' . self::TABLE_CAR . '_model_id', self::TABLE_CAR, 'model_id', self::TABLE_MODEL, 'id', 'CASCADE');
        $this->addForeignKey('FK_' . self::TABLE_BRAND_API . '_parent_id', self::TABLE_BRAND_API, 'parent_id', self::TABLE_BRAND, 'id', 'CASCADE');
        $this->addForeignKey('FK_' . self::TABLE_MODEL_API . '_parent_id', self::TABLE_MODEL_API, 'parent_id', self::TABLE_MODEL, 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('FK_' . self::TABLE_MODEL . '_brand_id', self::TABLE_MODEL);
        $this->dropForeignKey('FK_' . self::TABLE_CAR . '_brand_id', self::TABLE_CAR);
        $this->dropForeignKey('FK_' . self::TABLE_CAR . '_model_id', self::TABLE_CAR);
        $this->dropForeignKey('FK_' . self::TABLE_BRAND_API . '_parent_id', self::TABLE_BRAND_API);
        $this->dropForeignKey('FK_' . self::TABLE_MODEL_API . '_parent_id', self::TABLE_MODEL_API);
        $this->dropTable(self::TABLE_BRAND_API);
        $this->dropTable(self::TABLE_MODEL_API);

        $this->dropTable(self::TABLE_MODEL);
        $this->dropTable(self::TABLE_BRAND);
        $this->dropColumn(self::TABLE_CAR, 'brand_id');
        $this->dropColumn(self::TABLE_CAR, 'model_id');

        $this->addColumn(self::TABLE_CAR, 'model', $this->string());
        $this->addColumn(self::TABLE_CAR, 'brand', $this->string());
        $this->addColumn(self::TABLE_CAR, 'model_id', $this->integer());
        $this->addColumn(self::TABLE_CAR, 'brand_id', $this->integer());
        $this->addColumn(self::TABLE_CAR, 'source_id', $this->integer());
    }
}
