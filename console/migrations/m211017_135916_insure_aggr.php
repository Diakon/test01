<?php

use common\modules\orders\models\Order;
use common\modules\orders\models\OrderProperty;
use yii\db\Migration;

/**
 * Class m211017_135916_insure_aggr
 */
class m211017_135916_insure_aggr extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn(Order::tableName(), 'type');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn(Order::tableName(), 'type', $this->tinyInteger());
    }

}
