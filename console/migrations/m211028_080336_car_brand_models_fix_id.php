<?php

use common\modules\cars\models\CarBrandApi;
use common\modules\cars\models\CarModelApi;
use yii\db\Migration;

/**
 * Class m211028_080336_car_brand_models_fix_id
 */
class m211028_080336_car_brand_models_fix_id extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn(CarModelApi::tableName(), 'id');
        $this->dropColumn(CarBrandApi::tableName(), 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn(CarModelApi::tableName(), 'id', $this->primaryKey());
        $this->dropColumn(CarBrandApi::tableName(), 'id', $this->primaryKey());
    }
}
