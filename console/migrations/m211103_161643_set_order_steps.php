<?php

use common\modules\orders\dictionaries\OrderDictionary;
use common\modules\orders\models\Order;
use yii\db\Migration;

/**
 * Class m211103_161643_set_order_steps
 */
class m211103_161643_set_order_steps extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        Order::updateAll(
            ['step' =>OrderDictionary::STEP_CAR_INFO],
            ['status' => OrderDictionary::STATUS_NEW]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        return true;
    }
}
