<?php

use common\modules\bonuses\models\Bonus;
use yii\db\Migration;

/**
 * Class m211129_150143_add_order_dealer_commission
 */
class m211129_150143_add_order_dealer_commission extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(Bonus::tableName(), 'parent_dealer_bonus', $this->integer()->defaultValue(null));
        $this->addColumn(Bonus::tableName(), 'parent_dealer_id', $this->integer()->defaultValue(null));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn(Bonus::tableName(), 'parent_dealer_bonus');
        $this->dropColumn(Bonus::tableName(), 'parent_dealer_id');
    }
}
