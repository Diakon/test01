<?php

use common\modules\users\models\UserInfo;
use common\modules\users\services\UserService;
use yii\db\Migration;

/**
 * Class m211201_120344_user_info_dealer_id
 */
class m211201_120344_user_info_dealer_id extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $service = new UserService();

        $this->addColumn(UserInfo::tableName(), 'dealer_id', $this->integer()->defaultValue(null));
        $this->createIndex('IND_' . UserInfo::tableName() . '_dealer_id', UserInfo::tableName(), 'dealer_id');
        $query = UserInfo::find();
        /**
         * @var $user UserInfo
         */
        foreach ($query->each(100) as $user) {
            $params = [
                'id' => $user->id,
                'expand' => 'dealer'
            ];
            $userStore = $service->getUsersList($params);
            $dealer = !empty($userStore) && !empty($userStore['items']) ? current($userStore['items']) : [];
            $dealerId = $dealer['dealer']['id'] ?? null;
            if (empty($dealerId)) continue;
            $user->dealer_id = $dealerId;
            $user->save();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn(UserInfo::tableName(), 'dealer_id');
    }
}
