<?php

use common\modules\orders\models\OrderProperty;
use yii\db\Migration;

/**
 * Class m211214_154746_orders_add_offers
 */
class m211214_154746_orders_add_offers extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn(OrderProperty::tableName(), 'value', 'LONGTEXT');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn(OrderProperty::tableName(), 'value', $this->text());
    }
}
