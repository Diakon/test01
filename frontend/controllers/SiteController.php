<?php
namespace frontend\controllers;

use frontend\modules\insurance\forms\InsuranceForm;
use common\modules\orders\dictionaries\OrderDictionary;
use common\modules\orders\models\Order;
use common\modules\orders\services\OrderService;
use common\modules\users\services\UserInfoService;
use Yii;
use yii\helpers\Url;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\web\HttpException;
use yii\web\Response;

/**
 * Site controller
 */
class SiteController extends Controller
{
    const CODE_SUCCESS = 200;
    const CODE_ERROR = 400;

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error', 'success', 'fail'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index', 'calculation', 'faq'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Главная страница
     *
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Страница расчета
     *
     * @return mixed
     */
    public function actionCalculation()
    {
        // Если пользователь не согласился с условиями - направление на страницу с правилами
        if (!UserInfoService::userRuleAgree(Yii::$app->user->id)) {
            $this->redirect(Url::to(['/users/user/rules']));
            Yii::$app->end();
        }

        $orderId = Yii::$app->request->get('order_id', 0);
        if (!empty($orderId)) {
            // Проверяю, что это заказ авторизованного пользователя
            /**
             * @var $order Order
             */
            $order = OrderService::getOrdersByParams(['id' => $orderId, 'user_id' => Yii::$app->user->id]);

            // Если заказ не новый и не в статусе отложен для оплаты - рассчитывать нечего
            if (empty($order) || !in_array($order->status, [OrderDictionary::STATUS_NEW, OrderDictionary::STATUS_AWAITING_PAYMENT])) {
                throw new HttpException(404 ,'Страница не найдена');
            }

            // Для не завершенных расчетов (заказы из раздела Мои расчеты) на этапе Расчет всегда открывать с этапа Полис
            if ($order->status == OrderDictionary::STATUS_NEW && $order->step ==  InsuranceForm::SCENARIO_STEP_CALCULATE) {
                $order->step = InsuranceForm::SCENARIO_STEP_POLICY;
                $service = new OrderService($order);
                $service->save(false);
            }
        }

        return $this->render('calculation', [
            'order' => !empty($order) ? $order : null,
        ]);
    }

    /**
     * Возвращает JSON ответ
     *
     * @param $code
     * @param null $msg
     * @return array
     */
    public function returnAjax($code, $msg = null): array
    {
        $returnData = [];
        Yii::$app->response->format = Response::FORMAT_JSON;
        Yii::$app->response->statusCode = $code;
        $returnData['status'] = $code;
        if (!is_null($msg)) {
            $returnData['msg'] = $msg;
        }

        return $returnData;
    }
}
