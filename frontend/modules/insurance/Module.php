<?php
namespace frontend\modules\insurance;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'frontend\modules\insurance\controllers';

    public function init()
    {
        parent::init();
    }
}
