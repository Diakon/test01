<?php

namespace frontend\modules\insurance\controllers;

use common\modules\insurance\services\SessionInsuranceService;
use common\modules\insurance\models\InsuranceFactory;
use common\modules\orders\dictionaries\OrderPropertyDictionary;
use common\modules\orders\models\Order;
use common\modules\orders\models\OrderProperty;
use common\modules\orders\services\OrderPropertyService;
use frontend\modules\insurance\forms\InsuranceForm;
use frontend\modules\insurance\traits\InsuranceTrait;
use Yii;
use frontend\controllers\SiteController;
use yii\widgets\ActiveForm;

/**
 * Class CalculationController
 * @package frontend\modules\insurance\controllers
 */
class CalculationController extends SiteController
{
    use InsuranceTrait;

    /**
     * @return mixed|string
     */
    public function actionIndex()
    {
        if (!Yii::$app->request->isPost) {
            return $this->goHome();
        }

        $params = Yii::$app->request->post();
        $action = Yii::$app->request->get('action');

        if (!empty($action)) {
            return $this->{$action}($params);
        }

        $params = $this->getOrderParams($params, InsuranceForm::classNameShort());
        $modelForm = $this->setFormOrderModel(new InsuranceForm(), $params);
        $modelForm->setScenario(InsuranceForm::SCENARIO_STEP_CALCULATE);
        $modelForm->load($params);

        if (!$modelForm->validate()) {
            return $this->returnAjax(self::CODE_ERROR, ActiveForm::validate($modelForm));
        }
        /**
         * @var Order $order
         */
        $order = $this->getOrder();

        // Получаю предложения
        $modelFactory = (new InsuranceFactory())->calculation();
        $modelFactory->setOffers($modelForm, $order);
        $offersInsurance = $modelFactory->offersInsurance;

        // Если пришли предложения - сохраняю в сессию и в OrderProperty (список предложений)
        if (!empty($offersInsurance)) {
            SessionInsuranceService::setOffersData($offersInsurance);
            OrderPropertyService::setProperty($order->id, OrderPropertyDictionary::TYPE_CALCULATE_PROPERTY_ID, ['offersInsurance' => $offersInsurance]);
        }

        return $this->renderPartial('index', [
            'offersInsurance' => $offersInsurance,
            'order' => $order,
            'row' => Yii::$app->request->get('row', 1),
        ]);
    }
}