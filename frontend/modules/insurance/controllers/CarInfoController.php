<?php

namespace frontend\modules\insurance\controllers;

use common\modules\insurance\dictionaries\InsuranceFormDictionaries;
use common\modules\insurance\models\InsuranceFactory;
use frontend\modules\insurance\forms\InsuranceForm;
use Yii;
use frontend\controllers\SiteController;
use frontend\modules\insurance\traits\InsuranceTrait;
use yii\helpers\Json;
use yii\widgets\ActiveForm;

/**
 * Class CarInfoController
 * @package frontend\modules\insurance\controllers
 */
class CarInfoController extends SiteController
{
    use InsuranceTrait;

    /**
     * @return mixed|string
     */
    public function actionIndex()
    {
        $action = Yii::$app->request->get('action');

        if (empty($action) && Yii::$app->request->isGet) {
            return $this->formHtml();
        }
        $post = Yii::$app->request->post();

        return $this->{$action}($post);
    }


    /**
     * Возвращает HTML кода формы
     *
     * @param array $params
     *
     * @return string
     */
    private function formHtml(array $params = []): string
    {
        $model = $this->setFormOrderModel($this->getModel(), $params);
        $this->setCarCategoryDefault($model);

        return $this->renderPartial('index', ['model' => $model]);
    }

    /**
     * Проверяет форму
     *
     * @param array $params
     *
     * @return array
     */
    private function validate(array $params): array
    {
        $model = $this->getModel();
        $model->load($this->getOrderParams($params, $model::classNameShort()));
        $this->setCarCategoryDefault($model);

        if (!$model->validate()) {
            return $this->returnAjax(self::CODE_ERROR, ActiveForm::validate($model));
        }

        // Сохраняю шаг
        $order = $this->saveStep($model);
        if (!$order) {
            return $this->returnAjax(self::CODE_ERROR, "Ошибка АПИ");
        }

        return $this->returnAjax(self::CODE_SUCCESS, ['step' => 'driver', 'orderId' => $order->id]);
    }

    /**
     * Выставляет значение категории ТС
     *
     * @param InsuranceForm $model
     * @return InsuranceForm
     */
    protected function setCarCategoryDefault(InsuranceForm $model): InsuranceForm
    {
        $model->vehicleCategory = InsuranceFormDictionaries::CAR_CATEGORY_B;

        return $model;
    }

    /**
     * Поиск модели по бренду
     *
     * @param array $params
     *
     * @return array|string
     *
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\httpclient\Exception
     * @throws \yii\web\HttpException
     */
    private function searchModel(array $params = [])
    {
        $form = $this->getModel();
        $models = $form->getModels($params['brandId'] ?? 0);
        if (!empty($models)) {
            return Json::encode($models);
        }

        return $this->returnAjax(self::CODE_ERROR, "Ошибка АПИ");
    }

    /**
     * @return InsuranceForm
     */
    private function getModel(): InsuranceForm
    {
        $model = new InsuranceForm();
        $model->setScenario(InsuranceForm::SCENARIO_STEP_CAR_INFO);

        return $model;
    }

    /**
     * Получает данные об авто по гос. номеру. Если нет в БД - тянем по АПИ и сохраняем в БД у себя
     *
     * @param array $params
     *
     * @return array
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\httpclient\Exception
     * @throws \yii\web\HttpException
     */
    private function carInfoByNumber(array $params): array
    {
        $form = new InsuranceForm();
        $form->setScenario(InsuranceForm::SCENARIO_VALIDATE_CAR_NUMBER);
        $form->load($params);
        if (!$form->validate()) {
            return $this->returnAjax(self::CODE_ERROR, ActiveForm::validate($form));
        }
        $model = (new InsuranceFactory())->carInfo();
        $model->setCarInfoByNumber($form->vehicleLicensePlate);

        if (empty($model->car)) {
            $model->addError('vehicleLicensePlate', 'Не удалось получить данные о машине по гос. номеру. Пожалуйста, введите данные вручную или попробуйте снова.');

            return $this->returnAjax(self::CODE_ERROR, ['insuranceform-vehiclelicenseplate' => $model->getErrorSummary(true)]);
        }

        return $this->returnAjax(self::CODE_SUCCESS, ['car' => $model->car]);
    }
}