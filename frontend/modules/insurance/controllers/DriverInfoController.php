<?php

namespace frontend\modules\insurance\controllers;

use frontend\modules\insurance\forms\InsuranceForm;
use frontend\modules\insurance\traits\InsuranceTrait;
use Yii;
use frontend\controllers\SiteController;
use yii\widgets\ActiveForm;

/**
 * Class DriverInfoController
 * @package frontend\modules\insurance\controllers
 */
class DriverInfoController extends SiteController
{
    use InsuranceTrait;

    /**
     * @return mixed|string
     */
    public function actionIndex()
    {
        if (Yii::$app->request->isGet) {
            return $this->formHtml();
        }

        $post = Yii::$app->request->post();
        $action = Yii::$app->request->get('action');

        return $this->{$action}($post);
    }

    /**
     * Возвращает HTML кода формы
     *
     * @param array $params
     *
     * @return string
     */
    private function formHtml(array $params = []): string
    {
        $isNew = Yii::$app->request->get('add', false);
        $model = $this->getModel();
        $model = $isNew ? $model : $this->setFormOrderModel($model, $params);
        $row = Yii::$app->request->get('row', 0);

        return $this->renderPartial('index', ['model' => $model, 'row' => $row]);
    }

    /**
     * Проверяет форму
     *
     * @param $params
     *
     * @return array
     */
    private function validate(array $params): array
    {
        $model = $this->getModel();
        $class = $model::classNameShort();
        $params = $this->getOrderParams($params, $class);

        // Если пришел driverLicenseSerialNumber или driverPrevLicenseSerialNumber -
        // разбиваю по пробелу и вставляю в driverLicenseSerial и driverLicenseNumber или в driverPrevLicenseSerial и driverPrevLicenseNumber
        foreach (['driverLicenseSerialNumber', 'driverPrevLicenseSerialNumber'] as $paramName) {
            if (!empty($params[$class][$paramName])) {
                foreach ($params[$class][$paramName] as $key => $licenseSerialNumber) {
                    $licenseSerialNumber = explode(' ', $licenseSerialNumber);
                    if (count($licenseSerialNumber) == 2) {
                        $suffix = $paramName == 'driverPrevLicenseSerialNumber' ? 'Prev' : '';
                        $params[$class]['driver' . $suffix . 'LicenseSerial'][$key] = $licenseSerialNumber[0];
                        $params[$class]['driver' . $suffix . 'LicenseNumber'][$key] = $licenseSerialNumber[1];
                    }
                }
            }
        }

        $model->load($params);
        if (!$model->validate()) {
            return $this->returnAjax(self::CODE_ERROR, ActiveForm::validate($model));
        }

        // Сохраняю шаг
        $order = $this->saveStep($model);
        if (!$order) {
            return $this->returnAjax(self::CODE_ERROR, "Ошибка АПИ");
        }

        return $this->returnAjax(self::CODE_SUCCESS, ['step' => InsuranceForm::SCENARIO_STEP_OWNER, 'orderId' => $order->id]);
    }

    /**
     * @return InsuranceForm
     */
    private function getModel(): InsuranceForm
    {
        $model = new InsuranceForm();
        $model->setScenario(InsuranceForm::SCENARIO_STEP_DRIVER);

        return $model;
    }
}