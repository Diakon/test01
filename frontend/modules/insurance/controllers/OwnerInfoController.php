<?php

namespace frontend\modules\insurance\controllers;

use common\models\kladr\City;
use common\models\kladr\Kladr;
use common\modules\insurance\dictionaries\InsuranceFormDictionaries;
use common\modules\insurance\services\inguru\KladrService;
use frontend\modules\insurance\forms\InsuranceForm;
use frontend\modules\insurance\traits\InsuranceTrait;
use Yii;
use frontend\controllers\SiteController;
use yii\helpers\Json;
use yii\widgets\ActiveForm;

/**
 * Class OwnerController
 * @package frontend\modules\insurance\controllers
 */
class OwnerInfoController extends SiteController
{
    use InsuranceTrait;

    /**
     * @return mixed|string
     */
    public function actionIndex()
    {
        $action = Yii::$app->request->get('action');

        if (empty($action) && Yii::$app->request->isGet) {
            return $this->formHtml();
        }
        $post = Yii::$app->request->post();

        return $this->{$action}($post);
    }

    /**
     * Возвращает HTML кода формы
     *
     * @param array $params
     * @return string
     */
    private function formHtml(array $params = []): string
    {
        $model = $this->setFormOrderModel($this->getModel(), $params);
        $model->multiDrive = $this->getMultiDrive(); // Для валидации данных о владельце требуется знать значение выбранное в multiDrive
        if (is_null($model->insurerIsOwner)) {
            $model->insurerIsOwner = InsuranceFormDictionaries::ONE_FACE_YES;
        }
        if (is_null($model->usePeriod)) {
            $model->usePeriod = InsuranceFormDictionaries::MONTH_12;
        }

        return $this->renderPartial('index', ['model' => $model]);
    }

    /**
     * Возвращает список адресов получая их через запрос в АПИ dadata
     *
     * @param array $params
     *
     * @return string
     */
    private function searchAddress(array $params = []): string
    {
        $term = $params['term'] ?? Yii::$app->request->get('term');

        return Json::encode(Yii::$app->api->getAddress($term));
    }

    /**
     * Проверяет форму
     *
     * @param $params
     * @return array
     */
    private function validate(array $params): array
    {
        $model = $this->getModel();
        $class = $model::classNameShort();
        $params = $this->getOrderParams($params, $class);

        // Если пришел ownerPassportSerialNumber или insurerPassportSerialNumber -
        // разбиваю по пробелу и вставляю в ownerPassportSerial и ownerPassportNumber или в insurerPassportSerial и insurerPassportNumber
        foreach (['ownerPassportSerialNumber', 'insurerPassportSerialNumber'] as $paramName) {
            if (!empty($params[$class][$paramName])) {
                $passportSerialNumber = $params[$class][$paramName];
                $passportSerialNumber = explode(' ', $passportSerialNumber);
                if (count($passportSerialNumber) == 2) {
                    $prefix = $paramName == 'insurerPassportSerialNumber' ? 'insurer' : 'owner';
                    $params[$class][$prefix . 'PassportSerial'] = $passportSerialNumber[0];
                    $params[$class][$prefix . 'PassportNumber'] = $passportSerialNumber[1];
                }
            }
        }

        $model->load($params);
        $model->multiDrive = $this->getMultiDrive(); // Для валидации данных о владельце требуется знать значение выбранное в multiDrive
        $model->insurerIsOwner = !empty($model->insurerIsOwner) ? InsuranceFormDictionaries::ONE_FACE_YES : InsuranceFormDictionaries::ONE_FACE_NO;

        if (!$model->validate()) {
            return $this->returnAjax(self::CODE_ERROR, ActiveForm::validate($model));
        }

        // Сохраняю шаг
        $order = $this->saveStep($model);
        if (!$order) {
            return $this->returnAjax(self::CODE_ERROR, "Ошибка АПИ");
        }

        return $this->returnAjax(self::CODE_SUCCESS, ['step' => InsuranceForm::SCENARIO_STEP_POLICY, 'orderId' => $order->id]);
    }

    /**
     * @return InsuranceForm
     */
    private function getModel(): InsuranceForm
    {
        $model = new InsuranceForm();
        $model->setScenario(InsuranceForm::SCENARIO_STEP_OWNER);

        return $model;
    }

    /**
     * Возвращает данные о мультидрайве
     *
     * @return int
     */
    private function getMultiDrive(): int
    {
        $order = $this->getOrder();
        $multiDrive = $order->propertyValue('multiDrive');

        return !empty($multiDrive) ? InsuranceFormDictionaries::IS_MULTI_DRIVE_YES : InsuranceFormDictionaries::IS_MULTI_DRIVE_NO;
    }

    /**
     * Возвращает список городов с областью (так как требует сравни.ру)
     * @param array $params
     * @return string
     */
    private function searchCity($params = [])
    {
        $result = [];

        foreach (KladrService::getCityByName($params['term'] ?? Yii::$app->request->get('term')) as $city) {
            $code = $city['code'];
            $regionName = $city['regionName'];
            $regionType = $city['regionType'];
            $cityName = $city['name'];
            $cityType = $city['type'];
            $label = '';
            // города федерального значения возвращаю без указания региона (у них это название самого города)
            if (!empty($regionName) && !in_array($city['code'], [7700000000000, 9200000000000, 7800000000000])) {
                $label .= trim("$regionName $regionType") . ", ";
            }

            $label .= "$cityType. $cityName";
            $label = trim($label);
            $result[] = [
                'id' => $code,
                'value' => $label,
                'label' => $label,
            ];
        }

        return Json::encode($result);
    }
}