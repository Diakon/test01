<?php

namespace frontend\modules\insurance\controllers;

use common\modules\insurance\dictionaries\InsuranceFormDictionaries;
use common\modules\insurance\forms\CalculationInsuranceForm;
use common\modules\insurance\services\SessionInsuranceService;
use common\modules\orders\models\Order;
use frontend\controllers\SiteController;
use frontend\modules\insurance\forms\InsuranceForm;
use frontend\modules\insurance\traits\InsuranceTrait;
use frontend\modules\orders\traits\OrderTrait;
use common\modules\orders\dictionaries\OrderDictionary;
use common\modules\orders\services\OrderService;
use Yii;
use yii\helpers\Url;

/**
 * Class PaymentsController
 * @package frontend\modules\insurance\controllers;
 */
class PaymentsController extends SiteController
{
    use InsuranceTrait;

    /**
     * @return mixed|string
     */
    public function actionIndex()
    {
        if (!Yii::$app->request->isPost) {
            return $this->goHome();
        }

        $offerKey = Yii::$app->request->post('offerKey');
        $awaiting = Yii::$app->request->post('awaiting');
        $awaiting = !empty($awaiting);
        /**
         * @var $model CalculationInsuranceForm
         */
        $model = SessionInsuranceService::getOfferData($offerKey);
        if (empty($model)) {
            return $this->returnAjax(self::CODE_ERROR, 'Ошибка при сохранении данных. Пожалуйста, обратитесь в службу поддержки!');
        }

        /**
         * @var Order $order
         */
        $order = $this->getOrder();

        /**
         * @var $form InsuranceForm
         */
        $form = $this->getForm();
        $form->calculateId = $model->calculateId;
        $form->companyId = $model->companyId;
        $form->companyName = $model->companyName;
        $form->paymentUrl = $model->paymentUrl;
        $form->offerId = $model->offerId;
        $form->paymentToken = $model->paymentToken;
        $form->paymentUrlAvailableDateTime = time() + InsuranceFormDictionaries::TIMESTAMP_AVAILABLE_PAYMENT_URL;
        $form->insuranceType = $model->insuranceType;

        // Сохраняю выбранную стоимость, комиссию, статус перевожу в "Ожидают оплаты"
        $order->price = $model->price;
        $order->commission = $model->commission;
        $order->status = OrderDictionary::STATUS_AWAITING_PAYMENT;
        $serviceOrder = new OrderService($order);
        $serviceOrder->save();

        // Если нажали на кнопку сохранить в разделе "Ожидают оплаты", то сохраняем заказ со статусом "Ожидает оплаты" и редирект в этот раздел
        if ($awaiting) {
            // Сохраняю шаг как для "отложить оплату"
            if(!$this->saveStep($form, OrderDictionary::STATUS_AWAITING_PAYMENT)) {
                return $this->returnAjax(self::CODE_ERROR, 'Ошибка при сохранении данных. Пожалуйста, обратитесь в службу поддержки!');
            }

            return $this->returnAjax(self::CODE_SUCCESS, ['url' => Url::to(['/awaiting-payment'])]);
        }

        return $this->returnAjax(self::CODE_SUCCESS, ['url' => $model->paymentUrl]);
    }

    /**
     * @return InsuranceForm
     */
    private function getForm(): InsuranceForm
    {
        $form = new InsuranceForm();
        $form->setScenario(InsuranceForm::SCENARIO_STEP_PAYMENT);

        return $form;
    }
}