<?php

namespace frontend\modules\insurance\controllers;

use common\modules\insurance\dictionaries\InsuranceFormDictionaries;
use frontend\modules\insurance\forms\InsuranceForm;
use frontend\modules\insurance\traits\InsuranceTrait;
use Yii;
use frontend\controllers\SiteController;
use yii\widgets\ActiveForm;

/**
 * Class PolicyInfoController
 * @package frontend\modules\insurance\controllers
 */
class PolicyInfoController extends SiteController
{
    use InsuranceTrait;

    /**
     * @return mixed|string
     */
    public function actionIndex()
    {
        $action = Yii::$app->request->get('action');

        if (empty($action) && Yii::$app->request->isGet) {
            return $this->formHtml();
        }

        $post = Yii::$app->request->post();

        return $this->{$action}($post);
    }

    /**
     * Возвращает HTML кода формы
     *
     * @param array $params
     *
     * @return string
     */
    private function formHtml(array $params = []): string
    {
        $model = $this->setFormOrderModel($this->getModel(), $params);
        if (is_null($model->prevPolicyIsset)) {
            $model->prevPolicyIsset = InsuranceFormDictionaries::PREV_POLICY_ISSET_YES;
        }

        return $this->renderPartial('index', ['model' => $model]);
    }

    /**
     * Проверяет форму
     *
     * @param array $params
     *
     * @return array
     */
    private function validate(array $params): array
    {
        $model = $this->getModel();
        $class = $model::classNameShort();
        $params = $this->getOrderParams($params, $class);

        // Если пришел prevPolicySerialNumber -
        // разбиваю по пробелу и вставляю в prevPolicySerial и prevPolicyNumber
        if (!empty($params[$class]['prevPolicySerialNumber'])) {
            $prevPolicySerialNumber = $params[$class]['prevPolicySerialNumber'];
            $prevPolicySerialNumber = explode(' ', $prevPolicySerialNumber);
            if (count($prevPolicySerialNumber) == 2) {
                $params[$class]['prevPolicySerial'] = $prevPolicySerialNumber[0];
                $params[$class]['prevPolicyNumber'] = $prevPolicySerialNumber[1];
            }
        }
        $params[$class]['usePeriod'] = InsuranceFormDictionaries::MONTH_12;

        $model->load($params);
        if (!$model->validate()) {
            return $this->returnAjax(self::CODE_ERROR, ActiveForm::validate($model));
        }

        // Сохраняю шаг
        $order = $this->saveStep($model);
        if (!$order) {
            return $this->returnAjax(self::CODE_ERROR, "Ошибка АПИ");
        }

        return $this->returnAjax(self::CODE_SUCCESS, ['step' => InsuranceForm::SCENARIO_STEP_CALCULATE, 'orderId' => $order->id]);
    }

    /**
     * @return InsuranceForm
     */
    private function getModel(): InsuranceForm
    {
        $model = new InsuranceForm();
        $model->setScenario(InsuranceForm::SCENARIO_STEP_POLICY);

        return $model;
    }
}