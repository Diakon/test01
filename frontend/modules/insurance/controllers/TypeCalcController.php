<?php

namespace frontend\modules\insurance\controllers;

use common\modules\orders\models\Order;
use frontend\modules\insurance\forms\InsuranceForm;
use Yii;
use frontend\controllers\SiteController;
use frontend\modules\insurance\traits\InsuranceTrait;
use yii\widgets\ActiveForm;

/**
 * Class TypeCalcController
 *
 * @package frontend\modules\insurance\controllers
 */
class TypeCalcController extends SiteController
{
    use InsuranceTrait;

    /**
     * @return mixed|string
     */
    public function actionIndex()
    {
        $action = Yii::$app->request->get('action');
        if (empty($action)) {
            return $this->formHtml();
        }

        $post = Yii::$app->request->post();

        $action = Yii::$app->request->get('action');

        return $this->{$action}($post);
    }

    /**
     * Возвращает HTML кода формы
     *
     * @param array $params
     * @return string
     */
    private function formHtml(array $params = []): string
    {
        $model = $this->getModel();
        $model->load($params);

        return $this->renderPartial('index', ['model' => $model]);
    }

    /**
     * Проверяет форму
     *
     * @param $params
     *
     * @return array
     */
    private function validate(array $params): array
    {
        $model = $this->getModel();
        $model->load($this->getOrderParams($params));
        if (!$model->validate()) {
            return $this->returnAjax(self::CODE_ERROR, ActiveForm::validate($model));
        }

        // Сохраняю шаг
        $order = $this->saveStep($model);
        if (!$order) {
            return $this->returnAjax(self::CODE_ERROR, "Ошибка АПИ");
        }

        return $this->returnAjax(self::CODE_SUCCESS, ['step' => InsuranceForm::SCENARIO_STEP_CAR_INFO, 'orderId' => 0]);
    }

    /**
     * Возвращает тип расчета (полный или предварительный) для заказа
     *
     * @param array $params
     *
     * @return array
     */
    private function typeCalc(array $params = []): array
    {
        /**
         * @var Order $order
         */
        $order = $this->getOrder();

        return $this->returnAjax(self::CODE_SUCCESS, $order->propertyValue('preCalculation'));
    }

    /**
     * @return InsuranceForm
     */
    private function getModel(): InsuranceForm
    {
        $model = new InsuranceForm();
        $model->setScenario(InsuranceForm::SCENARIO_STEP_TYPE_CALC);

        return $model;
    }
}