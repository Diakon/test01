<?php

namespace frontend\modules\insurance\forms;

use common\modules\insurance\forms\InsuranceForm as BaseInsuranceForm;
use common\modules\insurance\models\BrandModelFactory;
use common\modules\insurance\dictionaries\InsuranceFormDictionaries;
use yii\helpers\ArrayHelper;

/**
 * Форма для оформления страховки (полный расчет с возможностью оформить страховку)
 */
class InsuranceForm extends BaseInsuranceForm
{
    /**
     * Тип расчета
     * @return string[]
     */
    public function getTypeCalc(): array
    {
        return InsuranceFormDictionaries::TYPE_CALC;
    }

    /**
     * Возвращает список марок ТС
     *
     * @return array
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\httpclient\Exception
     */
    public function getBrands(): array
    {
        return ArrayHelper::map((new BrandModelFactory())->getBrands(), 'id', 'name');
    }

    /**
     * Возвращает список моделей ТС
     *
     * @param null|int $brandId
     * @return array
     */
    public function getModels(int $brandId = null): array
    {
        $brandId = !empty($brandId) ? $brandId : $this->vehicleBrand;
        return !empty($brandId)
            ? ArrayHelper::map((new BrandModelFactory())->getModels($brandId), 'id', 'name')
            : [];
    }

    /**
     * Тип идентификации ТС (по VIN, кузову, шасси)
     *
     * @return string[]
     */
    public function getTypeCarIdentity(): array
    {
        return InsuranceFormDictionaries::CAR_HAVE_LIST;
    }

    /**
     * Типы документов ТС
     * @return string[]
     */
    public function getTypeCarDocs(): array
    {
        return InsuranceFormDictionaries::CAR_DOCS;
    }

    /**
     * Страхователь и владелец одно лицо
     * @return string[]
     */
    public function getOneFaces(): array
    {
        return InsuranceFormDictionaries::ONE_FACES;
    }

    /**
     * Период использования (кол-во месяцев)
     *
     * @return array
     */
    public function getUsePeriodItems(): array
    {
        return InsuranceFormDictionaries::MONTHS;
    }

    /**
     * Категории ТС
     *
     * @return array
     */
    public function getCarCategory(): array
    {
        return InsuranceFormDictionaries::CAR_CATEGORY_LIST;
    }
}
