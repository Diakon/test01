<?php

namespace frontend\modules\insurance\traits;

use Yii;
use common\modules\orders\models\CalculateProperty;
use common\modules\orders\models\CarProperty;
use common\modules\orders\dictionaries\OrderDictionary;
use common\modules\orders\models\DriverProperty;
use common\modules\orders\models\OwnerProperty;
use common\modules\orders\models\PaymentProperty;
use common\modules\orders\models\PolicyProperty;
use common\modules\orders\models\TypeCalcProperty;
use common\modules\orders\services\OrderPropertyService;
use common\modules\orders\services\OrderService;
use frontend\modules\insurance\forms\InsuranceForm;
use common\modules\orders\models\Order;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

trait InsuranceTrait
{
    /**
     * Сохраняет шаг заказа
     *
     * @param InsuranceForm $form
     * @param int $status
     *
     * @return bool|Order
     */
    public function saveStep($form, int $status = OrderDictionary::STATUS_NEW)
    {
        $step = $form->scenario;
        $params = $form->scenarios();
        $params = $params[$step] ?? [];

        $transaction = Yii::$app->db->beginTransaction();
        try {
            $needRollBack = false;
            $model = $this->getOrder();
            $model->step = $step;
            $model->status = $status;
            $serviceOrder = new OrderService($model);

            // Пользователь решил не оплачивать заказ, а отложить -
            // ставлю шаг на этап расчет (когда пользователь захочет оплатить - надо снова будет проверить введенные данные)
            if ($model->status == OrderDictionary::STATUS_AWAITING_PAYMENT) {
                $model->step = $form::SCENARIO_STEP_CALCULATE;
            }
            if ($serviceOrder->save()) {
                // Сохраняю выбранные параметры
                switch ($step) {
                    case "type_calc":
                        $class = new TypeCalcProperty();
                        break;
                    case "car":
                        $class = new CarProperty();
                        break;
                    case "driver":
                        $class = new DriverProperty();
                        break;
                    case "owner":
                        $class = new OwnerProperty();
                        break;
                    case "policy":
                        $class = new PolicyProperty();
                        break;
                    case "calc":
                        $class = new CalculateProperty();
                        break;
                    case "payment":
                        $class = new PaymentProperty();
                        break;
                    default:
                        $class = new CarProperty();
                }
                OrderPropertyService::deleteByParams($model->id, ['type' =>  $class::typeId()]);

                // Пишу новые параметры
                foreach ($form->attributes as $key => $value) {
                    if (!in_array($key, $params)) {
                        continue;
                    }
                    $className = get_class($class);
                    $modelProperty = new $className();
                    $modelProperty->order_id = $model->id;
                    $modelProperty->key = $key;
                    $modelProperty->value = is_array($value) ? Json::encode($value) : trim($value);
                    $serviceProperty = new OrderPropertyService($modelProperty);
                    if (!$serviceProperty->save()) {
                        $needRollBack = true;
                        Yii::error('Ошибка при сохранении параметров заказа на шаге ' . $step . '. Сообщение валидатора: ' . Json::encode($modelProperty->errors));
                    }
                }

                if ($needRollBack) {
                    $transaction->rollBack();
                    return false;
                }

                $transaction->commit();

                return $model;
            }
        } catch (\Throwable $e) {
            $transaction->rollBack();
            Yii::error('Ошибка при сохранении шага ' . $step);
        }

        return false;
    }

    /**
     * Возвращает выставленные пользователем для заказа параметры
     *
     * @param array $params
     * @param string $paramsKey
     *
     * @return array
     */
    public function getOrderParams(array $params = [], string $paramsKey = 'InsuranceForm'): array
    {
        $orderId = Yii::$app->request->get('order_id');
        if (!empty($orderId)) {
            $property = OrderPropertyService::getOrdersPropertyByParams(['order_id' => $orderId]);
            if (!empty($property)) {
                foreach (ArrayHelper::map($property, 'key', 'value') as $key => $value) {
                    $value = $this->isJson($value) ? Json::decode($value) : $value;
                    $params[$paramsKey][$key] = $params[$paramsKey][$key] ?? $value;
                }
            }
        }

        return $params;
    }

    /**
     * Возвращает заказ
     *
     * @return array|Order|\yii\db\ActiveRecord
     */
    public function getOrder()
    {
        $orderId = Yii::$app->request->get('order_id', 0);
        // Если был передан ID заказа и он не завершен - продолжаю с выбранного заказа
        $model = OrderService::getOrdersByParams(['id' => $orderId, 'user_id' => Yii::$app->user->id]);

        // Если нет заказа - создаем новый
        if (empty($model)) {
            $model = new Order();
            $model->status = OrderDictionary::STATUS_NEW;
            $model->user_id = \Yii::$app->user->id;
        }
        $model->token = !empty($model->token) ? $model->token : $model->generateToken();

        return $model;
    }

    /**
     * Выставляет параметры для формы заказа ОСАГО
     *
     * @param InsuranceForm $model
     * @param array $params
     *
     * @return InsuranceForm
     */
    public function setFormOrderModel(InsuranceForm $model, array $params = []): InsuranceForm
    {
        $order = $this->getOrder();
        // Если это продолжение оформления не завершенного ранее заказа - проставляю на форму данные, которые пользователь вводил для этого заказа ранее
        if (!$order->isNewRecord) {
            $paramsProperty = [];
            foreach ($order->properties as $property) {
                $paramsProperty[$model::classNameShort()][$property->key] = $this->isJson($property->value) ? Json::decode($property->value) : $property->value;
            }
            $params = ArrayHelper::merge($paramsProperty, $params);
        }

        $model->load($params);

        return $model;
    }

    /**
     * Проверяет, является ли строка json
     *
     * @param $string
     *
     * @return bool
     */
    public function isJson(string $string): bool
    {
        json_decode($string);

        return (json_last_error() == JSON_ERROR_NONE);
    }
}