<?php
/**
 * @var $offersInsurance \common\modules\insurance\forms\CalculationInsuranceForm[]
 * @var $order \common\modules\orders\models\Order
 * @var $row integer
 */

use yii\helpers\Html;
use common\models\dictionaries\BaseDictionary;
use common\modules\bonuses\services\BonusService;

?>
<div>
    <?php if (empty($offersInsurance)) { ?>
        <div class="row">
            <h4>Ни одна из страховых компаний не готова сделать предложение о заключении договора полиса ОСАГО</h4>
        </div>
    <?php } else { ?>
        <div class="panel-group" id="accordion" style="margin-top: -10px;">
            <?php
            // Сперва выводим предложения сравни.ру, потом ИНГУРУ
            foreach ([BaseDictionary::TYPE_SRAVNIRU, BaseDictionary::TYPE_INGURU] as $insuranceType) { ?>
                <?php foreach ($offersInsurance as $offer) { ?>
                    <?php
                    if ($insuranceType != $offer->insuranceType) continue;

                    $offerKey = $offer->key;
                    $calculateId = $offer->calculateId;
                    $paymentToken = $offer->paymentToken;
                    $companyId = $offer->companyId;
                    $price = $offer->price;
                    $commission = $offer->commission;
                    $paymentUrl = $offer->paymentUrl;
                    $companyName = $offer->companyName;
                    $coefficients = $offer->coefficients;
                    if (empty($paymentUrl) || (!empty($selectedCompanyId) && $selectedCompanyId != $companyId)) {
                        continue;
                    }
                    ?>
                    <div class="panel panel-default js-order-block-row-<?= $order->id ?>">
                        <div id="collapse<?= $companyId ?>" class="panel-collapse collapse in" aria-expanded="true">
                            <div>
                                <div align="center">
                                    <h4 class="black-color" style="margin-bottom: 0px;">
                                        <?= !empty($companyFullName) ? $companyFullName : $companyName ?>
                                    </h4>
                                    <p></p>
                                    <table style="text-align:center">
                                        <tr>
                                            <td style="padding:0 20px 0;">
                                                <span class="black-color">
                                                    Стоимость полиса ОСАГО:
                                                    <br>
                                                    <?= Yii::$app->formatter->asPrice($price) ?>
                                                </span>
                                            </td>
                                            <td style="padding:0 20px;">
                                                <span class="black-color">
                                                    Ваши бонусы:
                                                    <br>
                                                    <?= BonusService::calculatePrice($commission) ?>
                                                </span>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div>
                                    <?php if (!empty($coefficients)) { ?>
                                        <br>
                                        <div align="center">
                                            <a
                                                    href="#"
                                                    style="text-align: center"
                                                    onclick="$(this).parent().parent().find('.js-calc-add-data-info-block').slideToggle(); return false;"
                                                    class="desc__toggle js-calc-add-data-info-block-btn"
                                            >Подробнее о расчете</a>
                                        </div>
                                        <div class="js-calc-add-data-info-block" style="display: none; margin-left:-25px;">
                                            <ul>
                                                <?php foreach ($coefficients as $coefficientKey => $coefficientValue) { ?>
                                                    <?php switch ($coefficientKey) {
                                                        case "tb":
                                                            echo Html::tag('li', '<span class="black-color">Базовая стоимость полиса: ' . (Yii::$app->formatter->asPrice($coefficientValue)) . '</span>');
                                                            break;
                                                        case "kt":
                                                            echo Html::tag('li', '<span class="black-color">Территориальный коэффициент: ' . $coefficientValue . '</span>');
                                                            break;
                                                        case "kbm":
                                                            echo Html::tag('li', '<span class="black-color">Коэффициент возникновения страховых случаев: ' . $coefficientValue . '</span>');
                                                            break;
                                                        case "kvs":
                                                            echo Html::tag('li', '<span class="black-color">Коэффициент водительского стажа: ' . $coefficientValue . '</span>');
                                                            break;
                                                        case "ks":
                                                            echo Html::tag('li', '<span class="black-color">Сезонный показатель: ' . $coefficientValue . '</span>');
                                                            break;
                                                        case "kp":
                                                            echo Html::tag('li', '<span class="black-color">Коэффициент применения в определенный период времени: ' . $coefficientValue . '</span>');
                                                            break;
                                                        case "km":
                                                            echo Html::tag('li', '<span class="black-color">Коэффициент мощности двигателя: ' . $coefficientValue . '</span>');
                                                            break;
                                                        case "kn":
                                                            echo Html::tag('li', '<span class="black-color">Ставка страховой компании (зависит от количества выявленных нарушений): ' . $coefficientValue . '</span>');
                                                            break;
                                                        case "ko":
                                                            echo Html::tag('li', '<span class="black-color">Коэффициент вычисляемый от количества страхуемых водителей: ' . $coefficientValue . '</span>');
                                                            break;
                                                        case "kpr":
                                                            echo Html::tag('li', '<span class="black-color">Коэффициент вычисляемый от факта наличие прицепного устройства: ' . $coefficientValue . '</span>');
                                                            break;
                                                    } ?>
                                                <?php } ?>
                                            </ul>
                                        </div>
                                    <?php } ?>
                                    <div class="js-payment-btn-block" align="center">
                                        <a
                                                style="margin-top: 10px; width: 95%"
                                                href="#"
                                                data-offerKey="<?= $offerKey ?>"
                                                class="btn btn-primary js-save-to-awaiting-payment"
                                        >Выбрать предложение</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php }
                ?>
            <?php } ?>
        </div>
    <?php } ?>
</div>