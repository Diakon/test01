<?php
/**
 * @var $model \frontend\modules\insurance\forms\InsuranceForm
 */

use yii\helpers\Html;
use common\modules\insurance\dictionaries\InsuranceFormDictionaries;

$labels = $model->attributeLabels();
?>
<p>
    Вы можете заполнить данные по гос. номеру или указать вручную.
</p>
<div class="col-lg-4">
    <div class="row">
        <?= Html::activeLabel($model, 'vehicleCategory') ?><br>
        <?= Html::activeDropDownList($model, 'vehicleCategory', $model->getCarCategory(),
            ['class' => 'form-control', 'disabled' => 'disabled', 'prompt' => 'Выберите категорию ТС']); ?>
        <div class="js-error-msg js-error-insuranceform-vehiclecategory"></div>
    </div>
    <div class="row">
        <?= Html::activeLabel($model, 'vehicleLicensePlate') ?><br>
        <?= Html::activeTextInput($model, 'vehicleLicensePlate', ['class' => 'form-control', 'placeholder' => 'Гос. номер']); ?>
        <div style="clear: both;" class="js-error-msg js-error-insuranceform-vehiclelicenseplate"></div>
        <a href="#" class="btn btn-primary js-set-car-info-by-number">Заполнить</a>
    </div>
    <div class="row">
        <?= Html::activeLabel($model, 'vehicleBrand') ?><br>
        <?= Html::activeDropDownList($model, 'vehicleBrand', $model->getBrands(),
            ['class' => 'form-control js-vehicle-brand-id', 'prompt' => 'Выберите марку ТС']); ?>
        <div class="js-error-msg js-error-insuranceform-vehiclebrand"></div>
    </div>
    <div class="row">
        <?= Html::activeLabel($model, 'vehicleModel') ?><br>
        <?= Html::activeDropDownList($model, 'vehicleModel', $model->getModels(),
            ['class' => 'form-control js-vehicle-model-id', 'prompt' => 'Выберите модель ТС']); ?>
        <div class="js-error-msg js-error-insuranceform-vehiclemodel"></div>
    </div>
    <div class="row">
        <?= Html::activeLabel($model, 'vehicleYear') ?><br>
        <?= Html::activeTextInput($model, 'vehicleYear', ['class' => 'form-control js-mask-year', 'placeholder' => 'Год выпуска']); ?>
        <div class="js-error-msg js-error-insuranceform-vehicleyear"></div>
    </div>
    <div class="row">
        <?= Html::activeLabel($model, 'vehiclePower') ?><br>
        <?= Html::activeTextInput($model, 'vehiclePower', ['class' => 'form-control', 'placeholder' => 'Мощность']); ?>
        <div class="js-error-msg js-error-insuranceform-vehiclepower"></div>
    </div>

    <div id="js-car-extended-info-block">
        <div class="row">
            <?= Html::activeLabel($model, 'vehicleIdentity') ?><br>
            <?= Html::activeDropDownList($model, 'vehicleIdentity', $model->getTypeCarIdentity(),
                ['class' => 'form-control js-vehicle-type-identity-select']); ?>
            <div class="js-error-msg js-error-insuranceform-vehicleidentity"></div>
        </div>
        <div class="row js-vehicle-type-identity js-vehicle-type-identity-id-<?= InsuranceFormDictionaries::CAR_HAVE_VIN ?>"
             style="<?= empty($model->vehicleIdentity) ? "" : "display:none" ?>">
            <?= Html::activeLabel($model, 'vehicleVin') ?><br>
            <?= Html::activeTextInput($model, 'vehicleVin', ['class' => 'form-control', 'placeholder' => $labels['vehicleVin']]); ?>
            <div class="js-error-msg js-error-insuranceform-vehiclevin"></div>
        </div>
        <div class="row js-vehicle-type-identity js-vehicle-type-identity-id-<?= InsuranceFormDictionaries::CAR_HAVE_BODY ?>"
             style="<?= $model->vehicleIdentity == InsuranceFormDictionaries::CAR_HAVE_BODY ? "" : "display:none" ?>">
            <?= Html::activeLabel($model, 'vehicleBodyNum') ?><br>
            <?= Html::activeTextInput($model, 'vehicleBodyNum', ['class' => 'form-control', 'placeholder' => $labels['vehicleBodyNum']]); ?>
            <div class="js-error-msg js-error-insuranceform-vehiclebodynum"></div>
        </div>
        <div class="row js-vehicle-type-identity js-vehicle-type-identity-id-<?= InsuranceFormDictionaries::CAR_HAVE_CHASSIS ?>"
             style="<?= $model->vehicleIdentity == InsuranceFormDictionaries::CAR_HAVE_CHASSIS ? "" : "display:none" ?>">
            <?= Html::activeLabel($model, 'vehicleChassisNum') ?><br>
            <?= Html::activeTextInput($model, 'vehicleChassisNum', ['class' => 'form-control', 'placeholder' => $labels['vehicleChassisNum']]); ?>
            <div class="js-error-msg js-error-insuranceform-vehiclechassisnum"></div>
        </div>
        <div class="row">
            <?= Html::activeLabel($model, 'vehicleDocType') ?><br>
            <?= Html::activeDropDownList($model, 'vehicleDocType', $model->getTypeCarDocs(),
                ['class' => 'form-control js-vehicle-doc-type', 'prompt' => $labels['vehicleDocType']]); ?>
            <div class="js-error-msg js-error-insuranceform-vehicledoctype"></div>
        </div>
        <div class="row">
            <?= Html::activeLabel($model, 'vehicleDocSerial') ?><br>
            <?= Html::activeTextInput($model, 'vehicleDocSerial', ['class' => 'form-control', 'placeholder' => $labels['vehicleDocSerial']]); ?>
            <div class="js-error-msg js-error-insuranceform-vehicledocserial"></div>
        </div>
        <div class="row">
            <?= Html::activeLabel($model, 'vehicleDocNumber') ?><br>
            <?= Html::activeTextInput($model, 'vehicleDocNumber', ['class' => 'form-control', 'placeholder' => $labels['vehicleDocNumber']]); ?>
            <div class="js-error-msg js-error-insuranceform-vehicledocnumber"></div>
        </div>
        <div class="row">
            <?= Html::activeLabel($model, 'vehicleDocDate') ?><br>
            <?php
            echo \yii\jui\DatePicker::widget([
                'model' => $model,
                'attribute' => 'vehicleDocDate',
                'language' => 'ru',
                'dateFormat' => 'dd.MM.yyyy',
                'options' => [
                    'id' => 'insuranceform-vehicledocdate',
                    'placeholder' => $labels['vehicleDocDate'],
                    'class' => 'form-control js-mask-date js-date-picker-change-year-yes',
                ],
            ]);
            ?>
            <div class="js-error-msg js-error-insuranceform-vehicledocdate"></div>
        </div>
    </div>
</div>
