<?php
/**
 * @var $model \frontend\modules\insurance\forms\InsuranceForm
 * @var $row integer
 */

use yii\helpers\Html;
use common\modules\insurance\dictionaries\InsuranceFormDictionaries;

$labels = $model->attributeLabels();
?>
<div class="col-lg-4" style="clear: both;">
    <div class="js-driver-block js-driver-block-id-<?= $row ?>">
        <?php if ($row > 0) { ?>
            <hr>
        <?php } ?>
        <div class="row">
            <div class="row">
                <?= Html::activeLabel($model, 'driverLastName') ?><br>
                <?= Html::activeTextInput($model, 'driverLastName[' . $row . ']',
                    [
                        'id' => 'insuranceform-driverlastname_' . $row,
                        'class' => 'form-control js-fio-input',
                        'placeholder' => $labels['driverLastName']
                    ]); ?>
                <div class="js-error-msg js-error-insuranceform-driverlastname_<?= $row ?>"></div>
            </div>
        </div>
        <div class="row">
            <div class="row">
                <?= Html::activeLabel($model, 'driverFirstName') ?><br>
                <?= Html::activeTextInput($model, 'driverFirstName[' . $row . ']',
                    [
                        'id' => 'insuranceform-driverfirstname_' . $row,
                        'class' => 'form-control js-fio-input',
                        'placeholder' => $labels['driverFirstName']
                    ]); ?>
                <div class="js-error-msg js-error-insuranceform-driverfirstname_<?= $row ?>"></div>
            </div>
        </div>
        <div class="row">
            <div class="row">
                <?= Html::activeLabel($model, 'driverMiddleName') ?><br>
                <?= Html::activeTextInput($model, 'driverMiddleName[' . $row . ']',
                    [
                        'id' => 'insuranceform-drivermiddlename_' . $row,
                        'class' => 'form-control js-fio-input',
                        'placeholder' => $labels['driverMiddleName']
                    ]); ?>
                <div class="js-error-msg js-error-insuranceform-drivermiddlename_<?= $row ?>"></div>
            </div>
        </div>
        <div class="row">
            <div class="row">
                <?= Html::activeLabel($model, 'driverBirthDate') ?><br>
                <?php
                echo \yii\jui\DatePicker::widget([
                    'model' => $model,
                    'attribute' => 'driverBirthDate[' . $row . ']',
                    'language' => 'ru',
                    'dateFormat' => 'dd.MM.yyyy',
                    'options' => [
                        'id' => 'insuranceform-driverbirthdate' . $row,
                        'placeholder' => $labels['driverBirthDate'],
                        'class' => 'form-control js-mask-date js-date-picker-change-year-yes',
                    ],
                ]);
                ?>
                <div class="js-error-msg js-error-insuranceform-driverbirthdate_<?= $row ?>"></div>
            </div>
        </div>
        <div class="row">
            <div class="row">
                <?= Html::activeLabel($model, 'driverLicenseSerialNumber') ?><br>
                <?= Html::activeTextInput($model, 'driverLicenseSerialNumber[' . $row . ']',
                    [
                        'id' => 'insuranceform-driverlicenseserialnumber_' . $row,
                        'class' => 'form-control js-mask-pass-serial-number',
                        'placeholder' => $labels['driverLicenseSerialNumber'],
                        'value' => (!empty($model->driverLicenseSerial[$row]) && !empty($model->driverLicenseNumber[$row]))
                            ? $model->driverLicenseSerial[$row] . " " . $model->driverLicenseNumber[$row]
                            : null
                    ]); ?>
                <div class="js-error-msg js-error-insuranceform-driverlicenseserialnumber_<?= $row ?>"></div>
            </div>
        </div>
        <div class="row">
            <div class="row">
                <?= Html::activeLabel($model, 'driverExpDate') ?><br>
                <?php
                echo \yii\jui\DatePicker::widget([
                    'model' => $model,
                    'attribute' => 'driverExpDate[' . $row . ']',
                    'language' => 'ru',
                    'dateFormat' => 'dd.MM.yyyy',
                    'options' => [
                        'id' => 'insuranceform-driverexpdate' . $row,
                        'placeholder' => $labels['driverExpDate'],
                        'class' => 'form-control js-mask-date js-date-picker-change-year-yes',
                    ],
                ]);
                ?>
                <div class="js-error-msg js-error-insuranceform-driverexpdate_<?= $row ?>"></div>
            </div>
        </div>

        <div class="row">
            <div class="row">
                <?= Html::activeHiddenInput($model, 'driverPrevAddInfo[' . $row . ']',
                    [
                        'id' => 'insuranceform-driverprevaddinfo_' . $row,
                        'class' => 'js-driver-prev-add-info-input'
                    ]) ?>
                <a href="#" class="btn bg-info" onclick="
                        let parentBlock = $(this).closest('.js-driver-block');
                        let input = parentBlock.find('.js-driver-prev-add-info-input');
                        let block = parentBlock.find('.js-driver-info-prev-info-form');
                        let addInfoVal = block.val();
                        input.val(parseInt(input.val()) == <?= InsuranceFormDictionaries::ADD_PREV_DRIVER_INFO_YES ?> ? <?= InsuranceFormDictionaries::ADD_PREV_DRIVER_INFO_NO ?> : <?= InsuranceFormDictionaries::ADD_PREV_DRIVER_INFO_YES ?>);
                        block.slideToggle();
                        return false;
                        ">
                    Указать предыдущее ВУ
                </a>
            </div>
        </div>

        <div
                class="js-driver-info-prev-info-form"
                style="<?=
                !empty($model->driverPrevLastName[$row]) ||
                !empty($model->driverPrevFirstName[$row]) ||
                !empty($model->driverPrevMiddleName[$row]) ||
                !empty($model->driverPrevLicenseSerial[$row]) ||
                !empty($model->driverPrevLicenseNumber[$row]) ||
                !empty($model->driverPrevLicenseDate[$row])
                    ? ""
                    : "display: none"
                ?>"
        >
            <?= Yii::$app->controller->renderPartial('_prev_driver_info', ['model' => $model, 'row' => $row]) ?>
        </div>

        <?php if ($row > 0) { ?>
            <div class="row">
                <div class="row" style="padding-bottom: 10px;">
                    <a href="#" class="btn btn-danger js-add-remove-driver-btn" data-type="remove" data-row="<?= $row ?>">Удалить
                        водителя</a>
                </div>
            </div>
        <?php } ?>
    </div>
</div>