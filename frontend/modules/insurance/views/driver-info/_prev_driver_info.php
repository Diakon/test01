<?php
/**
 * @var $model \frontend\modules\insurance\forms\InsuranceForm
 * @var $row integer
 */

use yii\helpers\Html;

$labels = $model->attributeLabels();
?>
<div class="row">
    <?= Html::activeLabel($model, 'driverPrevLastName') ?><br>
    <?= Html::activeTextInput($model, 'driverPrevLastName[' . $row . ']',
        [
            'id' => 'insuranceform-driverprevlastname_' . $row,
            'class' => 'form-control js-fio-input',
            'placeholder' => $labels['driverPrevLastName']
        ]); ?>
    <div class="js-error-msg js-error-insuranceform-driverprevlastname_<?= $row ?>"></div>
</div>
<div class="row">
    <?= Html::activeLabel($model, 'driverPrevFirstName') ?><br>
    <?= Html::activeTextInput($model, 'driverPrevFirstName[' . $row . ']',
        [
            'id' => 'insuranceform-driverprevfirstname_' . $row,
            'class' => 'form-control js-fio-input',
            'placeholder' => $labels['driverPrevFirstName']
        ]); ?>
    <div class="js-error-msg js-error-insuranceform-driverprevfirstname_<?= $row ?>"></div>
</div>
<div class="row">
    <?= Html::activeLabel($model, 'driverPrevMiddleName') ?><br>
    <?= Html::activeTextInput($model, 'driverPrevMiddleName[' . $row . ']',
        [
            'id' => 'insuranceform-driverprevmiddlename_' . $row,
            'class' => 'form-control js-fio-input',
            'placeholder' => $labels['driverPrevMiddleName']
        ]); ?>
    <div class="js-error-msg js-error-insuranceform-driverprevmiddlename_<?= $row ?>"></div>
</div>
<div class="row">
    <?= Html::activeLabel($model, 'driverPrevLicenseSerialNumber') ?><br>
    <?= Html::activeTextInput($model, 'driverPrevLicenseSerialNumber[' . $row . ']',
        [
            'id' => 'insuranceform-driverprevlicenseserialnumber_' . $row,
            'class' => 'form-control js-mask-pass-serial-number',
            'placeholder' => $labels['driverPrevLicenseSerialNumber'],
            'value' => !empty($model->driverPrevLicenseSerial[$row]) && !empty($model->driverPrevLicenseNumber[$row])
                ? ($model->driverPrevLicenseSerial[$row] . " " . $model->driverPrevLicenseNumber[$row])
                : null
        ]); ?>
    <div class="js-error-msg js-error-insuranceform-driverprevlicenseserialnumber_<?= $row ?>"></div>
</div>
<div class="row">
    <?= Html::activeLabel($model, 'driverPrevLicenseDate') ?><br>
    <?php
    echo \yii\jui\DatePicker::widget([
        'model' => $model,
        'attribute' => 'driverPrevLicenseDate[' . $row . ']',
        'language' => 'ru',
        'dateFormat' => 'dd.MM.yyyy',
        'options' => [
            'id' => 'insuranceform-driverprevlicensedate_' . $row,
            'placeholder' => $labels['driverPrevLicenseDate'],
            'class' => 'form-control js-date-picker-change-year-yes js-mask-date',
        ],
    ]);
    ?>
    <div class="js-error-msg js-error-insuranceform-driverprevlicensedate_<?= $row ?>"></div>
</div>