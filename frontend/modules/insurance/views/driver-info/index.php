<?php
/**
 * @var $model \frontend\modules\insurance\forms\InsuranceForm
 * @var $row integer
 */

use common\modules\insurance\dictionaries\InsuranceFormDictionaries;

$labels = $model->attributeLabels();
?>
<?php if ($row == 0) { ?>
    <?php echo \yii\helpers\Html::activeHiddenInput($model, 'multiDrive', ['class' => 'js-is-multi-drive-input']) ?>
    <a href="#" style="margin-bottom:10px;" data-type="<?= InsuranceFormDictionaries::IS_MULTI_DRIVE_NO ?>" class="js-is-multi-drive-href btn btn-info">Ограниченное число водителей</a>
    <a href="#" style="margin-bottom:10px;" data-type="<?= InsuranceFormDictionaries::IS_MULTI_DRIVE_YES ?>" class="js-is-multi-drive-href btn btn-info">Мультидрайв</a>
<?php } ?>

<div class="col-lg-12 js-drivers-main-block" style="<?= $model->multiDrive == InsuranceFormDictionaries::IS_MULTI_DRIVE_NO ? '' : 'display:none' ?>">
    <?php if (!empty($model->driverLastName)) { ?>
        <?php foreach ($model->driverLastName as $row => $data) { ?>
            <?= Yii::$app->controller->renderPartial('_form', ['model' => $model, 'row' => $row]) ?>
        <?php } ?>
    <?php } else { ?>
        <?= Yii::$app->controller->renderPartial('_form', ['model' => $model, 'row' => $row]) ?>
    <?php } ?>
</div>
