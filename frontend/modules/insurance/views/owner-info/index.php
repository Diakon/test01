<?php
/**
 * Поля для расширенного расчета
 *
 * @var $model \frontend\modules\insurance\forms\InsuranceForm
 */
use common\modules\insurance\dictionaries\InsuranceFormDictionaries;
use yii\helpers\Html;

$labels = $model->attributeLabels();
?>

<div class="col-lg-4">
    <div class="row">
        <h4>Информация о собственнике автомобиля</h4>
        <hr>
    </div>
    <div class="row">
        <?= Html::activeLabel($model, 'ownerCityName') ?><br>
        <?= \yii\jui\AutoComplete::widget([
            'model' => $model,
            'attribute' => 'ownerCityName',
            'clientOptions' => [
                'source' => \yii\helpers\Url::to(['/insurance/owner-info/', 'action' => 'searchCity']),
                'minLength'=>'3',
                'select' => new \yii\web\JsExpression("function( event, ui ) {
                console.log(ui);
                $('#insuranceform-ownercity').val(ui.item.id);
                }")
            ],
            'options'=>[
                'id' => 'js-city-owner',
                'class' => 'form-control',
                'placeholder' => $labels['ownerCityName']
            ]
        ]);
        ?>
        <?= Html::activeHiddenInput($model, 'ownerCity') ?>
        <div class="js-error-msg js-error-insuranceform-ownercity"></div>
    </div>
    <br>
    <div id="js-owner-info-block">
        <div class="row">
            <?= Html::activeLabel($model, 'ownerLastName') ?><br>
            <?= Html::activeTextInput($model, 'ownerLastName', ['class' => 'form-control js-fio-input', 'placeholder' => $labels['ownerLastName']]); ?>
            <div class="js-error-msg js-error-insuranceform-ownerlastname"></div>
        </div>
        <div class="row">
            <?= Html::activeLabel($model, 'ownerFirstName') ?><br>
            <?= Html::activeTextInput($model, 'ownerFirstName', ['class' => 'form-control js-fio-input', 'placeholder' => $labels['ownerFirstName']]); ?>
            <div class="js-error-msg js-error-insuranceform-ownerfirstname"></div>
        </div>
        <div class="row">
            <?= Html::activeLabel($model, 'ownerMiddleName') ?><br>
            <?= Html::activeTextInput($model, 'ownerMiddleName', ['class' => 'form-control js-fio-input', 'placeholder' => $labels['ownerMiddleName']]); ?>
            <div class="js-error-msg js-error-insuranceform-ownermiddlename"></div>
        </div>
        <div class="row">
            <?= Html::activeLabel($model, 'ownerBirthDate') ?><br>
            <?php
            echo \yii\jui\DatePicker::widget([
                'model' => $model,
                'attribute' => 'ownerBirthDate',
                'language' => 'ru',
                'dateFormat' => 'dd.MM.yyyy',
                'options' => [
                    'id' => 'insuranceform-ownerbirthdate',
                    'placeholder' => $labels['ownerBirthDate'],
                    'class' => 'form-control js-mask-date js-date-picker-change-year-yes',
                ],
            ]);
            ?>
            <div class="js-error-msg js-error-insuranceform-ownerbirthdate"></div>
        </div>
        <div class="row">
            <?= Html::activeLabel($model, 'ownerDadataAddress') ?><br>
            <?= \yii\jui\AutoComplete::widget([
                'model' => $model,
                'attribute' => 'ownerDadataAddress',
                'clientOptions' => [
                    'source' => \yii\helpers\Url::to(['/nsurance/owner-info/', 'action' => 'searchAddress']),
                    'minLength'=>'3',
                    'select' => new \yii\web\JsExpression("function( event, ui ) {
                $(this).val(ui.item.value);
                }")
                ],
                'options'=>[
                    'class' => 'form-control',
                    'placeholder' => $labels['ownerDadataAddress']
                ]
            ]);
            ?>
            <div class="js-error-msg js-error-insuranceform-insurerdadataaddress"></div>
        </div>
        <div class="row">
            <?= Html::activeLabel($model, 'ownerPassportSerialNumber') ?><br>
            <?= Html::activeTextInput($model, 'ownerPassportSerialNumber',
                [
                    'class' => 'form-control js-mask-pass-serial-number',
                    'placeholder' => $labels['ownerPassportSerialNumber'],
                    'value' => !empty($model->ownerPassportSerial) && !empty($model->ownerPassportNumber)
                        ? ($model->ownerPassportSerial . " " . $model->ownerPassportNumber)
                        : null
                ]); ?>
            <div class="js-error-msg js-error-insuranceform-ownerpassportserialnumber"></div>
        </div>
        <div class="row">
            <?= Html::activeLabel($model, 'ownerPassportIssuedBy') ?><br>
            <?= Html::activeTextInput($model, 'ownerPassportIssuedBy', ['class' => 'form-control', 'placeholder' => $labels['ownerPassportIssuedBy']]); ?>
            <div class="js-error-msg js-error-insuranceform-ownerpassportissuedby"></div>
        </div>
        <div class="row">
            <?= Html::activeLabel($model, 'ownerPassportDate') ?><br>
            <?php
            echo \yii\jui\DatePicker::widget([
                'model' => $model,
                'attribute' => 'ownerPassportDate',
                'language' => 'ru',
                'dateFormat' => 'dd.MM.yyyy',
                'options' => [
                    'id' => 'insuranceform-ownerpassportdate',
                    'placeholder' => $labels['ownerPassportDate'],
                    'class' => 'form-control js-mask-date js-date-picker-change-year-yes',
                ],
            ]);
            ?>
            <div class="js-error-msg js-error-insuranceform-ownerpassportdate"></div>
        </div>
        <div class="row">
            <?= Html::activeLabel($model, 'ownerEmail') ?><br>
            <?= Html::activeTextInput($model, 'ownerEmail', ['class' => 'form-control', 'placeholder' => $labels['ownerEmail']]); ?>
            <div class="js-error-msg js-error-insuranceform-owneremail"></div>
        </div>
        <div class="row">
            <?= Html::activeLabel($model, 'ownerPhone') ?><br>
            <?= Html::activeTextInput($model, 'ownerPhone', ['class' => 'form-control js-mask-phone', 'placeholder' => $labels['ownerPhone']]); ?>
            <div class="js-error-msg js-error-insuranceform-ownerphone"></div>
        </div>
    </div>

    <div id="js-insurer-info-block">
        <div class="row">
            <div class="custom-control custom-switch">
                <input type="checkbox" class="custom-control-input"
                    <?= $model->insurerIsOwner == InsuranceFormDictionaries::ONE_FACE_NO ? "" : "checked" ?>
                       id="js-insurer-is-owner-switch">
                <label class="custom-control-label" for="js-insurer-is-owner-switch">Страхователь и собственник одно лицо</label>
                <?= Html::activeHiddenInput($model, 'insurerIsOwner', ['class' => 'js-insurer-is-owner-input']) ?>
            </div>
        </div>

        <div class="js-insurer-is-owner-yes" style="<?= $model->insurerIsOwner == InsuranceFormDictionaries::ONE_FACE_NO ? "" : "display:none" ?>">
            <div class="row">
                <h4>Информация о страхователе</h4>
                <hr>
            </div>

            <div class="row">
                <?= Html::activeLabel($model, 'insurerLastName') ?><br>
                <?= Html::activeTextInput($model, 'insurerLastName', ['class' => 'form-control js-fio-input', 'placeholder' => $labels['insurerLastName']]); ?>
                <div class="js-error-msg js-error-insuranceform-insurerlastname"></div>
            </div>
            <div class="row">
                <?= Html::activeLabel($model, 'insurerFirstName') ?><br>
                <?= Html::activeTextInput($model, 'insurerFirstName', ['class' => 'form-control js-fio-input', 'placeholder' => $labels['insurerFirstName']]); ?>
                <div class="js-error-msg js-error-insuranceform-insurerfirstname"></div>
            </div>
            <div class="row">
                <?= Html::activeLabel($model, 'insurerMiddleName') ?><br>
                <?= Html::activeTextInput($model, 'insurerMiddleName', ['class' => 'form-control js-fio-input', 'placeholder' => $labels['insurerMiddleName']]); ?>
                <div class="js-error-msg js-error-insuranceform-insurermiddlename"></div>
            </div>
            <div class="row">
                <?= Html::activeLabel($model, 'insurerBirthDate') ?><br>
                <?php
                echo \yii\jui\DatePicker::widget([
                    'model' => $model,
                    'attribute' => 'insurerBirthDate',
                    'language' => 'ru',
                    'dateFormat' => 'dd.MM.yyyy',
                    'options' => [
                        'id' => 'insuranceform-insurerbirthdate',
                        'placeholder' => $labels['insurerBirthDate'],
                        'class' => 'form-control js-mask-date js-date-picker-change-year-yes',
                    ],
                ]);
                ?>
                <div class="js-error-msg js-error-insuranceform-insurerbirthdate"></div>
            </div>
            <div class="row">
                <?= Html::activeLabel($model, 'insurerDadataAddress') ?><br>
                <?= \yii\jui\AutoComplete::widget([
                    'model' => $model,
                    'attribute' => 'insurerDadataAddress',
                    'clientOptions' => [
                        'source' => \yii\helpers\Url::to(['/insurance/owner-info/', 'action' => 'searchAddress']),
                        'minLength'=>'3',
                        'select' => new \yii\web\JsExpression("function( event, ui ) {
                $(this).val(ui.item.value);
                }")
                    ],
                    'options'=>[
                        'class' => 'form-control',
                        'placeholder' => $labels['insurerDadataAddress']
                    ]
                ]);
                ?>
                <div class="js-error-msg js-error-insuranceform-insurerdadataaddress"></div>
            </div>
            <div class="row">
                <?= Html::activeLabel($model, 'insurerPassportSerialNumber') ?><br>
                <?= Html::activeTextInput($model, 'insurerPassportSerialNumber',
                    [
                        'class' => 'form-control js-mask-pass-serial-number',
                        'placeholder' => $labels['insurerPassportSerialNumber'],
                        'value' => !empty($model->insurerPassportSerial) && !empty($model->insurerPassportNumber)
                            ? ($model->insurerPassportSerial . " " . $model->insurerPassportNumber)
                            : null
                    ]); ?>
                <div class="js-error-msg js-error-insuranceform-insurerpassportserialnumber"></div>
            </div>
            <div class="row">
                <?= Html::activeLabel($model, 'insurerPassportIssuedBy') ?><br>
                <?= Html::activeTextInput($model, 'insurerPassportIssuedBy', ['class' => 'form-control', 'placeholder' => $labels['insurerPassportIssuedBy']]); ?>
                <div class="js-error-msg js-error-insuranceform-insurerpassportissuedby"></div>
            </div>
            <div class="row">
                <?= Html::activeLabel($model, 'insurerPassportDate') ?><br>
                <?php
                echo \yii\jui\DatePicker::widget([
                    'model' => $model,
                    'attribute' => 'insurerPassportDate',
                    'language' => 'ru',
                    'dateFormat' => 'dd.MM.yyyy',
                    'options' => [
                        'id' => 'insuranceform-insurerpassportdate',
                        'placeholder' => $labels['insurerPassportDate'],
                        'class' => 'form-control js-mask-date js-date-picker-change-year-yes',
                    ],
                ]);
                ?>
                <div class="js-error-msg js-error-insuranceform-insurerpassportdate"></div>
            </div>
            <div class="row">
                <?= Html::activeLabel($model, 'insurerEmail') ?><br>
                <?= Html::activeTextInput($model, 'insurerEmail', ['class' => 'form-control', 'placeholder' => $labels['insurerEmail']]); ?>
                <div class="js-error-msg js-error-insuranceform-insureremail"></div>
            </div>
            <div class="row">
                <?= Html::activeLabel($model, 'insurerPhone') ?><br>
                <?= Html::activeTextInput($model, 'insurerPhone', ['class' => 'form-control js-mask-phone', 'placeholder' => $labels['insurerPhone']]); ?>
                <div class="js-error-msg js-error-insuranceform-insurerphone"></div>
            </div>
        </div>
    </div>
</div>