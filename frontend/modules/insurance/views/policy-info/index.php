<?php
/**
 * @var $model \frontend\modules\insurance\forms\InsuranceForm
 */

use yii\helpers\Html;
use common\modules\insurance\dictionaries\InsuranceFormDictionaries;

$label = $model->attributeLabels();
?>

<div class="col-lg-4">
    <div class="row">
        <p>
            <a href="#" class="desc__toggle" onclick="$('.js-date-start-policy-notice').slideToggle(); return false;">
                Дата начала действия полиса
            </a>
        </p>
        <div class="js-date-start-policy-notice" style="display: none;">
            <p>
                <b>
                    Дата выдачи полиса должна быть не менее 4 дней от сегодняшнего дня. Это значительно повышает шансы на
                    получение максимально выгодного предложения!
                </b>
            </p>
        </div>
        <?php
        echo \yii\jui\DatePicker::widget([
            'model' => $model,
            'attribute' => 'ownerDateOSAGOStart',
            'language' => 'ru',
            'dateFormat' => 'dd.MM.yyyy',
            'options' => [
                'id' => 'insuranceform-ownerdateosagostart',
                'placeholder' => $label['ownerDateOSAGOStart'],
                'class' => 'form-control js-date-picker-change-year-no-mindate js-mask-date js-osago-start-date-input',
            ],
        ]);
        ?>
        <?= Html::activeTextInput($model, 'ownerDateOSAGOEnd',
            [
                'class' => 'form-control js-osago-end-date-input',
                'disabled' => 'disabled',
                'style' => 'display:none; width: 45%; position: absolute; margin-top:-33px; margin-left:55%;',
                'value' => !empty($model->ownerDateOSAGOEnd) ? Yii::$app->formatter->asDate($model->ownerDateOSAGOEnd, 'php:d.m.Y') : ""
            ]) ?>
        <div class="js-osago-end-date-input-date">
            <?php
            if (!empty($model->ownerDateOSAGOEnd)) {
                echo Html::tag('p', '<b>Дата окончания действия полиса: ' . Yii::$app->formatter->asDate($model->ownerDateOSAGOEnd, 'php:d.m.Y') . '</b>');
            }
            ?>
        </div>
        <div class="js-error-msg js-error-insuranceform-ownerdateosagostart"></div>
    </div>

    <div class="row">
        <?= Html::activeLabel($model, 'usePeriod') ?><br>
        <?= Html::activeDropDownList($model, 'usePeriod', $model->getUsePeriodItems(), ['class' => 'form-control', 'disabled' => 'disabled']); ?>
        <div class="js-error-msg js-error-insuranceform-useperiod"></div>
    </div>

    <div class="js-isset-prev-policy-block" style="<?= $model->prevPolicyIsset == InsuranceFormDictionaries::PREV_POLICY_ISSET_NO ? "display:none" : "" ?>">
        <div class="row">
            <hr>
            <h4>Информация о действующем полисе</h4>
            <?= Html::activeLabel($model, 'prevPolicySerialNumber') ?><br>
            <?= Html::activeTextInput(
                    $model,
                    'prevPolicySerialNumber',
                    [
                        'class' => 'js-mask-policy-serial-number form-control',
                        'placeholder' => $label['prevPolicySerialNumber'],
                        'value' => !empty($model->prevPolicySerial) && !empty($model->prevPolicyNumber)
                            ? ($model->prevPolicySerial . " " . $model->prevPolicyNumber)
                            : null
                    ]); ?>
            <div class="js-error-msg js-error-insuranceform-prevpolicyserialnumber"></div>
        </div>

        <div class="row">
            <?= Html::activeLabel($model, 'prevPolicyEndDate') ?><br>
            <?php
            echo \yii\jui\DatePicker::widget([
                'model' => $model,
                'attribute' => 'prevPolicyEndDate',
                'language' => 'ru',
                'dateFormat' => 'dd.MM.yyyy',
                'options' => [
                    'id' => 'insuranceform-prevpolicyenddate',
                    'placeholder' => $label['prevPolicyEndDate'],
                    'class' => 'form-control js-mask-date js-date-picker-change-year-yes',
                ],
            ]);
            ?>
            <div class="js-error-msg js-error-insuranceform-prevpolicyenddate"></div>
        </div>
    </div>
    <div class="row">
        <div class="custom-control custom-switch">
            <input type="checkbox"
                   class="custom-control-input" <?= $model->prevPolicyIsset == InsuranceFormDictionaries::PREV_POLICY_ISSET_NO ? "" : "checked" ?>
                   id="js-prev-policy-isset-switch">
            <label class="custom-control-label" for="js-prev-policy-isset-switch">Есть предыдущий полис</label>
            <?= Html::activeHiddenInput($model, 'prevPolicyIsset', ['class' => 'js-prev-policy-isset-input']) ?>
        </div>
    </div>
</div>