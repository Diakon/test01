<?php
/**
 * @var $model \frontend\modules\insurance\forms\InsuranceForm
 */
use yii\helpers\Html;
use common\modules\insurance\dictionaries\InsuranceFormDictionaries;

?>
<div align="center">
    <h4>Выбор типа расчета</h4>
</div>
<p>
    Для расчета потребуются все данные об авто (СТС или ПТС) и водителях, а так же данные собственника.
    После заполнения всей необходимой информации можно будет сразу перейти к оформлению и оплате полиса ОСАГО.
</p>
<div class="col-lg-4">
    <div class="row">
        <a href="#" class="btn btn-info js-select-type-calc-btn" data-type="<?= InsuranceFormDictionaries::PRE_CALCULATION_NO ?>">К расчету</a>
    </div>
</div>
<!--
<p style="margin-top: 40px; clear: both;">
    При предварительном расчете потребуется минимум информации. Позволяет быстро получить предварительную стоимость полиса.
</p>
<div class="col-lg-4">
    <div class="row">
        <a
                href="#"
                onclick="alert('Данный функционал временно не доступен. Воспользуйтесь полным расчетом.'); return false;"
                class="btn btn-info off----js-select-type-calc-btn"
                data-type="<?= InsuranceFormDictionaries::PRE_CALCULATION_YES ?>"
        >Предварительный расчет</a>
    </div>
</div>
-->
<div style="clear: both;">
    <?= Html::activeHiddenInput($model, 'preCalculation', ['class' => 'js-pre-calculation-type-input']) ?>
</div>