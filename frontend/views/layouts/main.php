<?php

/* @var $this \yii\web\View */

/* @var $content string */

use yii\helpers\Url;
use yii\helpers\Html;
use frontend\assets\AppAsset;
use frontend\modules\descriptionPage\widgets\DescriptionPage;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<!-- Chatra {literal} -->
<script>
    (function(d, w, c) {
        w.ChatraID = 'KiTo6mPMbGHBjAPgW';
        var s = d.createElement('script');
        w[c] = w[c] || function() {
            (w[c].q = w[c].q || []).push(arguments);
        };
        s.async = true;
        s.src = 'https://call.chatra.io/chatra.js';
        if (d.head) d.head.appendChild(s);
    })(document, window, 'Chatra');
</script>
<!-- /Chatra {/literal} -->
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<?php if (!in_array(Yii::$app->request->pathInfo, ['', 'payment-link/'])) { ?>
    <?php
    $descriptionPage = DescriptionPage::widget();
    ?>
    <table style="width: 100%">
        <tr>
            <td>
                <div class="js-header-logo-big" align="<?= !empty($descriptionPage) ? '' : 'center' ?>">
                    <?= Html::a('<img src="/images/logo2.png" width="150px" height="45px">', Url::to(['/'])) ?>
                </div>
            </td>
            <td>
                <div align="right">
                    <?php echo $descriptionPage ?>
                </div>
            </td>
        </tr>
    </table>
<?php } ?>

<div class="container">
    <?= $content ?>

    <?php if (!Yii::$app->user->isGuest) { ?>
        <div class="main-menu-block">
            <div class="stick-menu__menu-row">
                <?= Html::a('<img src="/images/icon-newcalc.png" width="24px"><br><span class="stick-menu__item-name">Новый расчет</span>', Url::to(['/new-calculation']), ['class' => 'stick-menu__menu-item btn btn-default']) ?>
                <?= Html::a('<img src="/images/icon-mycalc.png" width="24px"><br><span class="stick-menu__item-name">Мои расчеты</span>', Url::to(['/my-calculations']), ['class' => 'stick-menu__menu-item btn btn-default']) ?>
                <?= Html::a('<img src="/images/icon-repayment.png" width="24px"><br><span class="stick-menu__item-name">Ожидают оплаты</span>', Url::to(['/awaiting-payment']), ['class' => 'stick-menu__menu-item btn btn-default']) ?>
                <?= Html::a('<img src="/images/icon-coin.png" width="24px"><br><span class="stick-menu__item-name">Оплаченные</span>', Url::to(['/paid']), ['class' => 'stick-menu__menu-item btn btn-default']) ?>
                <?= Html::a('<img src="/images/icon-profile.png" width="24px"><br><span class="stick-menu__item-name">Профиль</span>', Url::to(['/profile']), ['class' => 'stick-menu__menu-item btn btn-default']) ?>
                <?= Html::a('<img src="/images/icon-support.png" width="24px"><br><span class="stick-menu__item-name">Поддержка</span>', Url::to(['/faq']), ['class' => 'stick-menu__menu-item btn btn-default']) ?>
            </div>
        </div>
    <?php } ?>
</div>

<div id="chatra-wrapper"></div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
