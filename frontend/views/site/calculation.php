<?php
/**
 * @var $this yii\web\View
 * @var $order \common\modules\orders\models\Order|null
 * @var string $viewName;
 */

use yii\bootstrap\BootstrapAsset;
use yii\web\JqueryAsset;
use yii\widgets\ActiveForm;
use yii\helpers\Html;

$this->title = 'Новый расчет';
$step = !empty($order) ? $order->step : 'typecalc';
?>
<?php $this->registerCssFile("/css/jquery-ui.css", ['depends' => [BootstrapAsset::class]]); ?>
<?php $this->registerJsFile('/js/moment.min.js', ['depends' => [JqueryAsset::class]]); ?>
<?php $this->registerJsFile('/js/jquery.inputmask.js', ['depends' => [JqueryAsset::class]]); ?>
<?php $this->registerJsFile('/js/calculation.js?v=370', ['depends' => [JqueryAsset::class]]); ?>
<?php
$form = ActiveForm::begin([
    'options' => [
        'id' => 'js-osago-form',
        'method' => 'post',
        'style' => 'margin-bottom:30px;',
    ]
]) ?>
<?= Html::hiddenInput('orderId', !empty($order) ? $order->id : 0, ['class' => 'js-order-id']) ?>

<div class="body-content body-style-block">
    <div class="row">
        <div>
            <ul class="navbar-nav  nav js-menu-block" style="<?= $step == 'typecalc' ? 'display:none;' : 'display:flex;' ?>;">
                <li>
                    <div class="nav-bar-circle">
                        <p data-step="typecalc" class="js-menu-item js-menu-item-switch-typecalc" style="text-align:center">1</p>
                    </div>
                    <p style="display: none" class="nav-bar-text js-menu-item js-menu-item-typecalc">Типа расчета</p>
                </li>
                <li>
                    <div class="nav-bar-circle">
                        <p data-step="car" class="js-menu-item js-menu-item-switch-car" style="text-align:center">2</p>
                    </div>
                    <p style="display: none" class="nav-bar-text js-menu-item js-menu-item-car">Автомобиль</p>
                </li>
                <li>
                    <div class="nav-bar-circle">
                        <p data-step="driver" class="js-menu-item js-menu-item-switch-driver" style="text-align:center">3</p>
                    </div>
                    <p style="display: none" class="nav-bar-text js-menu-item js-menu-item-driver">Водители</p>
                </li>
                <li>
                    <div class="nav-bar-circle">
                        <p data-step="owner" class="js-menu-item js-menu-item-switch-owner" style="text-align:center">4</p>
                    </div>
                    <p style="display: none" class="nav-bar-text js-menu-item js-menu-item-owner">Собственник</p>
                </li>
                <li>
                    <div class="nav-bar-circle">
                        <p data-step="policy" class="js-menu-item js-menu-item-switch-policy" style="text-align:center">5</p>
                    </div>
                    <p style="display: none" class="nav-bar-text js-menu-item js-menu-item-policy">Полис</p>
                </li>
                <li>
                    <div class="nav-bar-circle">
                        <p data-step="calc" class="js-menu-item js-menu-item-switch-calc" style="text-align:center">6</p>
                    </div>
                    <p style="display: none" class="nav-bar-text js-menu-item js-menu-item-calc">Расчет</p>
                </li>
            </ul>
        </div>
        <div style="clear: both; margin-top:-15px;">
            <hr>
        </div>
        <div class="js-step-order" style="margin-top:-15px;">
            <div class="js-step js-step-typecalc" data-step="typecalc"></div>
            <div class="js-step js-step-car" data-step="car"></div>
            <div class="js-step js-step-driver" data-step="driver"></div>
            <div class="js-step js-step-owner" data-step="owner"></div>
            <div class="js-step js-step-policy" data-step="policy"></div>
            <div class="js-step js-step-calc" data-step="calc"></div>
        </div>

        <div class="">
            <div class="js-driver-btn" style="display: none">
                <?= Html::hiddenInput('driverCount', 0, ['class' => 'js-input-driver-count']) ?>
                <a href="#" class="btn btn-info js-add-remove-driver-btn" data-type="add">Добавить водителя</a>
            </div>
        </div>

        <br>
        <?= Html::hiddenInput('step', $step, ['class' => 'js-input-step']) ?>
        <hr style="clear: both;">
        <div align="center">
            <div style="display:none" class="js-ajax-load-image">
                <div class="load-block-img js-load-block-img">
                    <img class="rot" src="/images/load-2.png" width="200px">
                    <h3 class="js-load-block-calc-title" style="position:absolute; margin-left:-50px; margin-top:-130px; width:300px;"></h3>
                </div>
            </div>

            <div class="js-action-reload-page-block" style="padding:5px; display: none" align="center">
                <?= Html::a('Обновить', '#',
                    [
                        'class' => 'btn btn-primary js-action-reload-page-btn',
                        'onclick' => 'location.reload(); return false;'
                    ]); ?>
            </div>

            <a href="#" class="btn btn-info js-prev-step-btn" style="display: none">Назад</a>
            <a href="#" class="btn btn-info js-next-step-btn" style="<?= $step == 'typecalc' ? 'display:none' : '' ?>">Продолжить</a>
        </div>

    </div>

</div>

<?php ActiveForm::end() ?>