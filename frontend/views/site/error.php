<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $message;
?>
<div align="center" style="margin-top: -50px;">

    <!--
    <div class="alert alert-danger">
        <?php echo nl2br(Html::encode($message)) ?>
        <h1>404</h1>
    </div>
    -->
    <p style="font-size: 100pt"><b>404</b></p>
    <br>
    <b>Страница не найдена</b>
    <br>

    <?= Html::a('Вернуться на главную', \yii\helpers\Url::to('/')) ?>

</div>
