<?php
/**
 * @var $this yii\web\View
 * @var $orderId integer|null
 * @var string $viewName;
 */

$this->title = 'Example';
?>

<div class="main-page-logo-img">
    <img src="/images/logo-big2.png" width="170px">
</div>

<div align="center" style="margin-top: -20px">
    <h5 class="purple-color">
        Перейдите в раздел «Новый расчет», введите номер автомобиля и часть данных мы заполним за Вас.
    </h5>
    <br>
    <?= \frontend\widgets\InstructionVideo::widget(['view' => 'main-page']) ?>
</div>