/**
 * Основной скрипт подключаемый всегда на всех страницах
 * @type {{init: BASE_SCRIPT.init, supportChat: ((function(): number)|*)}}
 */
var BASE_SCRIPT = {
    init: function (settings) {
        //конфиги
        BASE_SCRIPT.config = {

        };
        $.extend(BASE_SCRIPT.config, settings);
        BASE_SCRIPT.chatSupport();
        BASE_SCRIPT.orders();
        BASE_SCRIPT.faq();
        BASE_SCRIPT.modal();
    },

    /**
     * Параметры модального окна
     */
    modal:function () {
        // Для модалки
        let modalContainer = '.js-modal-page-container';
        $(modalContainer + ' .modal-dialog-centered').css('min-height', 'calc(100% - (.5rem * 2))');

        $(modalContainer).find('iframe').each(function() {
            $(this).attr('style', '');
        });
        $(modalContainer + ' .modal-dialog-centered').css('min-height', 'inherit');
        $(modalContainer + ' .modal-body').css('overflow-y', 'auto');
        $(modalContainer + ' .modal-body').css('overflow-x', 'hidden');
        $(modalContainer + ' .modal-body').css('max-height', $(window).height() * 0.77);
    },

    /**
     * Страница FAQ
     */
    faq:function () {
      $(document).on('click', '.js-faq-block', function () {
          $(this).find('.js-faq-answer').slideToggle();
          return false;
      });
    },

    /**
     * Страница заказов
     */
    orders:function () {
        // Кнопка "Поделиться ссылкой на оплату" - копирование в буфер обмена
        $(document).on('click', '.js-share-payment-link', function () {
            let paymentUrl = document.getElementById("js-payment-url-link-input");
            paymentUrl.select();
            document.execCommand("copy");
            alert("Ссылка на оплату\n" + $('#js-payment-url-link-input').val() + "\nскопирована в буфер обмена.");
            return false;
        });
        // Кнопка "Ссылка на документы" - копирование в буфер обмена
        $(document).on('click', '.js-paid-documents-link', function () {
            let orderId = $(this).data('order');
            let idName = "js-paid-documents-url-link-input-" + orderId;
            let documentsUrl = document.getElementById(idName);
            documentsUrl.select();
            document.execCommand("copy");
            alert("Ссылка на страницу документов\n" + $('#' + idName).val() + "\nскопирована в буфер обмена.");
            return false;
        });
        // Если это страница перехода на экран оплаты - делаю ajax запрос получения ссылки
        $(document).on('click', '.js-redirect-to-payment-order-accept-btn', function () {
            $('.js-ajax-load-image').show();
            $('.js-payment-link-page-error').empty();
            $(this).hide();
            $.ajax({
                type: 'POST',
                url: location.href,
                success: function (data) {
                    document.location.href = data.msg['url'];
                },
                error:  function(data){
                    $('.js-ajax-load-image').hide();
                    $('.js-redirect-to-payment-order-accept-btn').show();
                    let msg = 'Ссылка на оплату устарела. Пожалуйста, попросите сгенерировать новую ссылку.';
                    $('.js-payment-link-page-error').empty().append(msg);
                }
            });
        });
        // Кнопка получения ссылок документов на оплату
        $(document).on('click', '.js-payment-document-link', function () {
            let url = $(this).data('url');
            let block = $(this).parent().find('.js-payment-document-block');
            $.ajax({
                type: 'POST',
                url: url,
                success: function (data) {
                    if (data.msg.documents) {
                        let links = '';
                        $(data.msg.documents).each(function(key, value) {
                            if (value.name != 'Экземпляр полиса ОСАГО для страховщика.pdf') {
                                links += '<a class="btn btn-sm btn-primary" style="margin-bottom: 5px;" href="' + value.url + '" target="_blank">' + value.name + '</a><br>';
                            }
                        });
                        block.empty().append(links);
                        block.show();
                    }
                },
                error:  function(data){
                    let msg = '<p style="color: #FF6B6B">Ошибка при получении ссылок об оплаченом заказе. Пожалуйста, попробуйте снова или обратитесь в поддержку.</p>';
                    block.empty().append(msg);
                    block.show();
                }
            });

            return false;
        });

        // Кнопка удаления заказа
        $(document).on('click', '.js-order-delete-btn', function () {
            let url = window.location.href.split('?')[0];
            url = url.replace('#', '');
            let orderId = parseInt($(this).data('order'));
            let needDelete = confirm("Вы уверены что хотите те удалить данный расчет?");
            if (needDelete == true) {
                $.get(url + '?action=delete&orderId=' + orderId, function(data) {
                    $('.js-order-block-row-' + orderId).remove();

                    return false;
                });
            } else {
                return false;
            }
        });

        // Кнопка вернуть заказ на этап расчета - вывод запроса
        $(document).on('click', '.js-order-to-calc-return-btn', function () {
            let url = window.location.href.split('?')[0];
            url = url.replace('#', '');
            let orderId = parseInt($(this).data('order'));
            let returnToCalc = confirm("Вы уверены, что хотите внести изменения и вернуться к расчету?");
            if (returnToCalc == true) {
                $.get(url + '?action=returnToCalc&orderId=' + orderId, function(data) {
                    document.location.href = data.msg['url'];
                });
            } else {
                return false;
            }
        });

        // Отсчет времени оставшегося до оплаты заказа и оплаченных полисов
        let timerClass = 'js-order-countdown-timer';
        if ($('span').hasClass(timerClass)) {
            $('.' + timerClass).each(function() {
                let order = $(this).data('order');
                let end_date = {
                    "full_year": $(this).data('year'),
                    "month": $(this).data('month'),
                    "day": $(this).data('day'),
                    "hours": $(this).data('hours'),
                    "minutes": $(this).data('minutes'),
                    "seconds": $(this).data('seconds')
                };
                let pattern = $(this).data('pattern');
                let timer_show = document.getElementById("js-timer-" + order);
                countdownTimer(end_date, timer_show, pattern);
            });
        }

        function countdownTimer(endDateObj, timer_show, pattern)
        {
            let end_date_str = `${endDateObj.full_year}-${endDateObj.month}-${endDateObj.day}T${endDateObj.hours}:${endDateObj.minutes}:${endDateObj.seconds}`;
            timer = setInterval(function () {
                let now = new Date();
                let date = new Date(end_date_str);
                let ms_left = date - now;

                let res = new Date(ms_left);
                let year = res.getUTCFullYear() - 1970;
                let day = res.getUTCDate() - 1;
                let month = res.getUTCHours();
                let hours = parseInt(res.getUTCHours()) < 10 ? ('0' + res.getUTCHours()) : res.getUTCHours();
                let minutes = parseInt(res.getUTCMinutes()) < 10 ? ('0' + res.getUTCMinutes()) : res.getUTCMinutes();
                let seconds = parseInt(res.getUTCSeconds()) < 10 ? ('0' + res.getUTCSeconds()) : res.getUTCSeconds();

                let patternStringFormat = "";
                switch (pattern) {
                    case "hч:iм:sс":
                        patternStringFormat = hours + "ч:" + minutes + "м:" + seconds + "с";
                        break;
                    case "dд:Hч:iм:sс":
                        // Считаю количество дней до даты (без разбивки по месяцам и годам)
                        let dateStart = new Date();
                        let dateEnd = new Date(end_date_str);
                        let differenceInTime = dateEnd.getTime() - dateStart.getTime();
                        let differenceInDays = differenceInTime / (1000 * 3600 * 24);
                        patternStringFormat = (Math.trunc(differenceInDays)) + "д:" + hours + "ч:" + minutes + "м:" + seconds + "с";
                        break;
                }

                timer_show.innerHTML = patternStringFormat;
            }, 1000);
        }

        // Отсчет времени оставшегося до истечения действия полис
    },

    /**
     * Чат поддержки
     */
    chatSupport:function () {
        // цвет кнопки чата
        window.ChatraSetup = {
            buttonSize: 1,
            buttonPosition: window.innerWidth < 1024 ? // порог ширины
                'bl' : // положение кнопки чата на маленьких экранах
                'br',  // положение кнопки чата на больших экранах
        };
        Chatra('hide'); // Кнопку показа чатры скрываем
        Chatra('setLocale', {
            name: 'Петр',
            yourName: 'Скляров',
            email: 'cowardly_lion@yahoo.com',
            phone: '+79620173333'
        });

        // Нажади на кнопку открытия чатры
        $(document).on('click', '.js-support-chat-button', function () {
            Chatra('openChat', true);
            Chatra('show');
        });
    }
};
BASE_SCRIPT.init();