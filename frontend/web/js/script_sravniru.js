/**
 * Скрипт JS для модуля Сравни.ру
*/
var requestAjax = false;
var PROJECT = {
    init: function (settings) {
        //конфиги
        PROJECT.config = {
            urlCarInfo: '/sravniru/car-info/',
            urlTypeCalc: '/sravniru/type-calc/',
            urlDriverInfo: '/sravniru/driver-info/',
            urlOwnerInfo: '/sravniru/owner-info/',
            urlPolicy: '/sravniru/policy-info/',
            urlCalc: '/sravniru/calculation/',
            urlPayment: '/sravniru/payments/',
        };

        $.extend(PROJECT.config, settings);

        PROJECT.installMenuSwitchStep();
        PROJECT.installMaskInput();
        PROJECT.addRemoveDriver();
        PROJECT.initPage();
        PROJECT.stepSwitch();
        PROJECT.clearErrorMsgOnForm();
        PROJECT.setErrorMsgOnForm(responseServer = null, stepName = null);
        PROJECT.setCarInfoByNumber();
        PROJECT.setCarIdentityType();
        PROJECT.payment();
    },

    /**
     * Возвращает ID заказа
     * @returns {number}
     */
    getOrderId:function () {
        return parseInt($('.js-order-id').val());
    },

    /**
     * Возвращает url для ajax запроса
     * @param step
     * @returns {string}
     */
    getStepUrl:function (step) {
        let url = "";
        switch (step) {
            case "typecalc":
                url = PROJECT.config.urlTypeCalc;
                break;
            case "car":
                url = PROJECT.config.urlCarInfo;
                break;
            case "driver":
                url = PROJECT.config.urlDriverInfo;
                break;
            case "owner":
                url = PROJECT.config.urlOwnerInfo;
                break;
            case "policy":
                url = PROJECT.config.urlPolicy;
                break;
            case "calc":
                url = PROJECT.config.urlCalc;
        }

        return url;
    },

    /**
     * Переход по шагам оформления ОСАГО
     * @param selectedStepName Шаг на который надо перейти. Если не указан - определяю следующий/предыдущий шаг от текущего шага
     */
    stepSwitch:function (selectedStepName = null) {
        /**
         * Переход на выбранный в selectedStepName шаг
         */
        if (selectedStepName != null) {
            $('.js-input-step').val(selectedStepName);
            prevStep(selectedStepName);
            // Прячу кнопки вперед / назад на 1ом шаге (тип расчета), т.к. там по другому отображаем переход
            if (selectedStepName == "typecalc") {
                $('.js-prev-step-btn').hide();
                $('.js-next-step-btn').hide();
                $('.js-menu-block').hide(); // Меню так же прячется на 1ом шаге
            }
        }

        /**
         * Кнопка возврата на предыдущий шаг
         */
        $('.js-prev-step-btn').click(function () {
            $('.js-menu-block').show();
            let currentStepName = $('.js-input-step').val(); // Текущий шаг
            // Получаю шаг, который надо обработать
            let prevStepName = "";
            $(".js-step").each(function() {
                let stepName = $(this).data("step");
                // Дошли до текущего шага - надо вернуться к предыдущему
                if (stepName == currentStepName) {
                    return false;
                }
                prevStepName = stepName;
            });

            $('.js-input-step').val(prevStepName);
            prevStep(prevStepName);

            // Прячу кнопки вперед / назад на 1ом шаге (тип расчета), т.к. там по другому отображаем переход
            if (prevStepName == "typecalc") {
                $(this).hide();
                $('.js-next-step-btn').hide();
                $('.js-menu-block').hide(); // Меню так же прячется на 1ом шаге
            }

            return false;
        });

        /**
         * Возврат на предыдущий шаг
         * @param stepName
         */
        function prevStep(stepName)
        {
            $('.js-ajax-load-image').show();
            $('.js-step').hide(); // Скрфваю все формы - showForm покажет нужную, когда подтянет данные
            // В меню ставлю активным (выделенным) новый шаг
            $('.nav-bar-text').hide();
            $('.nav-bar-circle').show();
            $('.js-menu-item-' + stepName).parent().find('.nav-bar-circle').hide();
            $('.js-menu-item-' + stepName).show();
            // Для водителей надо очистить от старых страниц загруженных по ajax
            if (stepName == 'driver') {
                $('.js-driver-block').remove();
            }
            showForm(stepName);
            $('.js-ajax-load-image').hide();
            setTimeout(function() {PROJECT.installMaskInput();}, 2000);
        }

        /**
         * Кнопка перехода на следующий шаг
         */
        $('.js-next-step-btn').click(function () {
            // Получаю шаг, который надо обработать
            let stepName = $('.js-input-step').val();
            nextStep(stepName);

            return false;
        });

        /**
         * Переход на следующий шаг
         * @param stepName
         */
        function nextStep(stepName) {
            $('.js-menu-block').show();
            $('.js-ajax-load-image').show();
            let url = PROJECT.getStepUrl(stepName);
            // Проверяю введенные данные
            let form = $('#js-osago-form').serialize();
            PROJECT.clearErrorMsgOnForm()

            $.ajax({
                type: 'POST',
                url: url + '?action=validate&order_id=' + PROJECT.getOrderId(),
                data: form,
                success: function(data) {
                    $('.js-ajax-load-image').hide();
                    let step = data.msg.step;

                    if (data.msg.orderId) {
                        $('.js-order-id').val(parseInt(data.msg.orderId))
                    }
                    $('.js-step').hide(); // Скрываю все формы - showForm покажет нужную, когда подтянет данные
                    // В меню ставлю активным (выделенным) новый шаг
                    $('.nav-bar-text').hide();
                    $('.nav-bar-circle').show();
                    $('.js-menu-item-' + step).parent().find('.nav-bar-circle').hide();
                    $('.js-menu-item-' + step).show();

                    // Для водителей надо очистить от старых страниц загруженных по ajax
                    if (step == 'driver') {
                        $('.js-driver-block').remove();
                    }

                    // Показываю - скрываю кнопку возврата назад в зависимости от шага
                    if (step != "typecalc") {
                        $('.js-prev-step-btn').show();
                    } else {
                        $('.js-prev-step-btn').hide();
                    }

                    // Если последний шаг - расчет (calc), то произвожу отправку всех введеных на прошлом шаге данных и вывод результата
                    if (step == "calc") {
                        PROJECT.calculate(step);
                    } else {
                        showForm(step);
                    }
                },
                error:  function(data){
                    $('.js-ajax-load-image').hide();
                    PROJECT.setErrorMsgOnForm(data, stepName);
                }
            });
        }

        /**
         * Переход на следующий шаг
         *
         * @param step
         * @returns {boolean}
         */
        function showForm(step) {
            if (requestAjax) requestAjax.abort(); // Прерываю запрос, если был отправлен уже

            $('.js-action-reload-page-block').hide();
            $('.js-next-step-btn').show();
            $('.js-driver-btn').hide();
            $('.js-input-step').val(step); // Скрытое поле текущего шага
            let html = $('.js-step-' + step).html();
            let url = PROJECT.getStepUrl(step);

            // Если шаг добавления водителей - показываю кнопки +/- для добавления
            if (step == "driver") {
                $('.js-driver-btn').show();
            }

            $('.js-next-step-btn').html(step == "policy" ? 'Рассчитать' : 'Продолжить');

            // Форма не тянулась - тяну
            let form = $('#js-osago-form').serialize();
            requestAjax = $.ajax({
                url: url + '?action=formHtml&order_id=' + PROJECT.getOrderId(),
                type: 'post',
                data: form,
                success: function (data) {
                    $('.js-step-' + step).empty().append(data).show();

                    PROJECT.installDatePicker(jQuery('.js-date-picker-change-year-yes'), true);
                    PROJECT.installDatePicker(jQuery('.js-date-picker-change-year-no'), false);
                    PROJECT.installDatePicker(jQuery('.js-date-picker-change-year-yes-mindate'), false, true);
                    PROJECT.installDatePicker(jQuery('.js-date-picker-change-year-no-mindate'), false, true);
                    PROJECT.installDatePicker(jQuery('.js-date-picker-change-year-no-mindate-4day'), false, true, false, 4);
                    // Инсталирую автокомплит, для шага "Автомобиль", "Водитель", "Собственник" или "Полис"
                    switch (step) {
                        case "owner":
                            jQuery('#osagosravniruform-ownercity').autocomplete({"source":"\/sravniru\/owner-info\/?action=searchCity","minLength":"3","select":function( event, ui ) {
                                    $('#osagosravniruform-ownercity').val(ui.item.id);
                                }});
                            jQuery('#osagosravniruform-ownerdadataaddress').autocomplete({"source":"\/sravniru\/owner-info\/?action=searchAddress","minLength":"3","select":function( event, ui ) {
                                    $(this).val(ui.item.value);
                                }});
                            jQuery('#osagosravniruform-insurerdadataaddress').autocomplete({"source":"\/sravniru\/owner-info\/?action=searchAddress","minLength":"3","select":function( event, ui ) {
                                    $(this).val(ui.item.value);
                                }});
                            break;
                        case "driver":
                            let isMultiDrive = parseInt($('.js-is-multi-drive-input').val());
                            if (isMultiDrive == 1) {
                                $('.js-add-remove-driver-btn').hide(); // Скрываю кнопку "Добавить водителя", если мультидрайв
                            }
                            break;
                        case "car":
                            // Дата выдачи диагностической карты не может быть больше 2х лет от текущей даты
                            let issueDate = new Date();
                            let yearIssueDate = issueDate.getFullYear();
                            let monthIssueDate = issueDate.getMonth();
                            let dayIssueDate = issueDate.getDate();
                            issueDate = new Date(yearIssueDate + 2, monthIssueDate, dayIssueDate);
                            PROJECT.installDatePicker(jQuery('#osagosravniruform-vehicleissuedate'),
                                false,
                                false,
                                issueDate
                            );
                            break;
                    }
                    window.moveTo(0,0); // Перемещаю к верху страницы экран
                    PROJECT.installMaskInput();
                }
            });

            return true;
        }
    },

    /**
     * Проставляет для меню класс js-menu-switch-step для быстрого перехода между шагами
     */
    installMenuSwitchStep:function ()
    {
        $('.js-menu-switch-step').remove();
        let steps = [];
        let currentStepName = $('.js-input-step').val(); // Текущий шаг
        // Для всех шагов до этого шага ставим в меню класс js-menu-switch-step - признак, что можно нажать и перейти сразу на этот шаг
        $(".js-step").each(function() {
            let step = $(this).data('step');
            steps.push(step);
            // Дошли до текущего шага - останавливаем цикл
            if (step == currentStepName) {
                return false;
            }
        });
        // Проставляю в меню для шагов по которым можно перейти класс js-menu-switch-step
        if (steps.length > 0) {
            $.each(steps, function (index, value) {
                $('.js-menu-block').find('.js-menu-item-switch-' + value).addClass('js-menu-switch-step');
            });
        }
    },

    /**
     * Инсталлирует маски ввода
     */
    installMaskInput:function()
    {
        setTimeout(function (){
            let inputMaskDriverSerial = new Inputmask("99**"); // Серия водительского удостоверения
            let inputMaskDriverNumber = new Inputmask("999999"); // Номер водительского удостоверения
            let inputMaskDriverSerialNumber = new Inputmask("99** 999999"); // Серия водительского удостоверения
            let inputMaskDate = new Inputmask("99.99.9999"); // Дата
            let inputMaskYear = new Inputmask("9999"); // Год
            let inputMaskPassportSerial = new Inputmask("9999"); // Серия паспорта
            let inputMaskPassportNumber = new Inputmask("999999"); // Номер паспорта
            let inputMaskPassportSerialNumber = new Inputmask("9999 999999"); // Серия и номер паспорта
            let phoneNumber = new Inputmask("+7 (\\999) 999-99-99"); // Номер телефона
            let inputMaskPolicySerialNumber = new Inputmask("*** 9999999999"); // Серия и номер полиса

            inputMaskYear.mask($('.js-mask-year')); // Год
            inputMaskDate.mask($('.js-mask-date')); // Дата
            inputMaskPassportSerial.mask($('.js-mask-pass-serial')); // Серия паспорта
            inputMaskPassportNumber.mask($('.js-mask-pass-number')); // Номер паспорта
            inputMaskPassportSerialNumber.mask($('.js-mask-pass-serial-number')); // Серия и номер паспорта
            inputMaskDriverSerialNumber.mask($('.js-mask-pass-serial-number')); // Серия и Номер  паспорта в одном поле
            inputMaskPolicySerialNumber.mask($('.js-mask-policy-serial-number')); // Серия и Номер  полиса в одном поле
            inputMaskDriverSerial.mask($('.js-mask-driver-serial')); // Серия водительского удостоверения
            inputMaskDriverNumber.mask($('.js-mask-driver-number')); // Номер водительского удостоверения
            phoneNumber.mask($('.js-mask-phone')); // Номер телефона

            $('#osagosravniruform-vehiclelicenseplate').on('keypress', function() {
                var that = this;
                setTimeout(function() {
                    var res = /[^а-я0-9]/g.exec(that.value);
                    that.value = that.value.replace(res, '');
                }, 0);
            });

        }, 100);
    },

    /**
     * Добавляет / убирает водителя
     */
    addRemoveDriver:function () {
        // Кнопки переключения Мульти драйв / указать данные о водителях
        $(document).on('click', '.js-is-multi-drive-href', function () {
            let isMultiDrive = parseInt($(this).data('type'));
            $('.js-is-multi-drive-input').val(isMultiDrive);
            setTimeout(function () {
                if (isMultiDrive == 0) {
                    $('.js-add-remove-driver-btn').show();
                    $('.js-drivers-main-block').show();
                } else {
                    $('.js-add-remove-driver-btn').hide();
                    $('.js-next-step-btn').click();
                }
            }, 300);

            return false;
        });

        $(document).on('click', '.js-add-remove-driver-btn', function () {
            let block = $('.js-input-driver-count');
            let currentCount = parseInt(block.val());
            let type = $(this).data('type');
            let count = type == 'add' ? currentCount + 1 : currentCount - 1;
            count = count < 1 ? 0 : count;
            block.val(count);
            if (type == 'add') {
                $.ajax({
                    url: PROJECT.config.urlDriverInfo + '?row=' + count + "&order_id=" + PROJECT.getOrderId() + "&add=1",
                    type: 'get',
                    success: function (data) {
                        $('.js-step-driver').append(data);
                        PROJECT.installDatePicker(jQuery('.js-date-picker-change-year-yes'), true);
                        PROJECT.installDatePicker(jQuery('.js-date-picker-change-year-no'), false);
                        PROJECT.installDatePicker(jQuery('.js-date-picker-change-year-yes-mindate'), false, true);
                        PROJECT.installDatePicker(jQuery('.js-date-picker-change-year-no-mindate'), false, true);

                        PROJECT.installMaskInput();
                    }
                });
            }
            else {
                let rowId = $(this).data('row');
                $('.js-driver-block-id-' + rowId).remove();
            }

            return false;
        });
    },

    /**
     * Инициализирует страницу при открыттии - выводит данные формы, подключает автокомплиты и тд.
     */
    initPage: function () {
        // Тянет на страницу данные при открытии
        if ($('input').hasClass('js-input-step')) {
            let step = $('.js-input-step').val();
            let url = PROJECT.getStepUrl(step);

            $('.js-menu-item-' + step).parent().find('.nav-bar-circle').hide();
            $('.js-menu-item-' + step).show();

            $('.js-next-step-btn').html(step == "policy" ? 'Рассчитать' : 'Продолжить');

            if (step != 'typecalc') {
                $('.js-prev-step-btn').show();
            }
            if (step == 'calc') {
                PROJECT.calculate(step);
            } else {
                $.ajax({
                    url: url + "?order_id=" + PROJECT.getOrderId(),
                    type: 'get',
                    success: function (data) {
                        $('.js-step-typecalc').empty().append(data);
                    }
                });
            }
            setTimeout(function() {
                PROJECT.installMaskInput();
                PROJECT.installDatePicker(jQuery('.js-date-picker-change-year-yes'), true);
                PROJECT.installDatePicker(jQuery('.js-date-picker-change-year-no'), false);
                PROJECT.installDatePicker(jQuery('.js-date-picker-change-year-yes-mindate'), false, true);
                PROJECT.installDatePicker(jQuery('.js-date-picker-change-year-no-mindate'), false, true);
                PROJECT.installDatePicker(jQuery('.js-date-picker-change-year-no-mindate-4day'), false, true, false, 4);
                }, 1000);
        }

        // Устанавливаю действия на определенные кнопки:
        // Показывать - скрывать доп. поля при смене типа авто
        $(document).on('change', '.js-car-info-type-car-selector', function () {
            let type = parseInt($(this).val());
            $('.js-car-info-type').hide();
            $('.js-car-info-type-id-'+ type).show();
        });
        // Показывать данные о страхователе, если это не одно лицо с владельцем авто
        $(document).on('change', '#osagosravniruform-insurerisowner', function () {
            let status = parseInt($(this).val());
            $('.js-insurer-is-owner-yes').hide();
            if (status == 0) {
                $('.js-insurer-is-owner-yes').show();
            }
        });
        // В поля ввода ФИО можно вводить только русские буквы и символ пробела
        $(document).on('keyup', '.js-fio-input', function () {
            let that = this;
            let res = /[^А-Яа-я-]/g.exec(that.value);
            that.value = that.value.replace(res, '');
        });

        // Если нажали на кнопку "Перейти к полному расчету" - отправляю запрос на сервер и делаю обновление страницы (кнопка появляется на экране предварительного расчета)
        $(document).on('click', '.js-switch-to-final-calculation-btn', function () {
            $.ajax({
                url: PROJECT.config.urlTypeCalc + "?order_id=" + PROJECT.getOrderId() + "&action=changeToFinalCalculation",
                type: 'get',
                success: function (data) {
                    location.href = '/calculation/?order_id=' + PROJECT.getOrderId();
                },
                error:  function(data){
                    let msg = 'Заказ не найден';
                    alert(msg)
                }
            });
        });

        // При смене бренда - обновлять список моделей
        $(document).on('change', '.js-vehicle-brand-id', function () {
            PROJECT.updateBrandList();
        });

        // Показывать данные о страхователе, если это не одно лицо с владельцем авто
        $(document).on('change', '#js-insurer-is-owner-switch', function () {
            let isset = $(this).is(':checked');
            let status = isset ? 1 : 0;
            $('#osagosravniruform-insurerisowner').val(status);
            $('.js-insurer-is-owner-yes').hide();
            if (status == 0) {
                $('.js-insurer-is-owner-yes').show();
            }
        });

        // Переключатель "Есть предыдущий полис"
        $(document).on('change', '#js-prev-policy-isset-switch', function () {
            let isset = $(this).is(':checked');
            $('.js-prev-policy-isset-input').val(isset ? 1 : 0);
            $('.js-isset-prev-policy-block').hide();
            if (isset) {
                $('.js-isset-prev-policy-block').show();
            }
        });

        // Смена даты в поле "Окончание действия полиса" при вводе даты начала действия полиса (прибавляем 1 год)
        $(document).on('change', '.js-osago-start-date-input', function () {
            $('.js-osago-end-date-input-date').empty();
            let date = $(this).val();
            if (date && date != "") {
                let startDate = moment(date, 'DD.MM.YYYY');
                let endDate = startDate.clone();
                endDate.add(1, 'years').subtract('1', 'days');
                endDate = endDate.format('DD.MM.YYYY');
                let endDateText = endDate == "Invalid date" ? "" : endDate;
                $('.js-osago-end-date-input').empty().val(endDateText);
                $('.js-osago-end-date-input-date').append('<p><b>Дата окончания действия полиса: ' + endDateText + '</b></p>');
            }
        });

        // Кнопки выбора типа расчета
        $(document).on('click', '.js-select-type-calc-btn', function () {
            $('.js-pre-calculation-type-input').val($(this).data('type'));
            setTimeout(function () {
                $('.js-next-step-btn').click();
            }, 500);
        });

        // Переход на шаг по нажатию на кнопку в меню
        $(document).on('click', '.js-menu-switch-step', function () {
            let step = $(this).data('step');
            PROJECT.stepSwitch(step);
        });
    },

    /**
     * Обновляет список марок авто (выпадающий список)
     * @param carModelId
     * @param carBrandId
     */
    updateBrandList:function (carModelId = false, carBrandId = false) {
        let brandId = carBrandId ? carBrandId : $('.js-vehicle-brand-id').val();
        $.ajax({
            url: PROJECT.config.urlCarInfo + "?order_id=" + PROJECT.getOrderId() + "&action=searchModel",
            type: 'post',
            data: {brandId:brandId},
            success: function (data) {
                data = JSON.parse(data);
                let pattern = '<option value="">Выберите модкль ТС</option>';
                $.each(data, function(index, value) {
                    pattern += '<option value="' + index + '">' + value + '</option>'
                });
                let block = $(".js-vehicle-model-id");
                block.empty().append(pattern);
                if (carModelId) {
                    setTimeout(function (){block.val(carModelId)}, 500);
                }
                return true;
            },
            error:  function(data){
                let msg = 'Ошибка получения списка моделей';
                alert(msg);
                return false;
            }
        });
    },

    /**
     * Выполняет расчет стоимости полиса
     * @param step
     */
    calculate:function (step) {
        if (requestAjax) requestAjax.abort(); // Прерываю запрос, если был отправлен уже
        $('.js-ajax-load-image').show();
        $('.js-next-step-btn').hide();
        $('.js-step-' + step).empty();
        $('.js-input-step').val(step);
        let url = PROJECT.getStepUrl(step);
        let form = $('#js-osago-form').serialize();
        var selectedCompany = parseInt($('.js-input-selected-company').val());
        // Получаю тип расчета
        $.when(
            requestAjax = $.ajax({
                type: 'GET',
                url: PROJECT.config.urlTypeCalc + "?action=typeCalc&order_id=" + PROJECT.getOrderId(),
            })).then(function(data, textStatus, jqXHR) {
                let isPreCalculate = parseInt(data.msg);
                isPreCalculate = isPreCalculate == 1;
                // Если предварительный расчет - отправляю все данные и жду ответ (быстрый расчет)
                if (isPreCalculate) {
                    $('.js-switch-to-final-calculation-btn').hide();
                    $.ajax({
                        type: 'POST',
                        url: url + "?order_id=" + PROJECT.getOrderId(),
                        data: form,
                        success: function (data) {
                            $('.js-ajax-load-image').hide();
                            $('.js-step-' + step).empty().append(data).show();
                            $('.js-switch-to-final-calculation-btn').show(); // Показываю кнопку "Перейти к полному расчету"
                            // Вывожу заголовок
                            setTimeout(function (){
                                let title = "<h4>Поступили следующие предложения от страховых компаний:</h4><hr>";
                                $('.js-final-calculation-header').empty().append(title);
                            }, 500);
                        },
                        error:  function(data){
                            $('.js-ajax-load-image').hide();
                            alert("Ошибка запроса")
                        }
                    });
                }
                else {
                    // Полный расчет
                    $.ajax({
                        type: 'POST',
                        url: url + "?order_id=" + PROJECT.getOrderId(),
                        data: form,
                        success: function (data) {
                            $('.js-step-' + step).append(data).show();
                            $('.js-ajax-load-image').hide();
                        },
                        error:  function(data){
                            $('.js-ajax-load-image').hide();
                            alert("Ошибка запроса");
                        }
                    });
                }

                return true;
            }
        );
    },

    /**
     * Выполняет переключение между типами идентификацияя авто (по VIN, кузову, шасси)
     */
    setCarIdentityType:function () {
        $(document).on('change', '.js-vehicle-type-identity-select', function () {
            let type = parseInt($(this).val());
            $('.js-vehicle-type-identity').hide();
            $('.js-vehicle-type-identity-id-' + type).show();
        });
    },

    /**
     * Убирает ошибки с формы (стилизация выделения ошибок)
     */
    clearErrorMsgOnForm:function () {
        $('input, select').removeClass('error-input');
        $('.js-error-msg').empty();
    },

    /**
     * Выводит ошибки на форме
     * @param responseServer Ответ сервера
     * @param stepName Текущий шаг
     */
    setErrorMsgOnForm: function (responseServer, stepName) {
        if (responseServer != null) {
            if (responseServer.responseJSON.msg) {
                Object.keys(responseServer.responseJSON.msg).forEach(prop => {
                    // Водители - массив и обрабатывать вывод ошибок надо по другому
                    $('.js-error-' + prop).empty();
                    if (stepName == "driver") {
                        Object.keys(responseServer.responseJSON.msg[prop]).forEach(keyDriver => {
                            let error = responseServer.responseJSON.msg[prop][keyDriver];
                            $('#' + prop).addClass('error-input');
                            $('.js-error-' + prop).append(error);
                        });
                    } else {
                        let error = responseServer.responseJSON.msg[prop][0];
                        $('#' + prop).addClass('error-input');
                        $('.js-error-' + prop).append(error);
                    }
                });
            }
        }
    },

    /**
     * Инсталирует js календаря
     * @param block  блок для которого установить календарь
     * @param changeYear  Флаг запрета смены года
     * @param minDate Минимальная дата - текущая дата
     * @param maxDate Максимальная дата (в формету new Date())
     * @param minDay
     */
    installDatePicker:function(block, changeYear = false, minDate = false, maxDate = false, minDay = false)
    {
        let params = {
            "dateFormat":"dd.mm.yy",
            "monthNames":['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
            "dayNamesMin":['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
        }
        if (changeYear) {
            params['changeYear'] = true;
            if (!minDate && !maxDate && !minDay) {
                let endDate = moment().format('YYYY');
                params['yearRange'] = "1920:" + parseInt(endDate);
            }
        }
        if (minDate) {
            params['minDate'] = minDay ? minDay : 0;
        }
        if (maxDate) {
            params['maxDate'] = maxDate;
        }

        block.datepicker($.extend({}, $.datepicker.regional['ru'], params));
    },

    /***
     * Возвращает данные об авто по гос. номеру
     */
    setCarInfoByNumber:function () {
        $(document).on('click', '.js-set-car-info-by-number', function () {
            let stepName = $('.js-input-step').val();
            $.ajax({
                type: 'POST',
                url: PROJECT.config.urlCarInfo + '?action=carInfoByNumber&order_id=' + PROJECT.getOrderId(),
                data: $('#js-osago-form').serialize(),
                success: function (data) {
                    // Проставляю данные по авто
                    console.log(data.msg.car);
                    PROJECT.clearErrorMsgOnForm();
                    $('#osagosravniruform-vehicletype').val(data.msg.car.type);
                    $('#osagosravniruform-vehiclepower').val(data.msg.car.power);
                    $('#osagosravniruform-vehicleyear').val(data.msg.car.year);
                    $('#osagosravniruform-vehiclevin').val(data.msg.car.vin);
                    $('#osagosravniruform-vehiclebrand').val(data.msg.car.brand_id);
                    $('#osagosravniruform-vehicledoctype').val(data.msg.car.doc_type);
                    $('#osagosravniruform-vehicledocserial').val(data.msg.car.doc_serial);
                    $('#osagosravniruform-vehicledocnumber').val(data.msg.car.doc_number);

                    PROJECT.updateBrandList(data.msg.car.model_id, data.msg.car.brand_id);
                    let dateString = data.msg.car.doc_date;
                    if (dateString != null) {
                        let dateParts = dateString.split("-");
                        console.log(dateParts);
                        $('#osagosravniruform-vehicledocdate').val(dateParts[2] + '.' + dateParts[1] + '.' + dateParts[0]);
                    }

                },
                error:  function(data){
                    PROJECT.setErrorMsgOnForm(data, stepName);
                }
            });

            return false;
        });
    },

    /**
     * Операции с оплатой полиса
     */
    payment:function () {
        // Нажали на кнопку оплаты или сохранить в "Ожидают оплаты"
        $(document).on('click', '.js-prepare-payment-service, .js-save-to-awaiting-payment', function () {
            let validate = confirm("Вы уверены что хотите те выбрать это предложение. Ссылка на оплату будет действительна 24 часа?");
            if (validate == true) {
                let awaiting = $(this).hasClass('js-save-to-awaiting-payment') ? 1 : 0;
                let id = $(this).data('id');
                let urlPayment = $(this).data('url');
                let hash = $(this).data('hash');
                let companyId = $(this).data('company-id');
                let companyName = $(this).data('company-name');
                $(this).attr('disabled', true);
                $(this).append('<br>Пожалуйста, подождите');
                $.ajax({
                    type: 'POST',
                    url: PROJECT.config.urlPayment + '?order_id=' + PROJECT.getOrderId(),
                    data: {'id': id, 'companyId': companyId, 'companyName': companyName, 'url': urlPayment, 'hash': hash, 'awaiting': awaiting},
                    success: function (data) {
                        document.location.href = data.msg['url'];
                    },
                    error:  function(data){
                        let msg = 'Возникла ошибка.\n';
                        if (data.msg) {
                            msg += "Страховая компания вернула следующий ответ:"
                            msg += data.msg;
                        }
                        msg += "\nПожалуйста, попробуйте снова или обратитесь к администратору.";
                        alert(msg)
                    }
                });
            } else {
                return false;
            }
        });
    },
};
PROJECT.init();